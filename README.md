# PHP Extension
[![Author](http://img.shields.io/badge/author-@hgraca-blue.svg)](https://www.herbertograca.com)
[![Software License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/hgraca/php-extension?label=version&sort=semver&color=blue)](https://gitlab.com/hgraca/php-extension/-/tags)

[![build status](https://gitlab.com/hgraca/php-extension/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/hgraca/php-extension/-/pipelines)
[![coverage report](https://gitlab.com/hgraca/php-extension/badges/master/coverage.svg)](https://gitlab.com/hgraca/php-extension/commits/master)

A PHP library with code that can be included into a project and used as if it was part of the PHP core itself.

## Dev usage

## How to run

### Using local PHP (8.2):
- Install the dependencies with `composer install`;
- The tests can be run with `composer test`;
- To list all custom scripts run `composer run-script --list`.

### Using Docker:
- Change PHP and xdebug configs in `./build`;
- Install the dependencies with `docker compose -f ./build/docker-compose.yaml run app composer install`;
- The tests can be run with `docker compose -f ./build/docker-compose.yaml run app composer test`;
- To list all custom scripts run `docker compose -f ./build/docker-compose.yaml run app composer run-script --list`.
