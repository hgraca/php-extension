<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use Hgraca\PhpExtension\Identity\Ulid\UlidGenerator;
use Hgraca\PhpExtension\Identity\UserFacingId\UserFacingIdGenerator;
use Hgraca\PhpExtension\Identity\Uuid\UuidGenerator;
use Hgraca\PhpExtension\String\StringHelper;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Symfony\Component\Uid\Ulid as SymfonyUlid;

UuidGenerator::setDefaultGenerator(
    static fn(): string => RamseyUuid::uuid7()->toString()
);

UlidGenerator::setDefaultGenerator(
    fn(): string => (string) new SymfonyUlid()
);

UserFacingIdGenerator::setDefaultGenerator(
    fn(): string => StringHelper::GenerateUserFriendlyId()
);
