<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        dirname(__FILE__, 3) . '/src',
        dirname(__FILE__, 3) . '/tests',
    ]);
    $rectorConfig->skip([
        dirname(__FILE__, 3) . '/tests/TestSubject',
        dirname(__FILE__, 3) . '/tests/TestCase/CodeUnit/TestSubjects',
    ]);

    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_73,
        LevelSetList::UP_TO_PHP_74,
        LevelSetList::UP_TO_PHP_80,
        LevelSetList::UP_TO_PHP_81,
        LevelSetList::UP_TO_PHP_82,
    ]);
};
