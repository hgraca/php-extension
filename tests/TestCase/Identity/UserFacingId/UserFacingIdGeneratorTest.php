<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Identity\UserFacingId;

use Hgraca\PhpExtension\Identity\UserFacingId\DefaultUserFacingIdGeneratorAlreadySetException;
use Hgraca\PhpExtension\Identity\UserFacingId\UserFacingId;
use Hgraca\PhpExtension\Identity\UserFacingId\UserFacingIdGenerator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class UserFacingIdGeneratorTest extends AbstractTest
{
    /**
     * @test
     */
    public function generate(): void
    {
        self::assertTrue(UserFacingIdGenerator::validate((string) UserFacingIdGenerator::generate()));
    }

    /**
     * @test
     */
    public static function generateAsString(): void
    {
        self::assertTrue(UserFacingIdGenerator::validate(UserFacingIdGenerator::generateAsString()));
    }

    /**
     * @test
     */
    public function default_generator_is_already_set_in_bootstrap_and_setting_again_throws_exception(): void
    {
        // This takes into account that the default generator has already been set in the bootstrap
        self::expectException(DefaultUserFacingIdGeneratorAlreadySetException::class);
        UserFacingIdGenerator::setDefaultGenerator(
            /** @phpstan-ignore-next-line */
            fn ($ufid): string => $ufid
        );
    }

    /**
     * @test
     */
    public function override_default_generator_as_uuid(): void
    {
        $ufid = '000-000-AAAA';
        UserFacingIdGenerator::overrideDefaultGenerator(
            fn () => $ufid
        );

        self::assertEquals(new UserFacingId($ufid), UserFacingIdGenerator::generate());
    }

    /**
     * @test
     */
    public function override_default_generator_as_string(): void
    {
        $ufid = '000-000-AAAA';
        UserFacingIdGenerator::overrideDefaultGenerator(
            fn () => $ufid
        );

        self::assertEquals($ufid, UserFacingIdGenerator::generateAsString());
    }

    /**
     * @test
     */
    public function reset(): void
    {
        $ufid = '000-000-AAAA';
        UserFacingIdGenerator::overrideDefaultGenerator(
            fn () => $ufid
        );

        self::assertEquals($ufid, (string) UserFacingIdGenerator::generate());

        UserFacingIdGenerator::reset();
        self::assertNotEquals($ufid, (string) UserFacingIdGenerator::generate());
    }
}
