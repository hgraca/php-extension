<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Identity\UserFacingId;

use Hgraca\PhpExtension\Identity\UserFacingId\InvalidUserFacingIdStringException;
use Hgraca\PhpExtension\Identity\UserFacingId\UserFacingId;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @group micro
 *
 * @internal
 */
final class UserFacingIdTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_keep_id_when_given(): void
    {
        $validId = '000-000-AAAA';
        $userFacingId = new UserFacingId($validId);

        self::assertSame($validId, $userFacingId->toScalar());
    }

    /**
     * @test
     *
     * @dataProvider validateProvider
     */
    public function it_should_throw_an_exception_when_given_invalid_id(string $id): void
    {
        self::expectException(InvalidUserFacingIdStringException::class);

        new UserFacingId($id);
    }

    /**
     * @return array<string, array<string>>
     */
    public static function validateProvider(): array
    {
        return [
            'Invalid start' => ['ABC-000-AAAA'],
            'Invalid middle' => ['000-0A0-AAAA'],
            'Invalid U character' => ['000-000-AAAU'],
            'Invalid I character' => ['000-000-AAAI'],
            'Invalid L character' => ['000-000-AAAL'],
            'Invalid with lowercase characters' => ['000-000-aaaa'],
        ];
    }

    /**
     * @test
     */
    public function it_should_be_castable_to_string(): void
    {
        $validId = '000-000-AAAA';
        $userFacingId = new UserFacingId($validId);

        self::assertSame($validId, (string) $userFacingId);
    }

    /**
     * @test
     *
     * @dataProvider equalityProvider
     */
    public function it_should_check_equality(UserFacingId $id1, UserFacingId $id2, bool $shouldBeEqual): void
    {
        self::assertSame($shouldBeEqual, $id1->equals($id2));
    }

    /**
     * @return array<string, array{UserFacingId, UserFacingId, bool}>
     */
    public static function equalityProvider(): array
    {
        $userFacingId1 = '000-000-AAAA';
        $userFacingId2 = '111-111-BBBB';

        return [
            'equals' => [
                new UserFacingId($userFacingId1),
                new UserFacingId($userFacingId1),
                true,
            ],
            'non-equals' => [
                new UserFacingId($userFacingId1),
                new UserFacingId($userFacingId2),
                false,
            ],
        ];
    }

    /**
     * @test
     */
    public function can_be_constructed_without_argument_and_generates_a_uuid(): void
    {
        self::assertTrue(UserFacingId::isValid((new UserFacingId())->toScalar()));
    }

    /**
     * @test
     */
    public function to_string_returns_correct_string(): void
    {
        $ufid = '000-000-AAAA';

        self::assertEquals($ufid, (string) (new UserFacingId($ufid)));
    }
}
