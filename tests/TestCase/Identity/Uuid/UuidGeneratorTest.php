<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Identity\Uuid;

use Hgraca\PhpExtension\Identity\Uuid\DefaultUuidGeneratorAlreadySetException;
use Hgraca\PhpExtension\Identity\Uuid\Uuid;
use Hgraca\PhpExtension\Identity\Uuid\UuidGenerator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Ramsey\Uuid\Uuid as RamseyUuid;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class UuidGeneratorTest extends AbstractTest
{
    private const UUID = 'ec6f8140-10b1-4aef-bce1-bb0289eaae70';

    /**
     * @test
     */
    public function generate(): void
    {
        self::assertTrue(RamseyUuid::isValid((string) UuidGenerator::generate()));
    }

    /**
     * @test
     */
    public static function generateAsString(): void
    {
        self::assertTrue(RamseyUuid::isValid(UuidGenerator::generateAsString()));
    }

    /**
     * @test
     */
    public function default_generator_is_already_set_in_bootstrap_and_setting_again_throws_exception(): void
    {
        // This takes into account that the default generator has already been set in the bootstrap
        $this->expectException(DefaultUuidGeneratorAlreadySetException::class);
        UuidGenerator::setDefaultGenerator(
            static fn (string $uuid): string => $uuid
        );
    }

    /**
     * @test
     */
    public function override_default_generator_as_uuid(): void
    {
        $uuid = self::UUID;
        UuidGenerator::overrideDefaultGenerator(
            fn () => $uuid
        );

        self::assertEquals(new Uuid($uuid), UuidGenerator::generate());
    }

    /**
     * @test
     */
    public function override_default_generator_as_string(): void
    {
        $uuid = self::UUID;
        UuidGenerator::overrideDefaultGenerator(
            fn () => $uuid
        );

        self::assertEquals($uuid, UuidGenerator::generateAsString());
    }

    /**
     * @test
     */
    public function reset(): void
    {
        $uuid = self::UUID;
        UuidGenerator::overrideDefaultGenerator(
            fn () => $uuid
        );

        self::assertEquals($uuid, (string) UuidGenerator::generate());

        UuidGenerator::reset();
        self::assertNotEquals($uuid, (string) UuidGenerator::generate());
    }
}
