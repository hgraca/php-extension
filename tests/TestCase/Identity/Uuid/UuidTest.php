<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Identity\Uuid;

use Hgraca\PhpExtension\Identity\Uuid\InvalidUuidStringException;
use Hgraca\PhpExtension\Identity\Uuid\Uuid;
use Hgraca\PhpExtension\Identity\Uuid\UuidGenerator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class UuidTest extends AbstractTest
{
    private const UUID = 'ec6f8140-10b1-4aef-bce1-bb0289eaae70';

    /**
     * @test
     */
    public function construct_throws_exception_if_invalid_uuid_string(): void
    {
        $this->expectException(InvalidUuidStringException::class);
        new Uuid('foo');
    }

    /**
     * @test
     */
    public function can_be_constructed_with_valid_uuid(): void
    {
        $uuid = new Uuid(self::UUID);

        self::assertEquals(self::UUID, $uuid->toScalar());
    }

    /**
     * @test
     */
    public function can_be_constructed_without_argument_and_generates_a_uuid(): void
    {
        self::assertTrue(Uuid::isValid((new Uuid())->toScalar()));
    }

    /**
     * @test
     *
     * @dataProvider provideUuid
     */
    public function is_valid(string $uuid, bool $expectedValue): void
    {
        self::assertEquals($expectedValue, Uuid::isValid($uuid));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideUuid(): array
    {
        return [
            [self::UUID, true],
            ['foo', false],
        ];
    }

    /**
     * @test
     */
    public function to_string_returns_correct_string(): void
    {
        self::assertEquals(self::UUID, (string) (new Uuid(self::UUID)));
    }

    /**
     * @test
     *
     * @dataProvider provideUuidForComparison
     */
    public function knows_if_its_the_same_uuid(Uuid $uuid1, Uuid $uuid2, bool $expectedIsSame): void
    {
        self::assertEquals($expectedIsSame, $uuid1->equals($uuid2));
    }

    /**
     * @return array<array{Uuid, Uuid, bool}>
     */
    public static function provideUuidForComparison(): array
    {
        $uuid = UuidGenerator::generate();

        return [
            [$uuid, $uuid, true],
            [$uuid, UuidGenerator::generate(), false],
        ];
    }
}
