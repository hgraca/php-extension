<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Identity\Ulid;

use Hgraca\PhpExtension\Identity\Ulid\DefaultUlidGeneratorAlreadySetException;
use Hgraca\PhpExtension\Identity\Ulid\UlidGenerator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Symfony\Component\Uid\Ulid as SymfonyUlid;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class UlidGeneratorTest extends AbstractTest
{
    /**
     * @test
     */
    public function generates_as_string(): void
    {
        self::assertTrue(
            SymfonyUlid::isValid(UlidGenerator::generate())
        );
    }

    /**
     * @test
     */
    public function default_generator_is_already_set_in_bootstrap_and_setting_again_throws_exception(): void
    {
        // This takes into account that the default generator has already been set in the bootstrap
        self::expectException(DefaultUlidGeneratorAlreadySetException::class);
        UlidGenerator::setDefaultGenerator(
            /** @psalm-suppress MissingClosureReturnType InvalidArgument */
            /** @phpstan-ignore-next-line */
            fn ($uuid): string => $uuid
        );
    }

    /**
     * @test
     */
    public function override_default_generator_as_string(): void
    {
        $ulid = '01BX5ZZKBKACTAV9WEVGEMMVS0';
        UlidGenerator::overrideDefaultGenerator(
            fn (): string => $ulid
        );

        self::assertEquals($ulid, UlidGenerator::generate());
    }

    /**
     * @test
     */
    public function reset(): void
    {
        $ulid = '01BX5ZZKBKACTAV9WEVGEMMVS0';
        UlidGenerator::overrideDefaultGenerator(
            fn (): string => $ulid
        );

        self::assertEquals($ulid, UlidGenerator::generate());

        UlidGenerator::reset();
        self::assertNotEquals($ulid, UlidGenerator::generate());
    }
}
