<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Identity\Ulid;

use Hgraca\PhpExtension\Identity\Ulid\AbstractUlid;
use Hgraca\PhpExtension\Identity\Ulid\InvalidUlidStringException;
use Hgraca\PhpExtension\String\Json\JsonHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class UlidTest extends AbstractTest
{
    /**
     * @test
     */
    public function cannot_construct_with_invalid_ulid(): void
    {
        $this->expectException(InvalidUlidStringException::class);
        new DummyUlid('invalid-ulid');
    }

    /**
     * @test
     */
    public function can_be_constructed_with_valid_ulid(): void
    {
        $ulidString = '01BX5ZZKBKACTAV9WEVGEMMVRZ';
        $ulid = new DummyUlid($ulidString);

        self::assertSame($ulidString, $ulid->toScalar());
    }

    /**
     * @test
     */
    public function can_be_constructed_without_argument_and_generate_a_valid_ulid(): void
    {
        self::assertTrue(
            AbstractUlid::isValid((new DummyUlid())->toScalar())
        );
    }

    /**
     * @test
     *
     * @dataProvider provideUlid
     */
    public function can_validate_ulids(string $ulid, bool $expectedValue): void
    {
        self::assertEquals($expectedValue, AbstractUlid::isValid($ulid));
    }

    /**
     * @return array<string, array{string, bool}>
     */
    public static function provideUlid(): array
    {
        return [
            'valid ulid' => ['01BX5ZZKBKACTAV9WEVGEMMVRZ', true],
            'invalid string' => ['foo', false],
            'empty string' => ['', false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideUlidForComparison
     */
    public function knows_if_its_the_same_ulid(AbstractUlid $ulid1, AbstractUlid $ulid2, bool $expectedIsSame): void
    {
        /** @psalm-suppress ArgumentTypeCoercion */
        self::assertEquals($expectedIsSame, $ulid1->equals($ulid2));
    }

    /**
     * @return array<array{DummyUlid, DummyUlid, bool}>
     */
    public static function provideUlidForComparison(): array
    {
        $ulid = new DummyUlid();

        return [
            [$ulid, $ulid, true],
            [$ulid, new DummyUlid(), false],
        ];
    }

    /**
     * @test
     */
    public function it_should_be_serializable_to_json(): void
    {
        $dummyUlid = new DummyUlid();
        $serlializedDummyUlid = $dummyUlid->jsonSerialize();
        $dummyJson = '{"foo": "' . $serlializedDummyUlid . '"}';

        self::assertNotNull($serlializedDummyUlid);
        self::assertIsString($serlializedDummyUlid);
        /** @var array<mixed> $decoded */
        $decoded = JsonHelper::decode($dummyJson);
        $this->assertJsonContains($serlializedDummyUlid, $decoded);
    }

    /**
     * @param array<mixed> $jsonArray
     */
    private function assertJsonContains(string $needle, array $jsonArray): void
    {
        self::assertTrue(in_array($needle, array_values($jsonArray)));
    }
}
