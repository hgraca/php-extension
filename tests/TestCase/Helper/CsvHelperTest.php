<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Helper;

use Hgraca\PhpExtension\Helper\CsvException;
use Hgraca\PhpExtension\Helper\CsvHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class CsvHelperTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_converts_a_single_dimension_array(): void
    {
        $row = ['col_1' => 1, 'col_2' => 'b', 'col_3' => 'something something', 'col_4' => true];

        self::assertEquals(
            "1,b,\"something something\",1\n",
            CsvHelper::arrayRowToCsv($row)
        );
    }

    /**
     * @test
     */
    public function it_throws_exception_when_trying_to_convert_multi_dimensional_arrays(): void
    {
        self::expectException(CsvException::class);
        self::expectExceptionMessage('Can only convert one dimensional arrays to CSV.');
        CsvHelper::arrayRowToCsv(
            [
                ['col_1' => 1, 'col_2' => 'b', 'col_3' => true],
                ['col_1' => 2, 'col_2' => 'c', 'col_3' => false],
            ]
        );
    }
}
