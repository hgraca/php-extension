<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Helper;

use Hgraca\PhpExtension\Exception\UnexpectedValueException;
use Hgraca\PhpExtension\Helper\Assert;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use stdClass;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class AssertTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_throw_when_value_is_null(): void
    {
        $this->expectException(UnexpectedValueException::class);

        Assert::notNull(null, 'foo');
    }

    /**
     * @test
     *
     * @dataProvider notNullMixedProvider
     */
    public function it_should_not_throw_when_value_is_not_null(mixed $value): void
    {
        $this->expectNotToPerformAssertions();

        Assert::notNull($value, 'Value is null');
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function notNullMixedProvider(): array
    {
        return [
            'string' => ['foo'],
            'empty-string' => [''],
            'int' => [1],
            'float' => [0.1],
        ];
    }

    public function it_should_throw_when_string_value_is_empty(): void
    {
        $this->expectException(UnexpectedValueException::class);

        Assert::notEmptyString('', 'foo');
    }

    /**
     * @test
     */
    public function it_should_not_throw_when_string_is_not_empty(): void
    {
        $this->expectNotToPerformAssertions();

        Assert::notEmptyString('foo', 'Value is empty');
    }

    public function it_should_test_is_positive(): void
    {
        $this->expectNotToPerformAssertions();

        Assert::isPositive(5, 'foo');
        Assert::isPositive(0.1, 'foo');
    }

    /**
     * @test
     *
     * @dataProvider notPositiveNumberProvider
     */
    public function it_should_throw_when_not_positive(float|int $value): void
    {
        $this->expectException(UnexpectedValueException::class);

        Assert::isPositive($value, 'Value is negative');
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function notPositiveNumberProvider(): array
    {
        return [
            'negative' => [-5],
            'zero' => [0],
            'negative-float' => [-0.1],
        ];
    }

    /**
     * @test
     *
     * @dataProvider notStringValuesProvider
     */
    public function it_should_throw_when_not_string(mixed $value): void
    {
        $this->expectException(UnexpectedValueException::class);

        Assert::isString($value, 'Value is not a string');
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function notStringValuesProvider(): array
    {
        return [
            'int' => [1],
            'float' => [0.1],
            'array' => [[]],
            'object' => [new stdClass()],
            'bool' => [true],
        ];
    }

    public function it_should_not_throw_when_is_string(): void
    {
        $this->expectNotToPerformAssertions();

        Assert::isString('foo', 'Value is not a string');
        Assert::isString('', 'Value is not a string');
    }
}
