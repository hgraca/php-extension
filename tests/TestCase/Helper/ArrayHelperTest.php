<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Helper;

use Hgraca\PhpExtension\Exception\OutOfBoundsException;
use Hgraca\PhpExtension\Helper\ArrayHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class ArrayHelperTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideArrayKeyValues
     */
    public function it_detects_if_array_is_not_key_value_string_map(array $array, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, ArrayHelper::isKeyValueStringMap($array));
    }

    public static function provideArrayKeyValues()
    {
        return [
            [['a' => 'a', 'b' => 'b', 'c' => 'c'], true],
            [['a' => 1, 'b' => 'b', 'c' => 'c'], false],
            [['a' => 'a', 2 => 'b', 'c' => 'c'], false],
            [['a' => 1, 2 => 'b', 'c' => 'c'], false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideArrayKeys
     */
    public function it_detects_if_array_has_non_string_keys(array $array, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, ArrayHelper::hasOnlyStringKeys($array));
    }

    public static function provideArrayKeys()
    {
        return [
            [['a' => 1, 'b' => 'b', 'c' => 'c'], true],
            [['a' => 'a', 2 => 'b', 'c' => 'c'], false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideArrayValues
     */
    public function it_detects_if_array_has_non_string_values(array $array, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, ArrayHelper::hasOnlyStringValues($array));
    }

    public static function provideArrayValues()
    {
        return [
            [['a', 'b', 'c'], true],
            [['a', 1, 'c'], false],
        ];
    }

    /**
     * @test
     */
    public function filter_by_keys(): void
    {
        self::assertSame(['a' => 1], ArrayHelper::filterByKeys(['a' => 1, 'b' => 2], ['a', 'c']));
    }

    /**
     * @test
     *
     * @dataProvider provideEnsureAllStringsAreWrappedInCases
     */
    public function ensure_all_strings_are_wrapped_in(string $left, string $right, array $haystack, array $expected): void
    {
        self::assertSame($expected, ArrayHelper::ensureAllStringsAreWrappedIn($left, $right, $haystack));
    }

    public static function provideEnsureAllStringsAreWrappedInCases(): array
    {
        return [
            ['<p>', '</p>', ['aaabbb', 'ccc'], ['<p>aaabbb</p>', '<p>ccc</p>']],
            ['<p>', '</p>', ['<p>aaabbb</p>', '<p>ccc'], ['<p>aaabbb</p>', '<p>ccc</p>']],
        ];
    }

    /**
     * @test
     */
    public function ensure_all_strings_are_wrapped_in_evaluates_given_callable(): void
    {
        $array = ['aaabbb', 'ccc'];

        self::assertSame(
            $array,
            ArrayHelper::ensureAllStringsAreWrappedIn(
                '<p>',
                '</p>',
                $array,
                fn ($value) => true
            )
        );
    }

    /**
     * @test
     *
     * @dataProvider provideEnsureAllStringsAreWrappedInCases
     */
    public function array_map_recursive(): void
    {
        $haystack = [
            'x',
            [
                'y',
                'z',
            ],
            'w',
        ];
        $expected = [
            'a',
            [
                'a',
                'a',
            ],
            'a',
        ];

        self::assertSame($expected, ArrayHelper::arrayMapRecursive($haystack, fn () => 'a'));
        self::assertSame(
            [
                'x',
                [
                    'y',
                    'z',
                ],
                'w',
            ],
            $haystack
        );
    }

    /**
     * @test
     *
     * @dataProvider provideArraysForSettingThatCreatesPath
     */
    public function it_sets_value_in_path_and_creates_path_if_it_does_not_exist(
        array $array,
        string $path,
        $value,
        array $expectedResult
    ): void {
        self::assertEquals($expectedResult, ArrayHelper::set($array, $path, $value, true));
    }

    public static function provideArraysForSettingThatCreatesPath()
    {
        return [
            [['a' => 'a'], 'b', 'b', ['a' => 'a', 'b' => 'b']],
            [['a' => 'a'], 'b' . DIRECTORY_SEPARATOR . 'c' . DIRECTORY_SEPARATOR . 'd', 'b', ['a' => 'a', 'b' => ['c' => ['d' => 'b']]]],
        ];
    }

    /**
     * @test
     */
    public function it_throws_exception_if_setting_in_path_that_does_not_exist(): void
    {
        $array = ['a' => 'a'];
        $path = 'b' . DIRECTORY_SEPARATOR . 'c' . DIRECTORY_SEPARATOR . 'd';
        $value = 'b';

        self::expectException(OutOfBoundsException::class);
        ArrayHelper::set($array, $path, $value);
    }

    /**
     * @test
     */
    public function it_can_get_from_path(): void
    {
        $array = ['a' => 'a', 'b' => ['c' => ['d' => 'b']]];
        $path = 'b' . DIRECTORY_SEPARATOR . 'c' . DIRECTORY_SEPARATOR . 'd';
        $expectedValue = 'b';

        self::assertEquals($expectedValue, ArrayHelper::get($array, $path));
    }

    /**
     * @test
     */
    public function it_throws_exception_if_getting_from_path_that_does_not_exist(): void
    {
        $array = ['a' => 'a'];
        $path = 'b' . DIRECTORY_SEPARATOR . 'c' . DIRECTORY_SEPARATOR . 'd';
        $expectedValue = 'b';

        self::expectException(OutOfBoundsException::class);
        ArrayHelper::get($array, $path);
    }

    /**
     * @test
     */
    public function it_can_unset_path(): void
    {
        $array = ['a' => 'a', 'b' => ['c' => ['d' => 'b']]];
        $path = 'b' . DIRECTORY_SEPARATOR . 'c';
        $expectedValue = ['a' => 'a', 'b' => []];

        self::assertEquals($expectedValue, ArrayHelper::unset($array, $path));
    }

    /**
     * @test
     */
    public function it_throws_exception_if_unsetting_path_that_does_not_exist(): void
    {
        $array = ['a' => 'a'];
        $path = 'b' . DIRECTORY_SEPARATOR . 'c';

        self::expectException(OutOfBoundsException::class);
        ArrayHelper::unset($array, $path);
    }

    /**
     * @test
     */
    public function it_knows_if_path_exists(): void
    {
        $array = ['a' => 'a', 'b' => ['c' => ['d' => 'b']]];
        $existingPath1 = 'b' . DIRECTORY_SEPARATOR . 'c';
        $existingPath2 = 'b' . DIRECTORY_SEPARATOR . 'c' . DIRECTORY_SEPARATOR . 'd';
        $nonExistingPath = 'b' . DIRECTORY_SEPARATOR . 'z';

        self::assertTrue(ArrayHelper::has($array, $existingPath1));
        self::assertTrue(ArrayHelper::has($array, $existingPath2));
        self::assertFalse(ArrayHelper::has($array, $nonExistingPath));
    }

    /**
     * @test
     *
     * @param string[] $haystack
     * @param string[] $needle
     *
     * @dataProvider provideSubsetTestData
     */
    public function it_should_know_if_an_array_contains_another_array(
        array $haystack,
        array $needle,
        bool $expected
    ): void {
        self::assertEquals($expected, ArrayHelper::containsSubset($needle, $haystack));
    }

    /**
     * @return array<
     *      array-key,
     *      array{0: array<array-key, string>, 1: array<array-key, string>, 2: bool}
     * >
     */
    public static function provideSubsetTestData(): array
    {
        return [
            [['a', 'b', 'c', 'd', 'e', 'f'], ['c', 'd', 'e'], true],
            [['a', 'b', 'c', 'd', 'e', 'f'], ['c', 'z', 'e'], false],
        ];
    }

    /**
     * @test
     *
     * @param string[] $haystack
     *
     * @dataProvider provideGlobPatters
     */
    public function it_should_match_on_glob_patterns(
        string $needle,
        array $haystack,
        bool $expected
    ): void {
        self::assertEquals($expected, ArrayHelper::containsGlobMatch($needle, ...$haystack));
    }

    /**
     * @return array<
     *      array-key,
     *      array{0: string, 1: array<array-key, string>, 2: bool}
     * >
     */
    public static function provideGlobPatters(): array
    {
        return [
            ['prod.globalticket.com', ['prod.globalticket.com'], true],
            ['*.prod.globalticket.com', ['match.prod.globalticket.com'], true],
            ['match.prod.globalticket.com', ['*.prod.globalticket.com'], true],

            ['prod.globalticket.com', ['xxx', 'prod.globalticket.com'], true],
            ['*.prod.globalticket.com', ['xxx', 'match.prod.globalticket.com'], true],
            ['match.prod.globalticket.com', ['xxx', '*.prod.globalticket.com'], true],

            ['prod.globalticket.com', ['xxx', 'prod.xxx.com'], false],
            ['*.prod.globalticket.com', ['xxx', 'match.prod.xxx.com'], false],
            ['match.prod.globalticket.com', ['xxx', '*.prod.xxx.com'], false],
        ];
    }

    /**
     * @test
     *
     * @param string[] $array
     *
     * @dataProvider provideArrayPaths
     */
    public function it_should_know_if_a_path_exists_in_an_array(string $path, array $array, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, ArrayHelper::containsPath($path, $array));
    }

    /**
     * @return array<
     *     array{
     *          string,
     *          array{
     *              string,
     *              array<array{string}>
     *          },
     *          bool
     *      }
     * >
     */
    public static function provideArrayPaths(): array
    {
        /** @phpstan-ignore-next-line I don't know how to make the doc block correctly match the actual return */
        return [
            [
                'b' . ArrayHelper::DEFAULT_PATH_SEPARATOR . 'c' . ArrayHelper::DEFAULT_PATH_SEPARATOR . 'd',
                ['a' => 'a', 'b' => ['c' => ['d' => 'b']]],
                true,
            ],
            [
                'a' . ArrayHelper::DEFAULT_PATH_SEPARATOR . 'c' . ArrayHelper::DEFAULT_PATH_SEPARATOR . 'd',
                ['a' => 'a', 'b' => ['c' => ['d' => 'b']]],
                false,
            ],
        ];
    }
}
