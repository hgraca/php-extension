<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Iterator;

use Hgraca\PhpExtension\Iterator\FileObjectStringIteratorDecorator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use SplFileObject;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class FileObjectStringIteratorDecoratorTest extends AbstractTest
{
    private const FILE_PATH = '/tmp/FileUploaderTest9/TestFile9.txt';

    /**
     * @test
     */
    public function it_should_return_first_element(): void
    {
        $this->setUpFile();
        $file = new SplFileObject(self::FILE_PATH, 'r');
        $file->setFlags(SplFileObject::READ_AHEAD);
        $iteratorDecorator = new FileObjectStringIteratorDecorator($file);

        self::assertEquals("ab\n", $iteratorDecorator->current());

        unset($file);
        $this->tearDown();
    }

    /**
     * @test
     */
    public function it_should_move_through_iterator(): void
    {
        $this->setUpFile();
        $file = new SplFileObject(self::FILE_PATH, 'r');
        $file->setFlags(SplFileObject::READ_AHEAD);
        $iteratorDecorator = new FileObjectStringIteratorDecorator($file);

        self::assertEquals("ab\n", $iteratorDecorator->current());
        $iteratorDecorator->next();
        self::assertEquals("cd\n", $iteratorDecorator->current());
        $iteratorDecorator->next();
        self::assertEquals("ef\n", $iteratorDecorator->current());

        unset($file);
        $this->tearDown();
    }

    /**
     * @test
     */
    public function it_should_rewind(): void
    {
        $this->setUpFile();
        $file = new SplFileObject(self::FILE_PATH, 'r');
        $file->setFlags(SplFileObject::READ_AHEAD);
        $iteratorDecorator = new FileObjectStringIteratorDecorator($file);

        $iteratorDecorator->next();
        $iteratorDecorator->next();
        $iteratorDecorator->next();

        self::assertEquals("ef\n", $iteratorDecorator->current());
        $iteratorDecorator->rewind();
        self::assertEquals("ab\n", $iteratorDecorator->current());

        unset($file);
        $this->tearDown();
    }

    /**
     * @test
     */
    public function it_should_know_when_eof_is_reached(): void
    {
        $this->setUpFile();
        $file = new SplFileObject(self::FILE_PATH, 'r');
        $file->setFlags(SplFileObject::READ_AHEAD);
        $iteratorDecorator = new FileObjectStringIteratorDecorator($file);
        $iteratorDecorator->rewind();

        self::assertTrue($iteratorDecorator->valid());

        $iteratorDecorator->next();
        $iteratorDecorator->next();
        $iteratorDecorator->next();
        $iteratorDecorator->next();

        self::assertFalse($iteratorDecorator->valid());

        $this->tearDown();
    }

    /**
     * @test
     */
    public function it_should_return_key(): void
    {
        $this->setUpFile();
        $file = new SplFileObject(self::FILE_PATH, 'r');
        $file->setFlags(SplFileObject::READ_AHEAD);
        $iteratorDecorator = new FileObjectStringIteratorDecorator($file);

        self::assertEquals(0, $iteratorDecorator->key());

        $iteratorDecorator->next();
        $iteratorDecorator->next();

        self::assertEquals(2, $iteratorDecorator->key());

        $this->tearDown();
    }

    private function setUpFile(): void
    {
        mkdir(dirname(self::FILE_PATH));
        $file = fopen(self::FILE_PATH, 'w');
        fwrite($file, "ab\ncd\nef\n");
        fclose($file);
    }

    protected function tearDown(): void
    {
        is_file(self::FILE_PATH)
        && unlink(self::FILE_PATH);

        is_dir(dirname(self::FILE_PATH)) && rmdir(dirname(self::FILE_PATH));
    }
}
