<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Iterator;

use ArrayIterator;
use Hgraca\PhpExtension\Iterator\ArrayToCsvIteratorDecorator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class ArrayToCsvIteratorDecoratorTest extends AbstractTest
{
    private const DATA_LIST = [
        [
            'a' => 1,
            'b' => 2,
        ],
        [
            'a' => 3,
            'b' => 4,
        ],
    ];

    /**
     * @test
     */
    public function it_should_returns_the_first_element(): void
    {
        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST));
        self::assertEquals("a,b\n", $decorator->current());

        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST), false);
        self::assertEquals("1,2\n", $decorator->current());
    }

    /**
     * @test
     */
    public function it_should_move_through_the_data_list(): void
    {
        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST));

        self::assertEquals("a,b\n", $decorator->current());
        $decorator->next();
        self::assertEquals("1,2\n", $decorator->current());
        $decorator->next();
        self::assertEquals("3,4\n", $decorator->current());
    }

    /**
     * @test
     */
    public function it_should_rewind(): void
    {
        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST));

        $decorator->next();
        $decorator->next();
        self::assertEquals("3,4\n", $decorator->current());
        $decorator->rewind();
        self::assertEquals("a,b\n", $decorator->current());
    }

    /**
     * @test
     */
    public function it_should_know_when_an_item_is_valid(): void
    {
        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST));

        self::assertTrue($decorator->valid());
        $decorator->next();
        $decorator->next();
        $decorator->next();
        self::assertFalse($decorator->valid());
    }

    /**
     * @test
     */
    public function it_should_return_the_element_key(): void
    {
        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST));
        self::assertEquals('header', $decorator->key());
        $decorator->next();
        self::assertEquals('0', $decorator->key());
        $decorator->next();
        self::assertEquals('1', $decorator->key());

        $decorator = new ArrayToCsvIteratorDecorator(new ArrayIterator(self::DATA_LIST), false);
        self::assertEquals('0', $decorator->key());
        $decorator->next();
        self::assertEquals('1', $decorator->key());
    }
}
