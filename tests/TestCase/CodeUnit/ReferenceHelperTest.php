<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit;

use Hgraca\PhpExtension\CodeUnit\ReferenceHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\DummyObject;
use ReflectionException;
use stdClass;

use function json_encode;

use const JSON_PRETTY_PRINT;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class ReferenceHelperTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideObjectsAndArrays
     */
    public function objects_and_array_references_are_compared_correctly(mixed &$a, mixed &$b, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, ReferenceHelper::isSame($a, $b));
    }

    /**
     * @return array<array{mixed, mixed, bool}>
     */
    public static function provideObjectsAndArrays(): array
    {
        $x = new stdClass();
        $y = $x;
        $z = new stdClass();
        $a = ['foo'];
        $b = ['foo'];
        $c = &$a;
        $d = $a;

        return [
            [&$x, &$x, true],
            [&$x, &$y, true],
            [&$x, &$z, false],
            [&$a, &$a, true],
            [&$a, &$b, false],
            [&$a, &$c, true],
            [&$a, &$d, false],
        ];
    }

    /**
     * @test
     *
     * @param DummyObject|array<string, DummyObject|array<string, array<string, array<string,DummyObject>|DummyObject>|DummyObject>|DummyObject> $tree
     * @param string|array<string, DummyObject> $expectedResult
     *
     * @dataProvider providePropertyTree
     *
     * @throws ReflectionException
     */
    public function extract_deeply_nested_properties_from_objects_or_arrays(
        DummyObject|array $tree,
        string $path,
        string|array $expectedResult
    ): void {
        self::assertEquals(
            $expectedResult,
            ReferenceHelper::getNestedProperty($path, $tree),
            "path: $path \ntree: \n" . json_encode($tree, JSON_PRETTY_PRINT)
        );
    }

    /**
     * @return array<string, array{DummyObject|array<string, DummyObject|array<string, array<string, array<string,DummyObject>|DummyObject>|DummyObject>|DummyObject>, string, string|array<string, DummyObject>}>
     */
    public static function providePropertyTree(): array
    {
        $tree1 = [
            'xxx' => new DummyObject(
                new DummyObject(
                    [
                        'yyy' => [
                            'zzz' => new DummyObject(
                                'mmm'
                            ),
                        ],
                    ]
                )
            ),
            'yyy' => [
                'zzz' => new DummyObject('nnn'),
            ],
            'zzz' => [
                'xxx' => [
                    'zzz' => new DummyObject('ppp'),
                ],
            ],
        ];
        $tree2 = new DummyObject(
            [
                'xxx' => [
                    'zzz' => new DummyObject('nnn'),
                ],
                'yyy' => new DummyObject(
                    [
                        'yyy' => [
                            'zzz' => new DummyObject('mmm'),
                        ],
                    ]
                ),
            ]
        );

        return [
            'test 1' => [
                $tree1,
                'xxx.ddd.ddd.yyy.zzz.ddd',
                'mmm',
            ],
            'test 2' => [
                $tree1,
                'yyy.zzz.aaa',
                'aaa',
            ],
            'test 3' => [
                $tree1,
                'yyy.zzz.bbb',
                'bbb',
            ],
            'test 4' => [
                $tree2,
                'ddd.yyy.ddd.yyy',
                ['zzz' => new DummyObject('mmm')],
            ],
        ];
    }
}
