<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit;

use Hgraca\PhpExtension\CodeUnit\ObjectHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\ChildClass;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\ChildClassFromAbstractParent;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\DummyClass;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\FinalChildClass;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\FinalChildClassFromAbstractParent;
use InvalidArgumentException;
use ReflectionException;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class ObjectHelperTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideClassNames
     */
    public function set_properties_from_key_value(string $className): void
    {
        $properties = [
            'parentPrivate' => 'a',
            'parentProtected' => 'b',
            'parentPublic' => null,
            'childPrivate' => 'd',
            'childProtected' => 'e',
            'childPublic' => 'f',
            'parentPrivateStatic' => 'g',
            'childPrivateStatic' => 'h',
        ];

        /** @var ChildClass|FinalChildClass|ChildClassFromAbstractParent|FinalChildClassFromAbstractParent $object */
        $object = new $className();

        ObjectHelper::setPropertiesFromKeyValue($object, $properties);

        self::assertEquals($properties['parentPrivate'], $object->getParentPrivate());
        self::assertEquals($properties['parentProtected'], $object->getParentProtected());
        self::assertEquals($properties['parentPublic'], $object->getParentPublic());
        self::assertEquals($properties['childPrivate'], $object->getChildPrivate());
        self::assertEquals($properties['childProtected'], $object->getChildProtected());
        self::assertEquals($properties['childPublic'], $object->getChildPublic());
        self::assertEquals($properties['parentPrivateStatic'], $object->getParentPrivateStatic());
        self::assertEquals($properties['childPrivateStatic'], $object->getChildPrivateStatic());
    }

    /**
     * @test
     *
     * @dataProvider provideClassNamesWithNullableProperty
     */
    public function set_property_null_if_not_provided_and_is_nullable(string $className): void
    {
        $properties = [
            'parentPrivate' => 'a',
            'parentProtected' => 'b',
            'parentPublic' => 'c',
            'childPrivate' => 'd',
            'childProtected' => 'e',
            'childPublic' => 'f',
            'parentPrivateStatic' => 'g',
        ];

        /** @var ChildClass|FinalChildClass|ChildClassFromAbstractParent|FinalChildClassFromAbstractParent $object */
        $object = new $className();

        ObjectHelper::setPropertiesFromKeyValue($object, $properties);

        self::assertEquals($properties['parentPrivate'], $object->getParentPrivate());
        self::assertEquals($properties['parentProtected'], $object->getParentProtected());
        self::assertEquals($properties['parentPublic'], $object->getParentPublic());
        self::assertEquals($properties['childPrivate'], $object->getChildPrivate());
        self::assertEquals($properties['childProtected'], $object->getChildProtected());
        self::assertEquals($properties['childPublic'], $object->getChildPublic());
        self::assertEquals($properties['parentPrivateStatic'], $object->getParentPrivateStatic());
        self::assertEquals(null, $object->getChildPrivateStatic());
    }

    /**
     * @test
     */
    public function throws_exception_if_property_not_provided_and_not_nullable(): void
    {
        $className = ChildClass::class;

        $properties = [
            'parentPrivate' => 'a',
            'parentProtected' => 'b',
            'parentPublic' => 'c',
            'childPrivate' => 'd',
            'childProtected' => 'e',
            'childPublic' => 'f',
            'parentPrivateStatic' => 'g',
        ];

        /** @var ChildClass|FinalChildClass|ChildClassFromAbstractParent|FinalChildClassFromAbstractParent $object */
        $object = new $className();

        $this->expectException(InvalidArgumentException::class);
        ObjectHelper::setPropertiesFromKeyValue($object, $properties);
    }

    /**
     * @test
     *
     * @dataProvider provideClassNames
     */
    public function get_properties_as_key_value(string $className): void
    {
        /** @var ChildClass|FinalChildClass|ChildClassFromAbstractParent|FinalChildClassFromAbstractParent $object */
        $object = new $className('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');

        $actualProperties = ObjectHelper::getPropertiesAsKeyValue($object);

        self::assertEquals($object->getParentPrivate(), $actualProperties['parentPrivate']);
        self::assertEquals($object->getParentProtected(), $actualProperties['parentProtected']);
        self::assertEquals($object->getParentPublic(), $actualProperties['parentPublic']);
        self::assertEquals($object->getChildPrivate(), $actualProperties['childPrivate']);
        self::assertEquals($object->getChildProtected(), $actualProperties['childProtected']);
        self::assertEquals($object->getChildPublic(), $actualProperties['childPublic']);
        self::assertEquals($object->getParentPrivateStatic(), $actualProperties['parentPrivateStatic']);
        self::assertEquals($object->getChildPrivateStatic(), $actualProperties['childPrivateStatic']);
    }

    /**
     * @return array<array{class-string}>
     */
    public static function provideClassNames(): array
    {
        return [
            [ChildClass::class],
            [FinalChildClass::class],
            [ChildClassFromAbstractParent::class],
            [FinalChildClassFromAbstractParent::class],
        ];
    }

    /**
     * @return array<array{class-string}>
     */
    public static function provideClassNamesWithNullableProperty(): array
    {
        return [
            [FinalChildClass::class],
            [ChildClassFromAbstractParent::class],
            [FinalChildClassFromAbstractParent::class],
        ];
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public static function invokeProtectedMethod_works_with_protected_methods(): void
    {
        $var = 100;
        $dummyObject = new DummyClass($var);

        self::assertEquals($var, ObjectHelper::invokeProtectedMethod($dummyObject, 'getVarProtected'));
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public static function invokeProtectedMethod_works_with_private_methods(): void
    {
        $var = 120;
        $dummyObject = new DummyClass($var);

        self::assertEquals($var, ObjectHelper::invokeProtectedMethod($dummyObject, 'getVarPrivate'));
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function set_protected_property(): void
    {
        $newValue = 'something new';
        $object = new DummyClass();
        self::assertNotSame($newValue, $object->getTestProperty());

        ObjectHelper::setProtectedProperty($object, 'testProperty', $newValue);
        self::assertSame($newValue, $object->getTestProperty());
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function set_protected_property_defined_in_parent_class(): void
    {
        $newValue = 'something new';
        $object = new DummyClass();
        self::assertNotSame($newValue, $object->getParentTestProperty());

        ObjectHelper::setProtectedProperty($object, 'parentTestProperty', $newValue);
        self::assertSame($newValue, $object->getParentTestProperty());
    }

    /**
     * @test
     */
    public function set_protected_property_fails_when_cant_find_the_property(): void
    {
        $this->expectException(ReflectionException::class);

        $object = new DummyClass();
        ObjectHelper::setProtectedProperty($object, 'i_dont_exist', 'non existent');
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function get_protected_property_from_object_class(): void
    {
        $value = 7;
        $object = new DummyClass($value);

        self::assertSame($value, ObjectHelper::getProtectedProperty($object, 'var'));
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function get_protected_property_from_object_parent_class(): void
    {
        $value = 7;
        $parentValue = 19;
        $object = new DummyClass($value, $parentValue);

        self::assertSame($parentValue, ObjectHelper::getProtectedProperty($object, 'parentVar'));
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function get_protected_property_throws_exception_if_not_found(): void
    {
        $this->expectException(ReflectionException::class);

        $object = new DummyClass();

        ObjectHelper::getProtectedProperty($object, 'inexistentVar');
    }
}
