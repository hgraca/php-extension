<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit;

use Hgraca\PhpExtension\CodeUnit\ClassHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\DummyClass;
use ReflectionException;
use Throwable;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class ClassHelperTest extends AbstractTest
{
    /**
     * @test
     */
    public function extract_namespace(): void
    {
        self::assertSame('Hgraca\\PhpExtension\\Test\\TestCase\\CodeUnit', ClassHelper::extractNamespace(self::class));
    }

    /**
     * @test
     */
    public function extract_canonical_class_name(): void
    {
        self::assertSame('ClassHelperTest', ClassHelper::extractCanonicalClassName(self::class));
    }

    /**
     * @test
     */
    public function extract_canonical_method_name(): void
    {
        self::assertSame(
            'extract_canonical_method_name',
            ClassHelper::extractCanonicalMethodName(__METHOD__)
        );
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function instantiate_without_constructor_does_not_use_the_constructor(): void
    {
        $object = ClassHelper::instantiateWithoutConstructor(DummyClass::class);
        $this->expectException(Throwable::class);
        $object->getAnotherVar();
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function it_should_get_all_method_parameters(): void
    {
        self::assertSame(
            [
                [
                    'type' => 'int',
                    'name' => 'var',
                ],
                [
                    'type' => 'int',
                    'name' => 'parentVar',
                ],
            ],
            ClassHelper::getClassMethodParameterList(DummyClass::class, '__construct')
        );
    }
}
