<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit;

use Hgraca\PhpExtension\CodeUnit\TypeHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects\DummyClass;

/**
 * @small
 *
 * @internal
 */
final class TypeHelperTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideValues
     */
    public function get_type(mixed $value, string $expectedType): void
    {
        self::assertEquals($expectedType, TypeHelper::getType($value));
    }

    /**
     * @return array<array{mixed, string}>
     */
    public static function provideValues(): array
    {
        return [
            [true, 'bool'],
            [1, 'int'],
            [1.2, 'float'],
            ['', 'string'],
            [[], 'array'],
            [[1, 2], 'array'],
            [[[1], [2]], 'array'],
            [null, 'null'],
            [new DummyClass(), DummyClass::class],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideFullyQualifiedNames
     */
    public function correctly_validates_fully_qualified_names(string $string, bool $expected): void
    {
        self::assertEquals($expected, TypeHelper::isValidFQCN($string));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideFullyQualifiedNames(): array
    {
        return [
            [self::class, true],
            ['\\' . self::class, true],
            [self::class . '\\', false],
            ['\\' . self::class . '\\', false],
            ['4site', true],
            ['_4site', true],
            ['täyte', true],
            ['boolean', false],
            ['bool', false],
            ['integer', false],
            ['int', false],
            ['float', false],
            ['string', false],
            ['array', false],
            ['object', false],
            ['callable', false],
            ['iterable', false],
            ['resource', false],
            ['null', false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideLabels
     */
    public function correctly_validates_labels(string $string, bool $expected): void
    {
        self::assertEquals($expected, TypeHelper::isValidLabel($string));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideLabels(): array
    {
        return [
            [self::class, true],
            ['\\' . self::class, true],
            ['4site', true],
            ['_4site', true],
            ['täyte', true],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideScalars
     */
    public function correctly_validates_scalar_types(string $string, bool $expected): void
    {
        self::assertEquals($expected, TypeHelper::isScalarType($string));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideScalars(): array
    {
        return [
            ['boolean', true],
            ['bool', true],
            ['integer', true],
            ['int', true],
            ['float', true],
            ['string', true],
            ['array', false],
            ['object', false],
            ['callable', false],
            ['iterable', false],
            ['resource', false],
            ['null', false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideCompounds
     */
    public function correctly_validates_compound_types(string $string, bool $expected): void
    {
        self::assertEquals($expected, TypeHelper::isCompoundType($string));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideCompounds(): array
    {
        return [
            ['boolean', false],
            ['bool', false],
            ['integer', false],
            ['int', false],
            ['float', false],
            ['string', false],
            ['array', true],
            ['object', true],
            ['callable', true],
            ['iterable', true],
            ['resource', false],
            ['null', false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideSpecials
     */
    public function correctly_validates_special_types(string $string, bool $expected): void
    {
        self::assertEquals($expected, TypeHelper::isSpecialType($string));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideSpecials(): array
    {
        return [
            ['boolean', false],
            ['bool', false],
            ['integer', false],
            ['int', false],
            ['float', false],
            ['string', false],
            ['array', false],
            ['object', false],
            ['callable', false],
            ['iterable', false],
            ['resource', true],
            ['null', true],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideNatives
     */
    public function correctly_validates_native_types(string $string, bool $expected): void
    {
        self::assertEquals($expected, TypeHelper::isNativeType($string));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideNatives(): array
    {
        return [
            ['boolean', true],
            ['bool', true],
            ['integer', true],
            ['int', true],
            ['float', true],
            ['string', true],
            ['array', true],
            ['object', true],
            ['callable', true],
            ['iterable', true],
            ['resource', true],
            ['null', true],
        ];
    }
}
