<?php

declare(strict_types=1);

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects;

use Hgraca\PhpExtension\CodeUnit\AbstractClassesOneOnOneMapper;

/**
 * @extends AbstractClassesOneOnOneMapper<DummyClass, DummyObject>
 */
final readonly class TestClassesOneOnOneMapper extends AbstractClassesOneOnOneMapper
{
}