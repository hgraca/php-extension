<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects;

final class DummyClass extends DummyClassParent
{
    private string $testProperty = 'FooBar';

    /** @var int|null */
    private int $anotherVar;

    public function __construct(private int $var = 1, int $parentVar = 2)
    {
        parent::__construct($parentVar);

        $this->anotherVar = 1;
    }

    public function getTestProperty(): string
    {
        return $this->testProperty;
    }

    public function getAnotherVar(): ?int
    {
        return $this->anotherVar;
    }

    protected function getVarProtected(): int
    {
        return $this->var;
    }

    private function getVarPrivate(): ?int
    {
        return $this->var;
    }
}
