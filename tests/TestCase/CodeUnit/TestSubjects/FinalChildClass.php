<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects;

final class FinalChildClass extends ParentClass
{
    /** @var string|null */
    private static ?string $childPrivateStatic;

    public function __construct(
        string $parentPrivate = '1',
        string $parentProtected = '2',
        string $parentPublic = '3',
        private string $childPrivate = '4',
        protected string $childProtected = '5',
        public string $childPublic = '6',
        string $parentPrivateStatic = '7',
        string $childPrivateStatic = '8'
    ) {
        parent::__construct($parentPrivate, $parentProtected, $parentPublic, $parentPrivateStatic);
        self::$childPrivateStatic = $childPrivateStatic;
    }

    public function getChildPrivate(): string
    {
        return $this->childPrivate;
    }

    public function getChildProtected(): string
    {
        return $this->childProtected;
    }

    public function getChildPublic(): string
    {
        return $this->childPublic;
    }

    public function getChildPrivateStatic(): ?string
    {
        return self::$childPrivateStatic;
    }
}
