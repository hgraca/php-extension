<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\CodeUnit\TestSubjects;

class ParentClass
{
    /** @var string */
    protected $parentProtected;

    /** @var ?string */
    public $parentPublic;

    private static string $parentPrivateStatic;

    public function __construct(private string $parentPrivate, string $parentProtected, ?string $parentPublic, string $parentPrivateStatic)
    {
        $this->parentProtected = $parentProtected;
        $this->parentPublic = $parentPublic;
        self::$parentPrivateStatic = $parentPrivateStatic;
    }

    public function getParentPrivate(): string
    {
        return $this->parentPrivate;
    }

    public function getParentProtected(): string
    {
        return $this->parentProtected;
    }

    public function getParentPublic(): ?string
    {
        return $this->parentPublic;
    }

    public function getParentPrivateStatic(): string
    {
        return self::$parentPrivateStatic;
    }
}
