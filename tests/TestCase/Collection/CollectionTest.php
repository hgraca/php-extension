<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Collection;

use Hgraca\PhpExtension\Collection\Collection;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

use function count;

/**
 * @internal
 *
 * @small
 */
final class CollectionTest extends AbstractTest
{
    /**
     * @test
     *
     * @param int[] $list
     *
     * @dataProvider listProvider
     */
    public function child_classes_can_rely_on_internal_array(array $list): void
    {
        new class(...$list) extends Collection {
            public function __construct(int ...$list)
            {
                foreach ($list as $item) {
                    $this->itemList[] = $item + 1;
                }
            }
        };

        self::assertTrue(true); // we just need to make sure the above code works
    }

    /**
     * @test
     */
    public function can_be_iterated(): void
    {
        $itemList = [1, 2, 3];
        $collection = new Collection($itemList);

        $count = 0;
        foreach ($collection as $item) {
            ++$count;
        }

        self::assertEquals(count($itemList), $count);
    }

    /**
     * @test
     */
    public function can_be_counted(): void
    {
        $itemList = [1, 2, 3];
        $collection = new Collection($itemList);

        self::assertEquals(count($itemList), $collection->count());
    }

    /**
     * @test
     *
     * @param int[] $itemList
     *
     * @dataProvider listProvider
     */
    public function knows_if_is_empty(array $itemList, bool $expectedResult): void
    {
        $collection = new Collection($itemList);

        self::assertEquals($expectedResult, $collection->isEmpty());
    }

    /**
     * @return array<array{array<int>, bool}>
     */
    public static function listProvider(): array
    {
        return [
            [[1, 2, 3], false],
            [[], true],
        ];
    }
}
