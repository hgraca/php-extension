<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\DateTime;

use Hgraca\PhpExtension\DateTime\Time;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use InvalidArgumentException;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class TimeTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_create_time_object(): void
    {
        $hr = 10;
        $min = 9;
        $sec = 20;

        $time = new Time($hr, $min, $sec);

        self::assertEquals($time->getHours(), $hr);
        self::assertEquals($time->getMinutes(), $min);
        self::assertEquals($time->getSeconds(), $sec);
    }

    /**
     * @test
     */
    public function it_should_create_time_object_from_parsed_string(): void
    {
        $hr = 10;
        $min = 9;
        $sec = 20;

        $time = Time::parse(sprintf('%02d:%02d:%02d', $hr, $min, $sec));

        self::assertEquals($time->getHours(), $hr);
        self::assertEquals($time->getMinutes(), $min);
        self::assertEquals($time->getSeconds(), $sec);
    }

    /**
     * @test
     */
    public function it_should_return_time_string(): void
    {
        $hr = 10;
        $min = 9;
        $sec = 20;

        $time = new Time($hr, $min, $sec);
        self::assertEquals($time->toString(), sprintf('%02d:%02d:%02d', $hr, $min, $sec));
    }

    /**
     * @test
     *
     * @dataProvider invalidTimeArgumentsProvider
     */
    public function it_should_throw_exception_for_invalid_arguments(int $hr, int $min, int $sec): void
    {
        self::expectException(InvalidArgumentException::class);
        new Time($hr, $min, $sec);
    }

    public static function invalidTimeArgumentsProvider(): array
    {
        return [
            'invalid hour' => [29, 9, 20],
            'invalid negative hour' => [-5, 9, 20],
            'invalid minute' => [22, 75, 20],
            'invalid negative minute' => [22, -5, 20],
            'invalid second' => [22, 9, 75],
            'invalid negative second' => [22, 9, -5],
            'check top boundary for hours' => [24, 0, 0],
            'check top boundary for minutes' => [23, 60, 0],
            'check top boundary for seconds' => [23, 59, 60],
        ];
    }
}
