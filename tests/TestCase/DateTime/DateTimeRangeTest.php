<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\DateTime;

use DateInterval;
use DateTimeImmutable;
use Hgraca\PhpExtension\DateTime\DateTimeRange;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class DateTimeRangeTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideDaysIntervalsWithDirection
     */
    public function it_should_know_if_its_going_forward(
        DateTimeRange $dateTimeInterval,
        bool $expectedIsGoingForward
    ): void {
        self::assertEquals($expectedIsGoingForward, $dateTimeInterval->isGoingForward());
    }

    /**
     * @return array<
     *      int,
     *      array{
     *          DateTimeRange,
     *          bool
     *      }
     * >
     */
    public static function provideDaysIntervalsWithDirection(): array
    {
        return [
            [new DateTimeRange(new DateTimeImmutable('yesterday'), new DateTimeImmutable('tomorrow')), true],
            [new DateTimeRange(new DateTimeImmutable('tomorrow'), new DateTimeImmutable('yesterday')), false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideDaysIntervalsForDaysCountCheck
     */
    public function it_should_know_how_many_days_it_contains(DateTimeRange $dateTimeInterval, int $expected): void
    {
        self::assertEquals($expected, $dateTimeInterval->getDaysDifference());
    }

    /**
     * @return array<
     *      int,
     *      array{
     *          DateTimeRange,
     *          int
     *      }
     * >
     */
    public static function provideDaysIntervalsForDaysCountCheck(): array
    {
        return [
            [new DateTimeRange(new DateTimeImmutable('yesterday'), new DateTimeImmutable('tomorrow')), 2],
            [new DateTimeRange(new DateTimeImmutable('tomorrow'), new DateTimeImmutable('yesterday')), 2],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideDaysIntervalsForContainsCheck
     */
    public function it_should_know_if_it_contains_date(DateTimeRange $dateTimeInterval, DateTimeImmutable $needle, bool $expected): void
    {
        self::assertEquals($expected, $dateTimeInterval->contains($needle));
    }

    /**
     * @return array<
     *      int,
     *      array{
     *          DateTimeRange,
     *          DateTimeImmutable,
     *          bool
     *      }
     * >
     */
    public static function provideDaysIntervalsForContainsCheck(): array
    {
        return [
            [new DateTimeRange(new DateTimeImmutable('yesterday'), new DateTimeImmutable('tomorrow')), new DateTimeImmutable(), true],
            [new DateTimeRange(new DateTimeImmutable('yesterday'), new DateTimeImmutable('tomorrow')), new DateTimeImmutable('yesterday'), true],
            [new DateTimeRange(new DateTimeImmutable('yesterday'), new DateTimeImmutable('tomorrow')), new DateTimeImmutable('tomorrow'), true],
            [new DateTimeRange(new DateTimeImmutable('tomorrow'), new DateTimeImmutable('yesterday')), (new DateTimeImmutable())->add(new DateInterval('P3D')), false],
            [new DateTimeRange(new DateTimeImmutable('tomorrow'), new DateTimeImmutable('yesterday')), (new DateTimeImmutable())->sub(new DateInterval('P3D')), false],
        ];
    }
}
