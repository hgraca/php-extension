<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\DateTime\Schedule;

use Hgraca\PhpExtension\DateTime\Schedule\TimeRange;
use Hgraca\PhpExtension\DateTime\Time;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use InvalidArgumentException;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class TimeRangeTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_create_a_time_range_object(): void
    {
        $from = Time::parse('09:00');
        $until = Time::parse('17:00');

        $timeRange = new TimeRange(
            $from,
            $until
        );

        self::assertEquals($timeRange->getFrom(), $from);
        self::assertEquals($timeRange->getUntil(), $until);
    }

    /**
     * @test
     */
    public function it_should_throw_exception_when_arguments_are_invalid(): void
    {
        self::expectException(InvalidArgumentException::class);

        new TimeRange(
            Time::parse('17:00'),
            Time::parse('09:00')
        );
    }
}
