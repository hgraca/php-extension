<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\DateTime;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\DateTime\DateTimeHelper;
use Hgraca\PhpExtension\DateTime\WeekdayEnum;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class DateTimeHelperTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideStringDateTime
     */
    public function can_create_date_time_from_string(string $stringDateTime, string $format): void
    {
        $datetime = DateTimeHelper::fromStringToDateTimeImmutable($stringDateTime, $format);

        self::assertInstanceOf(DateTimeImmutable::class, $datetime);
        self::assertEquals($stringDateTime, $datetime->format($format));
    }

    public static function provideStringDateTime(): array
    {
        return [
            ['2020-01-01', 'Y-m-d'],
            ['2020-01-01 19:21:32', 'Y-m-d H:i:s'],
            ['01-01-2020', 'd-m-Y'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideDateTime
     */
    public function cate_create_string_from_date_time(DateTimeInterface $dateTimeImmutable, string $format, string $stringDateTime): void
    {
        self::assertEquals($stringDateTime, DateTimeHelper::fromDateTimeToString($dateTimeImmutable, $format));
    }

    public static function provideDateTime(): array
    {
        return [
            [DateTimeImmutable::createFromFormat('Y-m-d', '2020-01-01', new DateTimeZone('Europe/Amsterdam')), 'Y-m-d', '2020-01-01'],
            [DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2018-10-21 19:21:32', new DateTimeZone('Europe/Amsterdam')), 'Y-m-d H:i:s', '2018-10-21 19:21:32'],
            [DateTimeImmutable::createFromFormat('d-m-Y', '21-10-2018', new DateTimeZone('Europe/Amsterdam')), 'd-m-Y', '21-10-2018'],
            [DateTime::createFromFormat('Y-m-d', '2020-01-01', new DateTimeZone('Europe/Amsterdam')), 'Y-m-d', '2020-01-01'],
            [DateTime::createFromFormat('Y-m-d H:i:s', '2018-10-21 19:21:32', new DateTimeZone('Europe/Amsterdam')), 'Y-m-d H:i:s', '2018-10-21 19:21:32'],
            [DateTime::createFromFormat('d-m-Y', '21-10-2018', new DateTimeZone('Europe/Amsterdam')), 'd-m-Y', '21-10-2018'],
        ];
    }

    /**
     * @test
     */
    public function groups_consecutive_dates_together(): void
    {
        $dateTimeImmutables = [
            DateTimeImmutable::createFromFormat('Y-m-d', '2020-01-01'),
            DateTimeImmutable::createFromFormat('Y-m-d', '2020-01-02'),
            DateTimeImmutable::createFromFormat('Y-m-d', '2020-01-03'),
            DateTimeImmutable::createFromFormat('Y-m-d', '2021-01-01'),
            DateTimeImmutable::createFromFormat('Y-m-d', '2022-01-01'),
        ];

        $dateTimes = [
            DateTime::createFromFormat('Y-m-d', '2020-01-01'),
            DateTime::createFromFormat('Y-m-d', '2020-01-02'),
            DateTime::createFromFormat('Y-m-d', '2020-01-03'),
            DateTime::createFromFormat('Y-m-d', '2021-01-01'),
            DateTime::createFromFormat('Y-m-d', '2022-01-01'),
        ];

        $groupedDateTimeImmutables = DateTimeHelper::groupConsecutiveDatesTogether($dateTimeImmutables);
        $groupedDateTimes = DateTimeHelper::groupConsecutiveDatesTogether($dateTimes);

        self::assertCount(3, $groupedDateTimeImmutables);
        self::assertCount(3, $groupedDateTimes);

        self::assertEquals('2020-01-01', $groupedDateTimeImmutables[0]['begin']->format('Y-m-d'));
        self::assertEquals('2020-01-03', $groupedDateTimeImmutables[0]['end']->format('Y-m-d'));
        self::assertEquals('2021-01-01', $groupedDateTimeImmutables[1]['begin']->format('Y-m-d'));
        self::assertEquals('2021-01-01', $groupedDateTimeImmutables[1]['end']->format('Y-m-d'));
        self::assertEquals('2022-01-01', $groupedDateTimeImmutables[2]['begin']->format('Y-m-d'));
        self::assertEquals('2022-01-01', $groupedDateTimeImmutables[2]['end']->format('Y-m-d'));

        self::assertEquals('2020-01-01', $groupedDateTimes[0]['begin']->format('Y-m-d'));
        self::assertEquals('2020-01-03', $groupedDateTimes[0]['end']->format('Y-m-d'));
        self::assertEquals('2021-01-01', $groupedDateTimes[1]['begin']->format('Y-m-d'));
        self::assertEquals('2021-01-01', $groupedDateTimes[1]['end']->format('Y-m-d'));
        self::assertEquals('2022-01-01', $groupedDateTimes[2]['begin']->format('Y-m-d'));
        self::assertEquals('2022-01-01', $groupedDateTimes[2]['end']->format('Y-m-d'));
    }

    /**
     * @test
     */
    public function returns_only_unique_dates(): void
    {
        $dateTimesWithDuplicates = [
            '2020-01-01 00:00:00',
            '2020-01-01 00:00:00',
            '2020-01-02 00:00:00',
            '2020-01-01 00:00:00',
        ];

        $uniqueDateTimes = DateTimeHelper::uniqueDates($dateTimesWithDuplicates);
        $stringifiedUniqueDateTimes = array_map(
            fn (DatetimeImmutable $item) => $item->format('Y-m-d H:i:s'),
            $uniqueDateTimes
        );

        self::assertCount(2, $uniqueDateTimes);
        self::assertContains('2020-01-01 00:00:00', $stringifiedUniqueDateTimes);
        self::assertContains('2020-01-02 00:00:00', $stringifiedUniqueDateTimes);
    }

    /**
     * @test
     */
    public function sorts_ascending_date_times(): void
    {
        $unsortedDateTimeImmutables = [
            new DateTimeImmutable('2021-10-01'),
            new DateTimeImmutable('2016-10-01'),
            new DateTimeImmutable('2012-10-01'),
            new DateTimeImmutable('2022-10-01'),
        ];

        $unsortedDateTimes = [
            new DateTime('2021-10-01'),
            new DateTime('2016-10-01'),
            new DateTime('2012-10-01'),
            new DateTime('2022-10-01'),
        ];

        $sortedDateTimeImmutables = DateTimeHelper::sortAscending(...$unsortedDateTimeImmutables);
        $sortedDateTimes = DateTimeHelper::sortAscending(...$unsortedDateTimes);

        self::assertEquals($unsortedDateTimeImmutables[0], $sortedDateTimeImmutables[2]);
        self::assertEquals($unsortedDateTimeImmutables[1], $sortedDateTimeImmutables[1]);
        self::assertEquals($unsortedDateTimeImmutables[2], $sortedDateTimeImmutables[0]);
        self::assertEquals($unsortedDateTimeImmutables[3], $sortedDateTimeImmutables[3]);

        self::assertEquals($unsortedDateTimes[0], $sortedDateTimes[2]);
        self::assertEquals($unsortedDateTimes[1], $sortedDateTimes[1]);
        self::assertEquals($unsortedDateTimes[2], $sortedDateTimes[0]);
        self::assertEquals($unsortedDateTimes[3], $sortedDateTimes[3]);
    }

    /**
     * @test
     */
    public function it_can_reset_seconds(): void
    {
        $original = new DateTimeImmutable('2021-10-01 00:00:12');

        $result = DateTimeHelper::resetSeconds($original);

        self::assertEquals('2021-10-01 00:00:00', $result->format(DateTimeHelper::MYSQL_DATE_TIME_FORMAT));
    }

    /**
     * @test
     *
     * @dataProvider provideDateTimesForDaysCount
     */
    public function it_counts_days_with_truncated_time(
        DateTimeImmutable $firstDateTime,
        DateTimeImmutable $secondDateTime,
        int $expectedDaysDifference
    ): void {
        self::assertEquals($expectedDaysDifference, DateTimeHelper::countDays($firstDateTime, $secondDateTime));
    }

    public static function provideDateTimesForDaysCount(): array
    {
        return [
            [new DateTimeImmutable('2021-09-01 10:00:00'), new DateTimeImmutable('2021-09-10 10:00:00'), 10],
            [new DateTimeImmutable('2021-09-10 10:00:00'), new DateTimeImmutable('2021-09-01 10:00:00'), 10],
            [new DateTimeImmutable('2021-09-01 09:00:00'), new DateTimeImmutable('2021-09-10 10:00:00'), 10],
            [new DateTimeImmutable('2021-09-01 10:00:00'), new DateTimeImmutable('2021-09-10 09:00:00'), 10],
        ];
    }

    /**
     * @test
     */
    public function it_should_add_week_days_to_date_range(): void
    {
        $from = DateTimeGenerator::generate('01-01-2022');
        $until = DateTimeGenerator::generate('03-01-2022');

        $actualTimeRangeWeekDays = DateTimeHelper::mapDateRangeToWeekDays($from, $until);
        $expectedTimeRangeWeekDays = [
            '2022-01-01' => WeekdayEnum::saturday(),
            '2022-01-02' => WeekdayEnum::sunday(),
            '2022-01-03' => WeekdayEnum::monday(),
        ];

        self::assertSame($expectedTimeRangeWeekDays, $actualTimeRangeWeekDays);
    }

    /**
     * @test
     */
    public function it_should_should_reset_milliseconds(): void
    {
        $originDateTime = DateTimeGenerator::generate();
        $resetDateTime = DateTimeGenerator::generate($originDateTime->format(DateTimeHelper::MYSQL_DATE_TIME_FORMAT));

        self::assertNotEquals($resetDateTime, $originDateTime);
        self::assertEquals($resetDateTime, DateTimeHelper::resetMilliseconds($originDateTime));
    }

    /**
     * @test
     */
    public function it_should_reset_time(): void
    {
        $original = new DateTimeImmutable('2021-10-01 12:12:12');

        $result = DateTimeHelper::resetTime($original);

        self::assertEquals('2021-10-01 00:00:00', $result->format(DateTimeHelper::MYSQL_DATE_TIME_FORMAT));
    }

    /**
     * @test
     */
    public function it_should_detect_more_days(): void
    {
        $startDates = [
            new DateTimeImmutable('2021-10-01'),
            new DateTimeImmutable('2021-10-10'),
            new DateTimeImmutable('2021-10-01'),
        ];

        $endDates = [
            new DateTimeImmutable('2021-10-01'),
            new DateTimeImmutable('2021-10-01'),
            new DateTimeImmutable('2021-10-10'),
        ];

        self::assertFalse(DateTimeHelper::hasMoreDays($startDates[0], $endDates[0]));
        self::assertTrue(DateTimeHelper::hasMoreDays($startDates[1], $endDates[1]));
        self::assertTrue(DateTimeHelper::hasMoreDays($startDates[2], $endDates[2]));
    }

    /**
     * @test
     */
    public function it_should_provide_next_day(): void
    {
        $dates = [new DateTimeImmutable('2021-10-01 12:12:12'), new DateTimeImmutable('2021-10-05 12:12:12')];

        $resultNextDay = DateTimeHelper::getNextDay($dates[0], $dates[1]);
        $resultPrevDay = DateTimeHelper::getNextDay($dates[1], $dates[0]);

        self::assertEquals('2021-10-02 00:00:00', $resultNextDay->format(DateTimeHelper::MYSQL_DATE_TIME_FORMAT));
        self::assertEquals('2021-10-04 00:00:00', $resultPrevDay->format(DateTimeHelper::MYSQL_DATE_TIME_FORMAT));
    }

    /**
     * @test
     */
    public function it_should_detect_time_direction(): void
    {
        $dates = [new DateTimeImmutable('2021-10-01 12:12:12'), new DateTimeImmutable('2021-10-05 12:12:12')];

        self::assertTrue(DateTimeHelper::isGoingForwardInTime($dates[0], $dates[1]));
        self::assertFalse(DateTimeHelper::isGoingForwardInTime($dates[1], $dates[0]));
    }
}
