<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\DateTime;

use DateInterval;
use DateTimeImmutable;
use Hgraca\PhpExtension\DateTime\DateTimeRange;
use Hgraca\PhpExtension\DateTime\DayIterator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class DayIteratorTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider provideDaysIntervals
     */
    public function it_should_start_at_the_first_item(
        DateTimeRange $dateTimeInterval,
        DateTimeImmutable $expectedFirst
    ): void {
        $iterator = new DayIterator($dateTimeInterval);

        self::assertEquals($expectedFirst, $iterator->current());
    }

    /**
     * @return array<
     *      int,
     *      array{
     *          DateTimeRange,
     *          DateTimeImmutable
     *      }
     * >
     */
    public static function provideDaysIntervals(): array
    {
        return [
            [
                new DateTimeRange(new DateTimeImmutable('yesterday'), new DateTimeImmutable('tomorrow')),
                (new DateTimeImmutable('yesterday'))->setTime(0, 0),
            ],
            [
                new DateTimeRange(new DateTimeImmutable('tomorrow'), new DateTimeImmutable('yesterday')),
                (new DateTimeImmutable('tomorrow'))->setTime(0, 0),
            ],
        ];
    }

    /**
     * @test
     */
    public function it_should_move_to_next_item(): void
    {
        $yesterday = new DateTimeImmutable('yesterday');
        $today = new DateTimeImmutable('now');
        $tomorrow = new DateTimeImmutable('tomorrow');
        $dateTimeInterval = new DateTimeRange($yesterday, $tomorrow);
        $iterator = new DayIterator($dateTimeInterval);

        self::assertEquals($yesterday->setTime(0, 0), $iterator->current());
        $iterator->next();
        self::assertEquals($today->setTime(0, 0), $iterator->current());
        $iterator->next();
        self::assertEquals($tomorrow->setTime(0, 0), $iterator->current());
    }

    /**
     * @test
     */
    public function it_should_know_the_key_of_the_current_item(): void
    {
        $yesterday = new DateTimeImmutable('yesterday');
        $tomorrow = new DateTimeImmutable('tomorrow');
        $dateTimeInterval = new DateTimeRange($yesterday, $tomorrow);
        $iterator = new DayIterator($dateTimeInterval);

        self::assertEquals(0, $iterator->key());
        $iterator->next();
        self::assertEquals(1, $iterator->key());
        $iterator->next();
        self::assertEquals(2, $iterator->key());
    }

    /**
     * @test
     */
    public function it_should_know_when_its_an_invalid_position(): void
    {
        $yesterday = new DateTimeImmutable('yesterday');
        $tomorrow = new DateTimeImmutable('tomorrow');
        $dateTimeInterval = new DateTimeRange($yesterday, $tomorrow);
        $iterator = new DayIterator($dateTimeInterval);

        self::assertTrue($iterator->valid());
        $iterator->next();
        self::assertTrue($iterator->valid());
        $iterator->next();
        self::assertTrue($iterator->valid());
        $iterator->next();
        self::assertFalse($iterator->valid());
    }

    /**
     * @test
     */
    public function it_should_be_able_to_rewind_to_the_beginning(): void
    {
        $yesterday = new DateTimeImmutable('yesterday');
        $today = new DateTimeImmutable('now');
        $tomorrow = new DateTimeImmutable('tomorrow');
        $dateTimeInterval = new DateTimeRange($yesterday, $tomorrow);
        $iterator = new DayIterator($dateTimeInterval);

        self::assertEquals($yesterday->setTime(0, 0), $iterator->current());
        $iterator->next();
        self::assertEquals($today->setTime(0, 0), $iterator->current());
        $iterator->rewind();
        self::assertEquals($yesterday->setTime(0, 0), $iterator->current());
    }

    /**
     * @test
     */
    public function it_should_be_able_to_loop_through_elements(): void
    {
        $yesterday = new DateTimeImmutable('yesterday');
        $tomorrow = new DateTimeImmutable('tomorrow');
        $dateTimeInterval = new DateTimeRange($yesterday, $tomorrow);
        $iterator = new DayIterator($dateTimeInterval);

        $i = 0;
        foreach ($iterator as $key => $day) {
            self::assertEquals($i, $key);
            self::assertEquals($yesterday->add(new DateInterval('P' . $i . 'D')), $day);
            self::assertTrue($iterator->valid());
            ++$i;
        }
        self::assertFalse($iterator->valid());
    }
}
