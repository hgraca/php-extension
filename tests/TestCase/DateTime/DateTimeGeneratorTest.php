<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\DateTime;

use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

use function time;

/**
 * @small
 *
 * @internal
 */
final class DateTimeGeneratorTest extends AbstractTest
{
    public const TOLERATED_SECONDS_DIFF = 5;

    /**
     * @test
     */
    public function generate_now(): void
    {
        self::assertLessThan(
            time() + self::TOLERATED_SECONDS_DIFF,
            DateTimeGenerator::generate()->getTimestamp()
        );
    }

    /**
     * @test
     *
     * @dataProvider provideDateTime
     */
    public function generate(
        string $time,
        ?DateTimeZone $timezone,
        int $expectedTimestamp
    ): void {
        self::assertEquals($expectedTimestamp, DateTimeGenerator::generate($time, $timezone)->getTimestamp());
    }

    /**
     * @return array<array{string, DateTimeZone|null, int}>
     */
    public static function provideDateTime(): array
    {
        return [
            ['Sun, 22 Apr 2018 19:21:32 GMT', null, 1_524_424_892],
            ['Sunday, 22 April 2018 19:21:32', new DateTimeZone('Europe/Amsterdam'), 1_524_417_692],
        ];
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function override_default_generator(): void
    {
        $date = '2018-10-21';
        DateTimeGenerator::overrideDefaultGenerator(
            fn () => new DateTimeImmutable($date)
        );

        self::assertEquals(new DateTimeImmutable($date), DateTimeGenerator::generate('abc'));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function reset(): void
    {
        $date = '2018-10-21';
        DateTimeGenerator::overrideDefaultGenerator(
            fn () => new DateTimeImmutable($date)
        );
        self::assertEquals(new DateTimeImmutable($date), DateTimeGenerator::generate('abc'));

        DateTimeGenerator::reset();
        self::assertLessThan(
            time() + self::TOLERATED_SECONDS_DIFF,
            DateTimeGenerator::generate()->getTimestamp()
        );
    }
}
