<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\String;

use Generator;
use Hgraca\PhpExtension\String\StringHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 */
final class StringHelperTest extends AbstractTest
{
    private const USER_FRIENDLY_ALLOWED_CHARS = '0123456789ABCDEFGHJKMNPQRSTVWXYZ';

    /**
     * @test
     *
     * @dataProvider getSlugs
     */
    public function to_slug(string $string, string $slug): void
    {
        self::assertSame($slug, StringHelper::toSlug($string));
    }

    public static function getSlugs(): Generator
    {
        yield ['Lorem Ipsum', 'lorem-ipsum'];
        yield ['  Lorem Ipsum  ', 'lorem-ipsum'];
        yield [' lOrEm  iPsUm  ', 'lorem-ipsum'];
        yield ['!Lorem Ipsum!', '!lorem-ipsum!'];
        yield ['lorem-ipsum', 'lorem-ipsum'];
        yield ['lorem 日本語 ipsum', 'lorem-日本語-ipsum'];
        yield ['lorem русский язык ipsum', 'lorem-русский-язык-ipsum'];
        yield ['lorem العَرَبِيَّة‎‎ ipsum', 'lorem-العَرَبِيَّة‎‎-ipsum'];
    }

    /**
     * @test
     *
     * @dataProvider provideStrings
     */
    public function contains_finds_needle_when_its_there(string $needle, string $haystack, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::contains($needle, $haystack));
    }

    /**
     * @return array<array{string, string, bool}>
     */
    public static function provideStrings(): array
    {
        return [
            ['', 'beginning to ending', true],
            ['beginning', 'beginning to ending', true],
            ['to', 'beginning to ending', true],
            ['ending', 'beginning to ending', true],
            ['unexistent', 'beginning to ending', false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideStudlyTests
     */
    public function to_studly_case(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::toStudlyCase($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideStudlyTests(): array
    {
        return [
            ['TABLE_NAME', 'TableName'],
            ['Table_NaMe', 'TableNaMe'],
            ['table_name', 'TableName'],
            ['TableName', 'TableName'],
            ['tableName', 'TableName'],
            ['table-Name', 'TableName'],
            ['table.Name', 'TableName'],
            ['table Name', 'TableName'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideCamelCaseTests
     */
    public function to_camel_case(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::toCamelCase($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideCamelCaseTests(): array
    {
        return [
            ['TABLE_NAME', 'tableName'],
            ['Table_NaMe', 'tableNaMe'],
            ['table_name', 'tableName'],
            ['TableName', 'tableName'],
            ['tableName', 'tableName'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideSnakeCaseTests
     */
    public function to_snake_case(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::toSnakeCase($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideSnakeCaseTests(): array
    {
        return [
            ['TABLE_NAME', 'table_name'],
            ['Table_NaMe', 'table_na_me'],
            ['TableName', 'table_name'],
            ['table_Name', 'table_name'],
            ['tableName', 'table_name'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideSpacedWordsTests
     */
    public function separate_words_with_space(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::separateWordsWithSpace($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideSpacedWordsTests(): array
    {
        return [
            ['TABLE_NAME', 'TABLE NAME'],
            ['Table.NaMe', 'Table NaMe'],
            ['TableName', 'TableName'],
            ['table Name', 'table Name'],
            ['table-Name', 'table Name'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideMakeAllWordsUpperCaseFirst
     */
    public function make_all_words_upper_case_first(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::makeAllWordsUpperCaseFirst($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideMakeAllWordsUpperCaseFirst(): array
    {
        return [
            ['TABLE_name', 'TABLE_Name'],
            ['Table.naMe', 'Table.NaMe'],
            ['TableName', 'TableName'],
            ['table Name', 'Table Name'],
            ['table name', 'Table Name'],
            ['table-name', 'Table-Name'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideMakeLowCase
     */
    public function make_low_case(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::makeLowCase($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideMakeLowCase(): array
    {
        return [
            ['TABLE_NAME', 'table_name'],
            ['Table.NaMe', 'table.name'],
            ['TableName', 'tablename'],
            ['table Name', 'table name'],
            ['table-Name', 'table-name'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideRemoveAllSpaces
     */
    public function remove_all_spaces(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::removeAllSpaces($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideRemoveAllSpaces(): array
    {
        return [
            ['TABLE_NAME', 'TABLE_NAME'],
            ['Table.NaMe', 'Table.NaMe'],
            ['TableName', 'TableName'],
            ['table Name', 'tableName'],
            ['table Name bla', 'tableNamebla'],
            ['table-Name', 'table-Name'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideSpacedCapitalizedWordsTests
     */
    public function separate_capitalized_words_with_space(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::separateCapitalizedWordsWithSpace($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideSpacedCapitalizedWordsTests(): array
    {
        return [
            ['TABLE_NAME', 'TABLE_NAME'],
            ['Table.NaMe', 'Table.Na Me'],
            ['TableName', 'Table Name'],
            ['table Name', 'table Name'],
            ['table-Name', 'table-Name'],
        ];
    }

    /**
     * @dataProvider dataProvider_testHasBeginning
     *
     * @test
     */
    public function has_beginning(string $needle, string $haystack, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::hasBeginning($needle, $haystack));
    }

    /**
     * @return array<array{string, string, bool}>
     */
    public static function dataProvider_testHasBeginning(): array
    {
        return [
            ['', 'beginning to ending', true],
            ['beginning', 'beginning to ending', true],
            ['to', 'beginning to ending', false],
            ['ending', 'beginning to ending', false],
            ['unexistent', 'beginning to ending', false],
        ];
    }

    /**
     * @dataProvider dataProvider_testHasEnding
     *
     * @test
     */
    public function has_ending(string $needle, string $haystack, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::hasEnding($needle, $haystack));
    }

    /**
     * @return array<array{string, string, bool}>
     */
    public static function dataProvider_testHasEnding(): array
    {
        return [
            ['', 'beginning to ending', true],
            ['beginning', 'beginning to ending', false],
            ['to', 'beginning to ending', false],
            ['ending', 'beginning to ending', true],
            ['unexistent', 'beginning to ending', false],
        ];
    }

    /**
     * @dataProvider dataProvider_testIsJson
     *
     * @test
     */
    public function is_json(string $string, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::isJson($string));
    }

    /**
     * @return array<array{string|false, bool}>
     */
    public static function dataProvider_testIsJson(): array
    {
        return [
            ['camel-from beginning_to ending', false],
            [json_encode(['test' => 1, 'a' => 'blabla']), true],
        ];
    }

    /**
     * @dataProvider dataProvider_testRemove
     *
     * @test
     */
    public function remove(string $needle, string $haystack, string $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::remove($needle, $haystack));
    }

    /**
     * @return array<array{string, string, string}>
     */
    public static function dataProvider_testRemove(): array
    {
        return [
            ['', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'beginning to ending', ' to ending'],
            ['to', 'beginning to ending', 'beginning  ending'],
            ['to', 'beginning to to endtoing', 'beginning   ending'],
            ['ending', 'beginning to ending', 'beginning to '],
            ['unexistent', 'beginning to ending', 'beginning to ending'],
        ];
    }

    /**
     * @dataProvider dataProvider_testRemoveFromBeginning
     *
     * @test
     */
    public function remove_from_beginning(string $needle, string $haystack, string $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::removeFromBeginning($needle, $haystack));
    }

    /**
     * @return array<array{string, string, string}>
     */
    public static function dataProvider_testRemoveFromBeginning(): array
    {
        return [
            ['', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'beginning to ending', ' to ending'],
            ['to', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'beginningbeginning to ending', 'beginning to ending'],
            ['ending', 'beginning to ending', 'beginning to ending'],
            ['unexistent', 'beginning to ending', 'beginning to ending'],
        ];
    }

    /**
     * @dataProvider dataProvider_testRemoveFromEnd
     *
     * @test
     */
    public function remove_from_end(string $needle, string $haystack, string $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::removeFromEnd($needle, $haystack));
    }

    /**
     * @return array<array{string, string, string}>
     */
    public static function dataProvider_testRemoveFromEnd(): array
    {
        return [
            ['', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'beginning to ending', 'beginning to ending'],
            ['to', 'beginning to ending', 'beginning to ending'],
            ['ending', 'beginning to endingending', 'beginning to ending'],
            ['ending', 'beginning to ending', 'beginning to '],
            ['unexistent', 'beginning to ending', 'beginning to ending'],
        ];
    }

    /**
     * @dataProvider dataProvider_testReplace
     *
     * @test
     */
    public function replace(string $needle, string $replace, string $haystack, string $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::replace($needle, $replace, $haystack));
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function dataProvider_testReplace(): array
    {
        return [
            ['', 'A', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'AAA', 'beginning to ending', 'AAA to ending'],
            ['to', 'AAA', 'beginning to ending', 'beginning AAA ending'],
            ['ending', 'AAA', 'beginning to endingending', 'beginning to AAAAAA'],
            ['ending', 'AAA', 'beginning to ending', 'beginning to AAA'],
            ['unexistent', 'AAA', 'beginning to ending', 'beginning to ending'],
        ];
    }

    /**
     * @dataProvider dataProvider_testReplaceFromBeginning
     *
     * @test
     */
    public function replace_from_beginning(string $needle, string $replace, string $haystack, string $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::replaceFromBeginning($needle, $replace, $haystack));
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function dataProvider_testReplaceFromBeginning(): array
    {
        return [
            ['', 'A', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'AAA', 'beginning to ending', 'AAA to ending'],
            ['beginning', 'AAA', 'beginningbeginning to ending', 'AAAbeginning to ending'],
            ['to', 'AAA', 'beginning to ending', 'beginning to ending'],
            ['ending', 'AAA', 'beginning to endingending', 'beginning to endingending'],
            ['ending', 'AAA', 'beginning to ending', 'beginning to ending'],
            ['unexistent', 'AAA', 'beginning to ending', 'beginning to ending'],
        ];
    }

    /**
     * @dataProvider dataProvider_testReplaceFromEnd
     *
     * @test
     */
    public function replace_from_end(string $needle, string $replace, string $haystack, string $expectedResult): void
    {
        self::assertEquals($expectedResult, StringHelper::replaceFromEnd($needle, $replace, $haystack));
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function dataProvider_testReplaceFromEnd(): array
    {
        return [
            ['', 'A', 'beginning to ending', 'beginning to ending'],
            ['beginning', 'AAA', 'beginning to ending', 'beginning to ending'],
            ['to', 'AAA', 'beginning to ending', 'beginning to ending'],
            ['ending', 'AAA', 'beginning to endingending', 'beginning to endingAAA'],
            ['ending', 'AAA', 'beginning to ending', 'beginning to AAA'],
            ['unexistent', 'AAA', 'beginning to ending', 'beginning to ending'],
        ];
    }

    /**
     * @test
     *
     * @param array<string> $expected
     *
     * @dataProvider provide_extracts_from_between_two_strings
     */
    public function extracts_from_between_two_strings(
        string $before,
        string $after,
        string $subject,
        array $expected
    ): void {
        self::assertEquals($expected, StringHelper::extractFromBetween($before, $after, $subject));
    }

    /**
     * @return array<array{string, string, string, array<string>}>
     */
    public static function provide_extracts_from_between_two_strings(): array
    {
        return [
            ['aaa', 'bbb', 'aaacccbbb', ['ccc']],
            ['', 'bbb', 'aaacccbbb', []],
            ['aaa', '', 'aaacccbbb', []],
            ['aaa', 'bbb', 'aaabbb', []],
            ['aaa', 'bbb', 'ccc', []],
            ['xxx', 'bbb', 'aaacccbbb', []],
            ['aaa', 'yyy', 'aaacccbbb', []],
            ['aaa', 'bbb', 'aaaxxxbbbzzzaaayyybbb', ['xxx', 'yyy']],
            ['@var', '*/', '/** @var CccEntity */', [' CccEntity ']],
        ];
    }

    /**
     * @test
     *
     * @dataProvider stringContainsCharacterProvider
     */
    public function it_should_resolve_whether_string_contains_only_characters_from_provided_alphabet(string $needle, string $haystack, bool $expectedOutcome): void
    {
        self::assertSame(
            $expectedOutcome,
            StringHelper::containsOnly($needle, $haystack),
            "Expecting only to find {$haystack} in {$needle} but found other characters as well"
        );
    }

    /**
     * @return array<array{string, string, bool}>
     */
    public static function stringContainsCharacterProvider(): array
    {
        return [
            ['abcd', 'ab', false],
            ['aabbbccccdddd', 'abcd', true],
            ['this is a sentence', 'thisaentc ', true],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideConstantTests
     */
    public function it_should_format_string_to_constant_case(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::toConstantCase($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideConstantTests(): array
    {
        return [
            ['TABLE_NAME', 'TABLE_NAME'],
            ['Table_NaMe', 'TABLE_NA_ME'],
            ['table_name', 'TABLE_NAME'],
            ['TableName', 'TABLE_NAME'],
            ['tableName', 'TABLE_NAME'],
            ['table-Name', 'TABLE_NAME'],
            ['table.Name', 'TABLE_NAME'],
            ['table Name', 'TABLE_NAME'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider kebabCaseDataProvider
     */
    public function it_should_format_input_in_kebab_case(string $input, string $expectedOutput): void
    {
        self::assertSame($expectedOutput, StringHelper::toKebabCase($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function kebabCaseDataProvider(): array
    {
        return [
            ['TABLE_NAME', 'table-name'],
            ['Table_NaMe', 'table-na-me'],
            ['TableName', 'table-name'],
            ['table_Name', 'table-name'],
            ['tableName', 'table-name'],
        ];
    }

    /**
     * @test
     */
    public function it_should_return_random_string(): void
    {
        $stringLength = 7;
        $randomString1 = StringHelper::getRandomString($stringLength);
        self::assertEquals($stringLength, strlen($randomString1));

        $randomString2 = StringHelper::getRandomString($stringLength);
        self::assertEquals($stringLength, strlen($randomString2));

        self::assertNotSame($randomString1, $randomString2);
    }

    /**
     * @test
     */
    public function it_should_return_random_string_from_provided_set(): void
    {
        $stringLength = 16;
        $maxIterations = 10;
        $allowedCharacters = 'ab';

        $randomStrings = [];

        for ($iteration = 0; $iteration < $maxIterations; ++$iteration) {
            $randomStrings[] = StringHelper::getRandomString($stringLength, $allowedCharacters);
        }

        $duplicationCount = count($randomStrings) - count(array_unique($randomStrings));
        self::assertSame(
            0,
            $duplicationCount,
            "There should be no duplicated strings, {$duplicationCount} duplicates found in {$maxIterations} attempts"
        );

        $this->assertStringOnlyContainsChars(
            $randomStrings[random_int(0, count($randomStrings) - 1)],
            $allowedCharacters
        );
    }

    /**
     * @test
     */
    public function it_should_shuffle_string(): void
    {
        $unshuffled = 'hello world';
        $shuffled = StringHelper::shuffle($unshuffled);

        self::assertNotSame(
            $unshuffled,
            $shuffled,
            'The shuffled string should not be idential to the unshuffled string'
        );

        self::assertSame(
            strlen($shuffled),
            strlen($unshuffled),
            'The shuffled strings should be of even length as the unshuffled string'
        );
    }

    /**
     * @test
     *
     * @dataProvider provideBase64UrlSafeEncode
     */
    public function it_should_encode_string_as_url_safe_base64(string $toEncode, string $encoded): void
    {
        self::assertSame(
            $encoded,
            StringHelper::base64UrlEncode($toEncode)
        );
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideBase64UrlSafeEncode()
    {
        return [
            ['string to encode', 'c3RyaW5nIHRvIGVuY29kZQ--'],
            ['encoded string', 'ZW5jb2RlZCBzdHJpbmc-'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideBase64UrlSafeDecode
     */
    public function it_should_decode_string_as_url_safe_base64(string $toDecode, string $decoded): void
    {
        self::assertSame(
            $decoded,
            StringHelper::base64UrlDecode($toDecode)
        );
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideBase64UrlSafeDecode()
    {
        return [
            ['c3RyaW5nIHRvIGVuY29kZQ--', 'string to encode'],
            ['ZW5jb2RlZCBzdHJpbmc-', 'encoded string'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideEnsureStartsWith
     */
    public function it_should_ensure_string_starts_with_given_needle(string $needle, string $haystack, string $expectedResult): void
    {
        self::assertSame(
            $expectedResult,
            StringHelper::ensureStartsWith($needle, $haystack)
        );
    }

    /**
     * @return array<string, list<string>>
     */
    public static function provideEnsureStartsWith(): array
    {
        return [
            'String starts with `x` already starting with multiple `x`' => ['x', 'xxxbcda', 'xxxbcda'],
            'String starts with `x` already starting with `x`' => ['x', 'xbcda', 'xbcda'],
            'String starts with `x`' => ['x', 'abcd', 'xabcd'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideEnsureEndsWith
     */
    public function it_should_ensure_string_ends_with_given_needle(string $needle, string $haystack, string $expectedResult): void
    {
        self::assertSame(
            $expectedResult,
            StringHelper::ensureEndsWith($needle, $haystack)
        );
    }

    /**
     * @return array<string, list<string>>
     */
    public static function provideEnsureEndsWith(): array
    {
        return [
            'String ends with `a`' => ['a', 'abcd', 'abcda'],
            'String ends with `a` already ending in `a`' => ['a', 'abcda', 'abcda'],
            'String ends with `a` already ending in multiple `a`' => ['a', 'abcdaaa', 'abcdaaa'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideEnsureWrappedWithStringCases
     */
    public function it_should_wrap_string_with_patterns_when_it_is_not_wrapper(string $left, string $center, string $right, string $expected): void
    {
        self::assertSame($expected, StringHelper::ensureWrappedWithString($left, $right, $center));
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function provideEnsureWrappedWithStringCases(): array
    {
        return [
            ['aaa', 'bbb', 'ccc', 'aaabbbccc'],
            ['aaa', 'aaabbb', 'ccc', 'aaabbbccc'],
            ['aaa', 'bbbccc', 'ccc', 'aaabbbccc'],
            ['aaa', 'aaabbbccc', 'ccc', 'aaabbbccc'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideStringTrimCases
     */
    public function it_should_replace_the_string_when_matches(string $needle, string $haystack, string $replacement, string $expected): void
    {
        if ($replacement === '') {
            self::assertSame($expected, StringHelper::stringReplace($needle, $haystack));
        } else {
            self::assertSame($expected, StringHelper::stringReplace($needle, $haystack, $replacement));
        }
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function provideStringTrimCases(): array
    {
        return [
            ['aaa', 'aaabbbccc', '', 'bbbccc'],
            ['bbb', 'aaabbbccc', '', 'aaaccc'],
            ['ccc', 'aaabbbccc', '', 'aaabbb'],
            ['aaa', 'aaabbbccc', 'zzz', 'zzzbbbccc'],
            ['bbb', 'aaabbbccc', 'zzz', 'aaazzzccc'],
            ['ccc', 'aaabbbccc', 'zzz', 'aaabbbzzz'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideLeftStringTrimCases
     */
    public function it_should_replace_start_of_the_string_when_matches(string $trim, string $haystack, string $replacement, string $expected): void
    {
        if ($replacement === '') {
            self::assertSame($expected, StringHelper::leftStringReplace($trim, $haystack));
        } else {
            self::assertSame($expected, StringHelper::leftStringReplace($trim, $haystack, $replacement));
        }
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function provideLeftStringTrimCases(): array
    {
        return [
            ['aaa', 'aaabbb', '', 'bbb'],
            ['bbb', 'aaabbb', '', 'aaabbb'],
            ['aaa', 'aaabbb', 'zzz', 'zzzbbb'],
            ['bbb', 'aaabbb', 'zzz', 'aaabbb'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideRightStringTrimCases
     */
    public function it_should_replace_end_of_the_string_when_matches(string $trim, string $haystack, string $replacement, string $expected): void
    {
        if ($replacement === '') {
            self::assertSame($expected, StringHelper::rightStringReplace($trim, $haystack));
        } else {
            self::assertSame($expected, StringHelper::rightStringReplace($trim, $haystack, $replacement));
        }
    }

    /**
     * @return array<array{string, string, string, string}>
     */
    public static function provideRightStringTrimCases(): array
    {
        return [
            ['aaa', 'aaabbb', '', 'aaabbb'],
            ['bbb', 'aaabbb', '', 'aaa'],
            ['aaa', 'aaabbb', 'zzz', 'aaabbb'],
            ['bbb', 'aaabbb', 'zzz', 'aaazzz'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideCapitalizedWordCases
     */
    public function it_should_format_string_from_known_formats_to_human_readable(string $input, string $expected): void
    {
        self::assertSame($expected, StringHelper::toHumanReadable($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideCapitalizedWordCases(): array
    {
        return [
            ['ThisIsAStringOfWords', 'This is a string of words'],
            ['This_Is_A_String_Of_Words', 'This is a string of words'],
            ['this-is-a-string-of-words', 'This is a string of words'],
            ['camelCasingWorksAsWell', 'Camel casing works as well'],
            ['we-are-part-of-the-url', 'We are part of the url'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideBase64EncodeStrings
     */
    public function it_should_encode_string_in_base64(string $input, string $expected): void
    {
        self::assertSame($expected, StringHelper::base64Encode($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideBase64EncodeStrings(): array
    {
        return [
            ['string to encode', 'c3RyaW5nIHRvIGVuY29kZQ=='],
            ['encoded string', 'ZW5jb2RlZCBzdHJpbmc='],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideBase64DecodeStrings
     */
    public function it_should_decode_string_in_base64(string $input, string $expected): void
    {
        self::assertSame($expected, StringHelper::base64Decode($input));
    }

    /**
     * @return array<array{string, string}>
     */
    public static function provideBase64DecodeStrings(): array
    {
        return [
            ['c3RyaW5nIHRvIGVuY29kZQ==', 'string to encode'],
            ['ZW5jb2RlZCBzdHJpbmc=', 'encoded string'],
        ];
    }

    /**
     * @test
     */
    public function it_should_generate_a_user_friendly_id(): void
    {
        $userFriendlyId = StringHelper::GenerateUserFriendlyId();

        self::assertMatchesRegularExpression(
            sprintf('/[0-9]{3}-[0-9]{3}-[%s]{4}/', self::USER_FRIENDLY_ALLOWED_CHARS),
            $userFriendlyId
        );
    }

    /**
     * @test
     *
     * @dataProvider provideGlobMatchTests
     */
    public function it_should_know_if_string_matches_glob_pattern(
        string $pattern,
        string $filename,
        bool $expected
    ): void {
        self::assertSame($expected, StringHelper::isGlobMatch($pattern, $filename));
    }

    /**
     * @return array<int, array{string, string, bool}>
     */
    public static function provideGlobMatchTests(): array
    {
        return [
            ['/one/two/three', '/one/two/three', true],
            ['/one/t*o/three', '/one/two/three', true],
            ['/one/t*/three', '/one/two/three', true],
            ['/one/t?o/three', '/one/two/three', true],
            ['/one/*/three', '/one/two/three', true],
            ['/one/t??o/three', '/one/two/three', false],
            ['/one/*n/three', '/one/two/three', false],
        ];
    }

    private function assertStringOnlyContainsChars(string $string, string $allowedChars): void
    {
        $allowedCharList = array_unique(str_split($allowedChars));
        $charsInString = array_unique(str_split($string));

        self::assertCount(
            0,
            array_diff($charsInString, $allowedCharList),
            "The string '{$string}' may only contain characters [" . implode(', ', $allowedCharList) . ']'
        );
    }
}
