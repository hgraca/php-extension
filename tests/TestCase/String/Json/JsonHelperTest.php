<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\String\Json;

use Hgraca\PhpExtension\String\Json\JsonDecodingException;
use Hgraca\PhpExtension\String\Json\JsonDoesNotContainPathException;
use Hgraca\PhpExtension\String\Json\JsonHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @internal
 *
 * @group micro
 */
final class JsonHelperTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_encode_an_array_to_json(): void
    {
        $data = ['a' => 'b', 'c' => 'd'];

        $json = JsonHelper::encode($data);

        self::assertSame(json_encode($data), $json);
    }

    /**
     * @test
     */
    public function it_should_decode_json_to_an_array(): void
    {
        $originalData = ['a' => 'b', 'c' => 'd'];
        /** @var string $json */
        $json = json_encode($originalData);

        $decodedData = JsonHelper::decode($json);

        self::assertSame($originalData, $decodedData);
    }

    /**
     * @test
     */
    public function it_should_throw_exception_when_given_invalid_json(): void
    {
        $this->expectException(JsonDecodingException::class);
        JsonHelper::decode('this is not json');
    }

    /**
     * @test
     *
     * @dataProvider provideJson
     */
    public function it_should_detect_json(string $value, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, JsonHelper::isJson($value));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideJson()
    {
        return [
            ['123', false],
            ['this is not json', false],
            ['{this is not json}', false],
            ['[this is not json]', false],
            ['{"a": 1}', true],
        ];
    }

    /**
     * @test
     *
     * @dataProvider jsonWithPathsDataProvider
     */
    public function it_should_resolve_json_by_path_when_available(string $json, string $path, string $expectedJson): void
    {
        self::assertJsonStringEqualsJsonString($expectedJson, JsonHelper::extract($json, $path));
    }

    /**
     * @return array<string, array{0: string, 1: string, 2: string}>
     */
    public static function jsonWithPathsDataProvider(): array
    {
        $json = <<<JSON
                        {
                            "data": {
                                "id": 123,
                                "name": "Important stuff",
                                "isGreen": false,
                                "price": 12.34,
                                "fruits": [
                                    {
                                        "id": 1,
                                        "name": "banana",
                                        "age": 15
                                    },
                                    {
                                        "id": 2,
                                        "name": "apple",
                                        "age": 999
                                    }
                                ],
                                "more": {
                                    "things": {
                                        "here": "and there"
                                    }
                                }
                            }
                        }
            JSON;

        return [
            'path to another json' => [$json, '.data.more.things', '{"here": "and there"}'],
            'path to a integer field' => [$json, '.data.id', '123'],
            'path to a string field' => [$json, '.data.name', '"Important stuff"'],
            'path to a boolean field' => [$json, '.data.isGreen', 'false'],
            'path to a float field' => [$json, '.data.price', '12.34'],
            'path to an index in array' => [$json, '.data.fruits[1]', '{"id":2, "name":"apple","age":999}'],
            'complex path' => [$json, '.data.fruits[0].name', '"banana"'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider jsonWithNonExistingPathsDataProvider
     */
    public function it_should_throw_an_exception_when_path_not_found_in_json(string $json, string $path): void
    {
        self::expectException(JsonDoesNotContainPathException::class);

        JsonHelper::extract($json, $path);
    }

    /**
     * @return array<string, array{0: string, 1: string}>
     */
    public static function jsonWithNonExistingPathsDataProvider(): array
    {
        return [
            'path does not exist' => ['{}', '.data.name'],
            'array index does not exist' => ['{"fruits": [ {"id": 1}, {"id":2}]}', '.fruits[5]'],
        ];
    }

    /**
     * @test
     *
     * This is a temporary thing we shoudl adhere to until we can properly deal with unicode characters in our database.
     * Which should be when we have moved to GCP.
     */
    public function it_should_not_encode_utf8_with_unicode_characters(): void
    {
        $textWithUtf8Characters = ['Bouciqué'];

        $encodedData = JsonHelper::encode($textWithUtf8Characters);

        self::assertSame('["Bouciqué"]', $encodedData);
    }
}
