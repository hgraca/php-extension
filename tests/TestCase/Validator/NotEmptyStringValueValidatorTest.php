<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator;

use Hgraca\PhpExtension\Exception\ValidationException;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\NotEmptyStringValueValidator;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class NotEmptyStringValueValidatorTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_not_add_error(): void
    {
        (new NotEmptyStringValueValidator($exception = new ValidationException()))
            ->validate('valid string', 'fieldName');

        self::assertEmpty($exception->toArray()['errors']);
    }

    /**
     * @test
     */
    public function it_should_add_error_for_empty_string(): void
    {
        (new NotEmptyStringValueValidator($exception = new ValidationException()))
            ->validate('', 'fieldName');

        self::assertEquals(
            NotEmptyStringValueValidator::EMPTY_STRING_VALUE_VIOLATION_ERROR_CODE,
            $exception->getErrorMessage('fieldName')
        );
    }

    /**
     * @test
     */
    public function it_should_add_error_for_whitespace_string(): void
    {
        (new NotEmptyStringValueValidator($exception = new ValidationException()))
            ->validate(' ', 'fieldName');

        self::assertEquals(
            NotEmptyStringValueValidator::EMPTY_STRING_VALUE_VIOLATION_ERROR_CODE,
            $exception->getErrorMessage('fieldName')
        );
    }
}
