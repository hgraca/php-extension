<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\PositiveNumberConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class PositiveNumberConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new PositiveNumberConstraint());
    }

    /**
     * @test
     *
     * @dataProvider positiveNumberProvider
     */
    public function it_should_throw_no_exception_when_number_is_positive(float|int $number): void
    {
        $constraint = new PositiveNumberConstraint();
        $constraint->evaluate($number);

        self::expectNotToPerformAssertions();
    }

    /**
     * @return array<string, array{0: float|int}>
     */
    public static function positiveNumberProvider(): array
    {
        return [
            'float' => [0.1],
            'precise-float' => [0.0000000000000001],
            'int' => [1],
            'max-int' => [PHP_INT_MAX],
        ];
    }

    /**
     * @test
     *
     * @dataProvider negativeNumberProvider
     */
    public function it_should_throw_exception_when_number_is_not_positive(float|int $number): void
    {
        self::expectException(ExpectationFailedException::class);

        $constraint = new PositiveNumberConstraint();
        $constraint->evaluate($number);
    }

    /**
     * @return array<string, array{0: float|int}>
     */
    public static function negativeNumberProvider(): array
    {
        return [
            'min-int' => [PHP_INT_MIN, false],
            'negative-number' => [-1, false],
            'zero' => [0, false],
        ];
    }
}
