<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\DateIntervalConstraint;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class DateIntervalConstraintTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider incorrectFormatDataProvider
     */
    public function it_should_throw_an_exception_when_interval_string_does_not_have_correct_format(string $format): void
    {
        self::expectException(ExpectationFailedException::class);

        (new DateIntervalConstraint())->evaluate($format);
    }

    /**
     * @test
     *
     * @dataProvider correctFormatDataProvider
     */
    public function it_should_not_throw_an_exception_when_interval_string_does_have_correct_format(string $format): void
    {
        (new DateIntervalConstraint())->evaluate($format);

        self::expectNotToPerformAssertions();
    }

    /**
     * @return array<string, array{0:string}>
     */
    public static function correctFormatDataProvider(): array
    {
        return [
            'has minutes' => ['PT42M11S'],
            'has hours' => ['PT2H40M10S'],
            'has days' => ['P2DT11H5M8S'],
        ];
    }

    /**
     * @return array<string, array{0:string}>
     */
    public static function incorrectFormatDataProvider(): array
    {
        return [
            'has banana' => ['Banana'],
            'has seconds before minutes' => ['PT13S12M'],
            'has unicorn' => ['not unicorn'],
        ];
    }
}
