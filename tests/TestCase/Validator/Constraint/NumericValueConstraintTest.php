<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\NumericValueConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class NumericValueConstraintTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider notNumericDataProvider
     */
    public function it_should_throw_exception_when_value_is_not_numeric(mixed $value): void
    {
        self::expectException(ExpectationFailedException::class);

        (new NumericValueConstraint())->evaluate($value);
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function notNumericDataProvider(): array
    {
        return [
            'Empty string is not numeric' => [''],
            'Only letters is not numeric' => ['hello'],
            'Mix alpha-numeric is not numeric' => ['h3ll0'],
            'Command is not numeric delimited' => ['1,2'],
            'Hex representation is not numeric' => ['0xAD'],
            'Binary representation is not numeric' => ['0b010110'],
            'Array is not numeric' => [[]],
            'Null is not numeric' => [null],
        ];
    }

    /**
     * @test
     *
     * @dataProvider numericDataProvider
     */
    public function it_should_not_throw_exception_when_value_is_numeric(mixed $value): void
    {
        (new NumericValueConstraint())->evaluate($value);

        self::expectNotToPerformAssertions();
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function numericDataProvider(): array
    {
        return [
            'Only digits is numeric' => ['1234'],
            'Float numbers are numeric' => ['3.1415'],
            'Number is numeric' => [123],
            'Float is numeric' => [3.14],
            'Math notation is numeric' => ['12e10'],
        ];
    }
}
