<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Filesystem\FileMetadata;
use Hgraca\PhpExtension\Filesystem\FileSize;
use Hgraca\PhpExtension\Filesystem\MimeType;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\OnlyAllowedMimeTypeConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class OnlyAllowedMimeTypeConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new OnlyAllowedMimeTypeConstraint());
    }

    /**
     * @test
     */
    public function it_should_constraint_file_for_not_allowed_mime_type(): void
    {
        $constraint = new OnlyAllowedMimeTypeConstraint(MimeType::imagePng(), MimeType::imageJpeg());

        self::expectException(ExpectationFailedException::class);

        $constraint->evaluate($this->createFileMetaData(MimeType::imageSvg()));
    }

    /**
     * @test
     *
     * @dataProvider validMimeTypeProvider
     */
    public function it_should_not_constraint_file_for_allowed_mime_type(MimeType $mimeType): void
    {
        (new OnlyAllowedMimeTypeConstraint(MimeType::imagePng(), MimeType::imageJpeg()))
            ->evaluate($this->createFileMetaData($mimeType));

        self::expectNotToPerformAssertions();
    }

    public static function validMimeTypeProvider(): array
    {
        return [
            'Mime type jpeg' => [MimeType::imageJpeg()],
            'Mime type png' => [MimeType::imagePng()],
        ];
    }

    private function createFileMetaData(MimeType $mimeType)
    {
        return new FileMetadata(
            '/some/path',
            new FileSize(1),
            $mimeType
        );
    }
}
