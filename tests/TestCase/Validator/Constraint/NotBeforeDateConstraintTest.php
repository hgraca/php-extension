<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use DateInterval;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\NotBeforeDateConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class NotBeforeDateConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(
            ConstraintInterface::class,
            new NotBeforeDateConstraint(
                DateTimeGenerator::generate()
            )
        );
    }

    /**
     * @test
     */
    public function it_should_not_throw_exception_if_date_is_after(): void
    {
        $referenceDate = DateTimeGenerator::generate();
        $actualDate = DateTimeGenerator::generate()->add(new DateInterval('P5D'));

        (new NotBeforeDateConstraint($referenceDate))
            ->evaluate($actualDate);

        self::expectNotToPerformAssertions();
    }

    /**
     * @test
     */
    public function it_should_throw_exception_if_date_is_not_after(): void
    {
        $referenceDate = DateTimeGenerator::generate();
        $actualDate = DateTimeGenerator::generate()->sub(new DateInterval('P5D'));

        self::expectException(ExpectationFailedException::class);

        (new NotBeforeDateConstraint($referenceDate))
            ->evaluate($actualDate);
    }
}
