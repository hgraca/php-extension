<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\String\StringHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Test\Framework\TestDouble\FakeConstraint;
use Hgraca\PhpExtension\Validator\Constraint\AbstractConstraint;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class AbstractConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new FakeConstraint());
        self::assertInstanceOf(AbstractConstraint::class, new FakeConstraint());
    }

    /**
     * @test
     */
    public function it_should_throw_when_constraint_throws(): void
    {
        $exception = new ExpectationFailedException();
        $constraint = new FakeConstraint();
        $constraint->setUpToThrow($exception);

        self::expectExceptionObject($exception);

        $constraint->evaluate('');
    }

    /**
     * @test
     */
    public function it_should_not_throw_when_constraint_pass(): void
    {
        $constraint = new FakeConstraint();
        $constraint->setUpToPass();

        $constraint->evaluate('');

        self::expectNotToPerformAssertions();
    }

    /**
     * @test
     */
    public function it_should_throw_exception_with_original_error_message(): void
    {
        $originalErrorMessage = 'Original error';

        $exception = new ExpectationFailedException($originalErrorMessage);

        $constraint = new FakeConstraint();
        $constraint->setUpToThrow($exception);

        self::expectExceptionMessage($originalErrorMessage);

        $constraint->evaluate('');
    }

    /**
     * @test
     */
    public function it_should_should_override_original_exception_message(): void
    {
        $originalErrorMessage = 'Original error';
        $customErrorMessage = 'Custom error';

        $exception = new ExpectationFailedException($originalErrorMessage);

        $constraint = new FakeConstraint();
        $constraint->setUpToThrow($exception);

        self::expectExceptionMessage($customErrorMessage);

        $constraint->withErrorMessage($customErrorMessage)->evaluate('');
    }

    /**
     * @test
     */
    public function it_should_throw_exception_with_original_error_code(): void
    {
        $originalErrorCode = 'ERROR_CODE';

        $exception = new ExpectationFailedException(StringHelper::getRandomString(), 1, null, $originalErrorCode);

        $constraint = new FakeConstraint();
        $constraint->setUpToThrow($exception);

        self::expectExceptionObject($exception);

        $constraint->evaluate('');
    }

    /**
     * @test
     */
    public function it_should_override_original_error_code(): void
    {
        $originalErrorCode = 'Original error';
        $customErrorCode = 'Custom error';

        $exception = new ExpectationFailedException($originalErrorMessage = StringHelper::getRandomString(), 1, null, $originalErrorCode);

        $constraint = new FakeConstraint();
        $constraint->setUpToThrow($exception);

        $customException = new ExpectationFailedException($originalErrorMessage, 0, $exception, $customErrorCode);

        self::expectExceptionObject($customException);

        $constraint->withErrorCode($customErrorCode)->evaluate('');
    }
}
