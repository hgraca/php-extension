<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\IsStringConstraint;
use Hgraca\PhpExtension\Validator\Constraint\NotNullConstraint;
use stdClass;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class IsStringConstraintTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider notStringValuesProvider
     */
    public function it_should_throw_when_value_is_not_string(mixed $value): void
    {
        $this->expectException(ExpectationFailedException::class);

        $constraint = new IsStringConstraint();
        $constraint->evaluate($value);
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function notStringValuesProvider(): array
    {
        return [
            'int' => [1],
            'float' => [0.1],
            'array' => [[]],
            'object' => [new stdClass()],
            'bool' => [true],
        ];
    }

    /**
     * @test
     *
     * @dataProvider stringValuesProvider
     */
    public function it_should_not_throw_when_value_is_string(mixed $value): void
    {
        self::expectNotToPerformAssertions();

        $constraint = new NotNullConstraint();
        $constraint->evaluate($value);
    }

    /**
     * @return array<string, array{0: mixed}>
     */
    public static function stringValuesProvider(): array
    {
        return [
            'string' => ['foo'],
            'empty-string' => [''],
        ];
    }
}
