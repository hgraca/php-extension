<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\StringDateIsInThePastConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class StringDateIsInThePastConstraintTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider invalidDatesProvider
     */
    public function it_should_throw_when_value_is_not_valid(string $value): void
    {
        $this->expectException(ExpectationFailedException::class);

        $constraint = new StringDateIsInThePastConstraint();
        $constraint->evaluate($value);
    }

    /**
     * @return array<string, array{0: string}>
     */
    public static function invalidDatesProvider(): array
    {
        return [
            'string' => ['not a date'],
            'future date' => ['tomorrow'],
            'far future date' => ['+1 year'],
        ];
    }

    /**
     * @test
     *
     * @dataProvider validDatesProvider
     */
    public function it_should_not_throw_when_value_is_a_date_in_the_past(string $value): void
    {
        self::expectNotToPerformAssertions();

        $constraint = new StringDateIsInThePastConstraint();
        $constraint->evaluate($value);
    }

    /**
     * @return array<string, array{0: string}>
     */
    public static function validDatesProvider(): array
    {
        return [
            'past date' => ['yesterday'],
            'past date 2' => ['2021-01-01'],
            'far past date' => ['-1 year'],
            'far past date with time' => ['2010-01-01 12:00:00'],
        ];
    }
}
