<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\RequiredKeyConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class RequiredKeyConstraintTest extends AbstractTest
{
    private const ARRAY = [
        'existingKey' => 'Some value',
        'someOtherKey' => 'Some other value',
    ];

    /**
     * @test
     */
    public function it_should_throw_when_array_does_not_contain_key(): void
    {
        $this->expectException(ExpectationFailedException::class);

        $constraint = new RequiredKeyConstraint('missingKey');
        $constraint->evaluate(self::ARRAY);
    }

    /**
     * @test
     */
    public function it_should_not_throw_when_array_contains_key(): void
    {
        self::expectNotToPerformAssertions();

        $constraint = new RequiredKeyConstraint('existingKey');
        $constraint->evaluate(self::ARRAY);
    }
}
