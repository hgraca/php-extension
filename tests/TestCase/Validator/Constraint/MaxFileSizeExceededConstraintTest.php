<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Filesystem\FileMetadata;
use Hgraca\PhpExtension\Filesystem\FileSize;
use Hgraca\PhpExtension\Filesystem\MimeType;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\MaxFileSizeExceededConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class MaxFileSizeExceededConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new MaxFileSizeExceededConstraint(FileSize::fromMb(10)));
    }

    /**
     * @test
     */
    public function it_should_constraint_file_to_not_exceed_max_file_size(): void
    {
        $constraint = new MaxFileSizeExceededConstraint(FileSize::fromMb(10));

        self::expectException(ExpectationFailedException::class);

        $constraint->evaluate($this->createFileMetaData(FileSize::fromMb(12)));
    }

    /**
     * @test
     *
     * @dataProvider validFileSizeProvider
     */
    public function it_should_not_constraint_file_size_for_sizes_equal_or_lower_than_max_size(FileSize $fileSize): void
    {
        (new MaxFileSizeExceededConstraint(FileSize::fromMb(10)))
            ->evaluate($this->createFileMetaData($fileSize));

        self::expectNotToPerformAssertions();
    }

    public static function validFileSizeProvider(): array
    {
        return [
            'Size lower than max file size' => [FileSize::fromMb(5)],
            'Size equal to max file size' => [FileSize::fromMb(10)],
        ];
    }

    private function createFileMetaData(FileSize $fileSize)
    {
        return new FileMetadata(
            '/some/path',
            $fileSize,
            MimeType::imageJpeg()
        );
    }
}
