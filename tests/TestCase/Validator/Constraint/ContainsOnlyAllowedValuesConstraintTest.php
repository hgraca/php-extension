<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ContainsOnlyAllowedValuesConstraint;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class ContainsOnlyAllowedValuesConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new ContainsOnlyAllowedValuesConstraint([]));
    }

    /**
     * @test
     */
    public function it_should_not_throw_exception_when_it_contains_only_allowed_values(): void
    {
        $allowedValues = ['a', 'b', 'c', 'd'];
        $actualValues = ['a', 'b', 'c', 'd'];

        (new ContainsOnlyAllowedValuesConstraint($allowedValues))
            ->evaluate($actualValues);

        self::expectNotToPerformAssertions();
    }

    /**
     * @test
     */
    public function it_should_throw_exception_when_it_contains_not_allowed_values(): void
    {
        $allowedValues = [1, 2, 5];
        $actualValues = [1, 2, 3, 4, 5];

        self::expectException(ExpectationFailedException::class);

        (new ContainsOnlyAllowedValuesConstraint($allowedValues))
            ->evaluate($actualValues);
    }
}
