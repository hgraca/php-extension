<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\IntegerRangeConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class IntegerRangeConstraintTest extends AbstractTest
{
    /**
     * @test
     *
     * @dataProvider validRangesDataProvider
     */
    public function it_should_pass_for_value_when_inside_boundaries(int $bottomBoundary, int $topBoundary, int $value): void
    {
        $constraint = new IntegerRangeConstraint($bottomBoundary, $topBoundary);
        $constraint->evaluate($value);

        self::expectNotToPerformAssertions();
    }

    /**
     * @test
     *
     * @dataProvider invalidRangesDataProvider
     */
    public function it_should_throw_when_boundary_crossed(int $bottomBoundary, int $topBoundary, int $value): void
    {
        self::expectException(ExpectationFailedException::class);

        $constraint = new IntegerRangeConstraint($bottomBoundary, $topBoundary);
        $constraint->evaluate($value);
    }

    public static function validRangesDataProvider(): array
    {
        return [
            'Inside boundaries' => [0, 100, 50],
            'Only one possible value' => [1, 1, 1],
            'On bottom boundary' => [1, 100, 1],
            'On top boundary' => [1, 100, 100],
        ];
    }

    public static function invalidRangesDataProvider(): array
    {
        return [
            'Outside the top boundary' => [1, 100, 101],
            'Outside the bottom boundary' => [1, 100, 0],
        ];
    }
}
