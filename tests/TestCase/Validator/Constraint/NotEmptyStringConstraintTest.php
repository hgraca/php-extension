<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\String\StringHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\NotEmptyStringConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class NotEmptyStringConstraintTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new NotEmptyStringConstraint());
    }

    /**
     * @test
     */
    public function it_should_constrain_string_to_be_not_empty(): void
    {
        $constraint = new NotEmptyStringConstraint();
        $emptyString = '';

        self::expectException(ExpectationFailedException::class);

        $constraint->evaluate($emptyString);
    }

    /**
     * @test
     */
    public function it_should_not_constrain_not_empty_string(): void
    {
        $constraint = new NotEmptyStringConstraint();
        $notEmptyString = StringHelper::getRandomString(16);

        $constraint->evaluate($notEmptyString);

        self::expectNotToPerformAssertions();
    }
}
