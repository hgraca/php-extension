<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Validator\Constraint;

use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\MinStringLengthConstraint;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class MinStringLengthConstraintTest extends AbstractTest
{
    private const STRING_LENGTH = 4;

    /**
     * @test
     */
    public function it_should_implement_constraint_interface(): void
    {
        self::assertInstanceOf(ConstraintInterface::class, new MinStringLengthConstraint(self::STRING_LENGTH));
    }

    /**
     * @test
     */
    public function it_should_constrain_min_string_length(): void
    {
        $constraint = new MinStringLengthConstraint(self::STRING_LENGTH);
        $string = 'fig';

        self::expectException(ExpectationFailedException::class);

        $constraint->evaluate($string);
    }

    /**
     * @test
     */
    public function it_should_constrain_min_string_length_special_characters(): void
    {
        $constraint = new MinStringLengthConstraint(self::STRING_LENGTH);
        $string = 'éîö';

        self::expectException(ExpectationFailedException::class);

        $constraint->evaluate($string);
    }

    /**
     * @test
     */
    public function it_should_constrain_min_string_length_emojis(): void
    {
        $constraint = new MinStringLengthConstraint(self::STRING_LENGTH);
        $string = '🍌🍌🍌';

        self::expectException(ExpectationFailedException::class);

        $constraint->evaluate($string);
    }
}
