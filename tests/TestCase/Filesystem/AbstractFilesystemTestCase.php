<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Filesystem;

use Hgraca\PhpExtension\Filesystem\FilesystemException;
use Hgraca\PhpExtension\Filesystem\FilesystemInterface;
use Hgraca\PhpExtension\Filesystem\Mode;
use Hgraca\PhpExtension\Iterator\StringIterator;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @small
 *
 * @group unit
 *
 * @internal
 */
abstract class AbstractFilesystemTestCase extends AbstractTest
{
    protected const BASE_DIR_PATH = '/tmp/AbstractFilesystemTest';
    private const FILE_PATH = self::BASE_DIR_PATH . '/a/b/file';

    abstract protected function getFilesystem(): FilesystemInterface;

    /**
     * @test
     */
    public function it_throws_exception_if_directory_to_save_file_does_not_exist(): void
    {
        $filesystem = $this->getFilesystem();

        self::expectException(FilesystemException::class);
        self::expectExceptionMessage("Path '" . dirname(self::FILE_PATH) . "' does not exist.");
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator("x\n", "y\n", 'z'));
    }

    /**
     * @test
     */
    public function it_throws_exception_if_directory_in_path_to_write_file(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(self::FILE_PATH);

        self::expectException(FilesystemException::class);
        self::expectExceptionMessage(
            "A directory already exists in path '" . self::FILE_PATH . "'. Can not create file."
        );
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator("x\n", "y\n", 'z'));
    }

    /**
     * @test
     */
    public function it_writes_to_a_file(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(dirname(self::FILE_PATH));

        $content = 'abcdefg';
        $filesystem->write(self::FILE_PATH, $content);

        self::assertEquals($content, $filesystem->readFile(self::FILE_PATH));
    }

    /**
     * @test
     */
    public function it_writes_an_iterator_to_a_file(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(dirname(self::FILE_PATH));

        $dataList = ["x\n", "y\n", 'z'];
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator(...$dataList));

        self::assertEquals(implode('', $dataList), $filesystem->readFile(self::FILE_PATH));
    }

    /**
     * @test
     */
    public function can_use_iterator_to_overwrite_file_if_exists(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(dirname(self::FILE_PATH));

        $dataList = ["a\n", "b\n", 'c'];
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator(...$dataList));
        $dataList = ["x\n", "y\n", 'z'];
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator(...$dataList));

        self::assertEquals(implode('', $dataList), $filesystem->readFile(self::FILE_PATH));
    }

    /**
     * @test
     */
    public function can_use_iterator_to_append_to_file_if_exists(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(dirname(self::FILE_PATH));

        $dataList = ["x\n", "y\n", 'z'];
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator(...$dataList));
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator(...$dataList), Mode::append());

        self::assertEquals(implode('', [...$dataList, ...$dataList]), $filesystem->readFile(self::FILE_PATH));
    }

    /**
     * @test
     */
    public function it_creates_folders_indempotently(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(dirname(self::FILE_PATH));
        $filesystem->createDirectory(dirname(self::FILE_PATH));

        self::assertTrue($filesystem->hasDirectory(dirname(self::FILE_PATH)));
    }

    /**
     * @test
     */
    public function it_created_directory_and_saves_file_from_iterator_and_reads(): void
    {
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory(dirname(self::FILE_PATH));

        $dataList = ["x\n", "y\n", 'z'];
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator(...$dataList));

        self::assertEquals(implode('', $dataList), $filesystem->readFile(self::FILE_PATH));
    }

    /**
     * @test
     */
    public function it_throws_exception_it_trying_to_read_file_that_does_not_exist(): void
    {
        $filesystem = $this->getFilesystem();

        self::expectException(FilesystemException::class);
        self::expectExceptionMessage("File '" . self::FILE_PATH . "' does not exist.");

        $filesystem->readFile(self::FILE_PATH);
    }

    /**
     * @test
     */
    public function it_creates_directories_and_knows_if_they_are_there(): void
    {
        $dirPath = dirname(self::FILE_PATH);
        $filesystem = $this->getFilesystem();

        self::assertFalse($filesystem->hasDirectory($dirPath));

        $filesystem->createDirectory($dirPath);

        self::assertTrue($filesystem->hasDirectory($dirPath));
    }

    /**
     * @test
     */
    public function it_deletes_empty_directories(): void
    {
        $dirPath = dirname(self::FILE_PATH);
        $filesystem = $this->getFilesystem();
        $filesystem->createDirectory($dirPath);

        self::assertTrue($filesystem->hasDirectory($dirPath));

        $filesystem->delete($dirPath);

        self::assertFalse($filesystem->hasDirectory($dirPath));
    }

    /**
     * @test
     */
    public function it_deletes_non_empty_directories(): void
    {
        $dirPath = dirname(self::FILE_PATH);
        $filesystem = $this->getFilesystem();
        $filesystem->createDirectory($dirPath);
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator("x\n", "y\n", 'z'));

        self::assertTrue($filesystem->hasDirectory($dirPath));
        self::assertTrue($filesystem->hasFile(self::FILE_PATH));

        $filesystem->delete(dirname($dirPath));

        self::assertFalse($filesystem->hasFile(self::FILE_PATH));
        self::assertFalse($filesystem->hasDirectory($dirPath));
        self::assertFalse($filesystem->hasDirectory(dirname($dirPath)));
    }

    /**
     * @test
     */
    public function it_deletes_a_file(): void
    {
        $file2 = self::FILE_PATH . '2';
        $dirPath = dirname(self::FILE_PATH);
        $filesystem = $this->getFilesystem();
        $filesystem->createDirectory($dirPath);
        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator("x\n", "y\n", 'z'));
        $filesystem->writeFromIterator(self::FILE_PATH . '2', new StringIterator("x\n", "y\n", 'z'));

        self::assertTrue($filesystem->hasFile(self::FILE_PATH));
        self::assertTrue($filesystem->hasFile($file2));

        $filesystem->delete(self::FILE_PATH);

        self::assertFalse($filesystem->hasFile(self::FILE_PATH));
        self::assertTrue($filesystem->hasFile($file2));
    }

    /**
     * @test
     */
    public function it_distinguishes_between_file_and_directory(): void
    {
        $dirPath = dirname(self::FILE_PATH);
        $filesystem = $this->getFilesystem();

        $filesystem->createDirectory($dirPath);
        self::assertTrue($filesystem->hasDirectory($dirPath));
        self::assertFalse($filesystem->hasFile($dirPath));

        $filesystem->writeFromIterator(self::FILE_PATH, new StringIterator("x\n", "y\n", 'z'));
        self::assertTrue($filesystem->hasFile(self::FILE_PATH));
        self::assertFalse($filesystem->hasDirectory(self::FILE_PATH));
    }

    /**
     * @test
     */
    public function it_lists_all_content_from_a_directory_in_alphabetical_order(): void
    {
        $dirpath = dirname(self::FILE_PATH);
        $filesystem = $this->getFilesystem();

        $dirNames = [
            $dirpath . '/dirA',
            $dirpath . '/dirB',
        ];
        $fileNames = [
            $dirpath . '/a',
            $dirpath . '/b',
        ];

        $filesystem->createDirectory($dirpath);

        foreach ($dirNames as $dirName) {
            $filesystem->createDirectory($dirName);
        }
        foreach ($fileNames as $fileName) {
            $filesystem->write($fileName, '');
        }

        $dirContents = $filesystem->listDirectoryContent($dirpath);

        self::assertSame(
            [...$fileNames, ...$dirNames],
            $dirContents,
            'Expecting directory contents to be the same, only found: [' . implode(', ', $dirContents) . ']'
        );
    }

    /**
     * @test
     */
    public function it_can_get_last_modificatin_time_of_a_file(): void
    {
        $filesystem = $this->getFilesystem();

        $dirName = self::BASE_DIR_PATH;
        $fileName = $dirName . '/mtime.txt';
        $filesystem->createDirectory($dirName);
        $filesystem->write($fileName, 'Test');

        self::assertEqualsWithDelta(
            time(),
            $filesystem->getFileLastModificationTime($fileName)->getTimestamp(),
            1 // 1 Second difference allowed
        );
    }
}
