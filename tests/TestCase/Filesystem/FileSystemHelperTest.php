<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Filesystem;

use Hgraca\PhpExtension\Filesystem\FileSystemHelper;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class FileSystemHelperTest extends AbstractTest
{
    /**
     * @test
     */
    public function can_create_and_delete_dirs(): void
    {
        $tmpPath = __DIR__ . '/some/deep/file/path';

        self::assertFalse(is_dir($tmpPath) && is_file($tmpPath) && is_link($tmpPath));

        FileSystemHelper::createDirIfNotExists($tmpPath);

        self::assertTrue(is_dir($tmpPath));

        FileSystemHelper::deleteDir(__DIR__ . '/some');

        self::assertFalse(is_dir($tmpPath) && is_file($tmpPath) && is_link($tmpPath));
    }

    /**
     * @test
     *
     * @dataProvider provideFilePaths
     */
    public function it_should_know_if_a_file_path_is_usable(string $path, bool $expected): void
    {
        self::assertEquals($expected, FileSystemHelper::isCompliantFilePath($path));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideFilePaths(): array
    {
        return [
            ['/a/b/c.txt', true],
            ['a/b/c.txt', true],
            ['/c.txt', true],
            ['c.txt', true],
            ['/a/?/c.txt', false],
            ['a/*/c.txt', false],
        ];
    }

    /**
     * @test
     *
     * @dataProvider provideDirectoryPaths
     */
    public function it_should_know_if_a_directory_path_is_usable(string $path, bool $expected): void
    {
        self::assertEquals($expected, FileSystemHelper::isCompliantDirectoryPath($path));
    }

    /**
     * @return array<array{string, bool}>
     */
    public static function provideDirectoryPaths(): array
    {
        return [
            ['/a/b/c', true],
            ['a/b/c', true],
            ['/a', true],
            ['a', true],
            ['/a/?/c', false],
            ['a/*/c', false],
        ];
    }
}
