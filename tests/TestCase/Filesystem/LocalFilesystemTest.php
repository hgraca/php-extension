<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Filesystem;

use Hgraca\PhpExtension\Filesystem\FilesystemInterface;
use Hgraca\PhpExtension\Filesystem\LocalFilesystem;

/**
 * @medium
 *
 * @internal
 *
 * @group micro
 */
final class LocalFilesystemTest extends AbstractFilesystemTestCase
{
    private ?LocalFilesystem $filesystem = null;

    protected function getFilesystem(): FilesystemInterface
    {
        return $this->filesystem ?? $this->filesystem = new LocalFilesystem();
    }

    /**
     * @after
     */
    public function removeLeftoverFileStructure(): void
    {
        /** @phpstan-ignore-next-line */
        $this->filesystem->delete(self::BASE_DIR_PATH);
    }
}
