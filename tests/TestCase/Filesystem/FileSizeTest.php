<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase\Filesystem;

use Hgraca\PhpExtension\Filesystem\FileSize;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;

/**
 * @internal
 *
 * @small
 *
 * @group micro
 */
final class FileSizeTest extends AbstractTest
{
    /**
     * @test
     */
    public function it_should_create_instance_with_correct_number_of_bytes(): void
    {
        $fileSize = FileSize::fromMb(7);

        self::assertInstanceOf(FileSize::class, $fileSize);
        self::assertEquals($fileSize->inBytes(), 1024 * 1024 * 7);
    }
}
