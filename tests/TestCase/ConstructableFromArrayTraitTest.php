<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestCase;

use Exception;
use Hgraca\PhpExtension\Test\Framework\AbstractTest;
use Hgraca\PhpExtension\Test\TestSubject\DummyConstructableFromArray;
use ReflectionException;

/**
 * @small
 *
 * @internal
 */
final class ConstructableFromArrayTraitTest extends AbstractTest
{
    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function from_array(): void
    {
        $value = 123;

        $dto = DummyConstructableFromArray::fromArray(['prop1' => $value, 'inexistent' => 'foo']);

        self::assertInstanceOf(DummyConstructableFromArray::class, $dto);
        self::assertSame($value, $dto->getProp1());
        self::assertSame(0, $dto->getProp2());
    }

    /**
     * @test
     *
     * @throws ReflectionException
     */
    public function from_array__throws_exception_if_argument_is_missing(): void
    {
        $this->expectException(Exception::class);

        $value = 123;

        $dto = DummyConstructableFromArray::fromArray(['inexistent' => 'foo']);

        self::assertInstanceOf(DummyConstructableFromArray::class, $dto);
        self::assertSame($value, $dto->getProp1());
    }
}
