<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\TestSubject;

use Hgraca\PhpExtension\ConstructableFromArrayInterface;
use Hgraca\PhpExtension\ConstructableFromArrayTrait;

final class DummyConstructableFromArray implements ConstructableFromArrayInterface
{
    use ConstructableFromArrayTrait;

    /**
     * @param int $prop2
     */
    public function __construct(private readonly mixed $prop1, private $prop2 = 0)
    {
    }

    /**
     * @return mixed
     */
    public function getProp1()
    {
        return $this->prop1;
    }

    public function getProp2(): int
    {
        return $this->prop2;
    }
}
