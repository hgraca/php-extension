<?php

declare(strict_types=1);

namespace Hgraca\PhpExtension\Test\Framework\TestDouble;

use Hgraca\PhpExtension\Validator\Constraint\AbstractConstraint;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;

final class FakeConstraint extends AbstractConstraint
{
    private ?\Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException $exception = null;

    public function setUpToThrow(ExpectationFailedException $exception): void
    {
        $this->exception = $exception;
    }

    public function setUpToPass(): void
    {
        $this->exception = null;
    }

    protected function doEvaluate($value): void
    {
        if ($this->exception) {
            throw $this->exception;
        }
    }
}
