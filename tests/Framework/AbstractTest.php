<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Test\Framework;

use DateTimeImmutable;
use DateTimeZone;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\DateTime\DateTimeHelper;
use Hgraca\PhpExtension\Identity\Ulid\UlidGenerator;
use Hgraca\PhpExtension\Identity\UserFacingId\UserFacingIdGenerator;
use Hgraca\PhpExtension\Identity\Uuid\UuidGenerator;
use PHPUnit\Framework\TestCase;

/**
 * A unit test will test a method, class or set of classes in isolation from the tools and delivery mechanisms.
 * How isolated the test needs to be, it depends on the situation and how you decide to test the application.
 * However, it is important to note that these tests do not need to boot the application.
 */
abstract class AbstractTest extends TestCase
{
    /**
     * @after
     */
    public function resetDateTimeGenerator(): void
    {
        DateTimeGenerator::reset();
    }

    /**
     * @after
     */
    public function resetUuidGenerator(): void
    {
        UuidGenerator::reset();
    }

    /**
     * @after
     */
    public function resetUserFacingIdGenerator(): void
    {
        UserFacingIdGenerator::reset();
    }

    /**
     * @after
     */
    public function resetUlidGenerator(): void
    {
        UlidGenerator::reset();
    }

    protected function overrideDefaultDateTimeGenerator(string $overrideDateTime): void
    {
        DateTimeGenerator::overrideDefaultGenerator(
            fn(?string $givenDateTime = null, ?DateTimeZone $dateTimeZone = null): DateTimeImmutable => new DateTimeImmutable(
                $givenDateTime ?? $overrideDateTime,
                $dateTimeZone ?? DateTimeHelper::getDefaultDateTimeZone()
            )
        );
    }

    protected function overrideDefaultUuidGenerator(string $overrideUuid): void
    {
        UuidGenerator::overrideDefaultGenerator(
            fn(): string => $overrideUuid
        );
    }

    protected function overrideDefaultUserFacingIdGenerator(string $overrideUserFacingId): void
    {
        UserFacingIdGenerator::overrideDefaultGenerator(
            fn(): string => $overrideUserFacingId
        );
    }

    protected function overrideDefaultUlidGenerator(string $overrideUlid): void
    {
        UlidGenerator::overrideDefaultGenerator(
            fn(): string => $overrideUlid
        );
    }
}
