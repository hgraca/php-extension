<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Identity;

use Hgraca\PhpExtension\EqualityInterface;
use Hgraca\PhpExtension\ScalarObjectInterface;
use JsonSerializable;
use Stringable;

/**
 * Maybe we could avoid having this class and the inheritance tree here, but I find it nice because of the validation
 * it encapsulates, because of the global type hinting it provides, and also just as an example.
 */
abstract class AbstractId implements JsonSerializable, ScalarObjectInterface, EqualityInterface, Stringable
{
    /** @var scalar|Stringable */
    protected $id;

    /**
     * @param scalar|Stringable $id
     */
    public function __construct(mixed $id)
    {
        $this->validate($id);

        $this->id = $id;
    }

    /**
     * This is an example of the Template pattern, where this method is defined (templated) and used here,
     * but implemented in a subclass.
     *
     * @param scalar|Stringable $value
     */
    abstract protected static function isValid(mixed $value): bool;

    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function jsonSerialize(): string
    {
        return (string) $this->id;
    }

    /**
     * @param static $object
     */
    public function equals($object): bool
    {
        return static::class === $object::class
               && $this->id === $object->id;
    }

    /**
     * @param scalar|Stringable $id
     */
    protected function validate(mixed $id): void
    {
        if (!static::isValid($id)) {
            throw new InvalidIdException($id);
        }
    }
}
