<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Identity\UserFacingId;

final class UserFacingIdGenerator
{
    private const ALLOWED_CHARACTERS = '0123456789ABCDEFGHJKMNPQRSTVWXYZ';
    private const VALIDATION_REGEX = '/[0-9]{3}-[0-9]{3}-[%s]{4}/';

    /** @var ?callable(): string */
    private static $defaultGenerator;

    /** @var ?callable(): string */
    private static $customGenerator;

    public static function generate(): UserFacingId
    {
        $generator = self::resolveGenerator();

        return new UserFacingId($generator());
    }

    public static function generateAsString(): string
    {
        $generator = self::resolveGenerator();

        return $generator();
    }

    /**
     * @param callable(): string $defaultGenerator
     */
    public static function setDefaultGenerator(callable $defaultGenerator): void
    {
        if (self::$defaultGenerator !== null) {
            throw new DefaultUserFacingIdGeneratorAlreadySetException();
        }

        self::$defaultGenerator = $defaultGenerator;
    }

    /**
     * @param callable(): string $customGenerator
     */
    public static function overrideDefaultGenerator(callable $customGenerator): void
    {
        self::$customGenerator = $customGenerator;
    }

    public static function reset(): void
    {
        self::$customGenerator = null;
    }

    public static function validate(string $value): bool
    {
        $regex = sprintf(self::VALIDATION_REGEX, self::ALLOWED_CHARACTERS);

        return preg_match($regex, $value) === 1;
    }

    /**
     * @return callable(): string
     */
    private static function resolveGenerator(): callable
    {
        if (self::$customGenerator === null && self::$defaultGenerator === null) {
            throw new NoUserFacingIdGeneratorSetException();
        }

        /** @var callable(): string */
        return self::$customGenerator ?? self::$defaultGenerator;
    }
}
