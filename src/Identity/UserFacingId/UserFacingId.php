<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Identity\UserFacingId;

use Hgraca\PhpExtension\Identity\AbstractStringId;

final class UserFacingId extends AbstractStringId
{
    public function __construct(string $id = null)
    {
        if ($id !== null && self::isValid($id) === false) {
            throw new InvalidUserFacingIdStringException($id);
        }

        $this->id = $id ?? UserFacingIdGenerator::generateAsString();
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public function toString(): string
    {
        return (string) $this;
    }

    /**
     * @param mixed $value
     */
    public static function isValid($value): bool
    {
        return is_string($value) && UserFacingIdGenerator::validate($value);
    }

    /**
     * @param static $object
     */
    public function equals($object): bool
    {
        return $this instanceof $object
            && $this->id === $object->id;
    }
}
