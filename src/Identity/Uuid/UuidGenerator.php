<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Identity\Uuid;

use Closure;

final class UuidGenerator
{
    // private const VALIDATION_REGEX_V4 = '/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i';
    private const VALIDATION_REGEX_V7 = '/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i';

    /** @var ?callable(): string */
    private static $defaultGenerator;

    /** @var ?callable(): string */
    private static $customGenerator;

    public static function generate(): Uuid
    {
        $generator = self::resolveGenerator();

        return new Uuid($generator());
    }

    public static function generateAsString(): string
    {
        $generator = self::resolveGenerator();

        return $generator();
    }

    public static function setDefaultGenerator(Closure $defaultGenerator): void
    {
        if (self::$defaultGenerator !== null) {
            throw new DefaultUuidGeneratorAlreadySetException();
        }

        self::$defaultGenerator = $defaultGenerator;
    }

    /**
     * @param callable(): string $customGenerator
     */
    public static function overrideDefaultGenerator(callable $customGenerator): void
    {
        self::$customGenerator = $customGenerator;
    }

    public static function reset(): void
    {
        self::$customGenerator = null;
    }

    public static function validate(string $uuid): bool
    {
        return preg_match(self::VALIDATION_REGEX_V7, $uuid) === 1;
    }

    /**
     * @return callable(): string
     */
    private static function resolveGenerator(): callable
    {
        if (self::$customGenerator === null && self::$defaultGenerator === null) {
            throw new NoUuidGeneratorSetException();
        }

        /** @var callable(): string */
        return self::$customGenerator ?? self::$defaultGenerator;
    }
}
