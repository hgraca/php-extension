<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Identity\Ulid;

use Hgraca\PhpExtension\Identity\AbstractId;

use function is_string;

/**
 * @property string $id
 */
abstract class AbstractUlid extends AbstractId
{
    final public const VALID_ULID_REGEX = '[a-zA-Z0-9]{26}';

    final public function __construct(string $ulid = null)
    {
        if ($ulid !== null && static::isValid($ulid) === false) {
            throw new InvalidUlidStringException($ulid);
        }

        $this->id = $ulid ?? UlidGenerator::generate();
    }

    public function toScalar(): string
    {
        return (string) $this;
    }

    public function toString(): string
    {
        return (string) $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    /**
     * @param static $object
     */
    public function equals($object): bool
    {
        return $this->id === $object->toScalar();
    }

    /**
     * @param mixed $value
     */
    public static function isValid($value): bool
    {
        return is_string($value) && UlidGenerator::validate($value);
    }
}
