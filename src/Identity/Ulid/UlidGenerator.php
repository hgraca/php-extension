<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Identity\Ulid;

use function strlen;
use function strspn;

final class UlidGenerator
{
    /** @var ?callable(): string */
    private static $defaultGenerator;

    /** @var ?callable(): string */
    private static $customGenerator;

    /**
     * This generator intentionally does not generate Ulid objects, but only ulid strings.
     * This is because we should not use a generic type Ulid (and why it doesn't even exist), we should always
     * use a specific Ulid type, who extends from AbstractUlid.
     * This gives us strong typing.
     */
    public static function generate(): string
    {
        $generator = self::resolveGenerator();

        return $generator();
    }

    /**
     * @param callable(): string $defaultGenerator
     */
    public static function setDefaultGenerator(callable $defaultGenerator): void
    {
        if (self::$defaultGenerator !== null) {
            throw new DefaultUlidGeneratorAlreadySetException();
        }

        self::$defaultGenerator = $defaultGenerator;
    }

    /**
     * @param callable(): string $customGenerator
     */
    public static function overrideDefaultGenerator(callable $customGenerator): void
    {
        self::$customGenerator = $customGenerator;
    }

    public static function reset(): void
    {
        self::$customGenerator = null;
    }

    public static function validate(string $ulid): bool
    {
        if (strlen($ulid) !== 26) {
            return false;
        }

        if (strspn($ulid, '0123456789ABCDEFGHJKMNPQRSTVWXYZabcdefghjkmnpqrstvwxyz') !== 26) {
            return false;
        }

        return $ulid[0] <= '7';
    }

    /**
     * @return callable(): string
     */
    private static function resolveGenerator(): callable
    {
        if (self::$customGenerator === null && self::$defaultGenerator === null) {
            throw new NoUlidGeneratorSetException();
        }

        /** @var callable(): string */
        return self::$customGenerator ?? self::$defaultGenerator;
    }
}
