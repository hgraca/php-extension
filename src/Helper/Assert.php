<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Helper;

use Hgraca\PhpExtension\Exception\UnexpectedValueException;
use Hgraca\PhpExtension\Validator\Constraint\ConstraintInterface;
use Hgraca\PhpExtension\Validator\Constraint\ExpectationFailedException;
use Hgraca\PhpExtension\Validator\Constraint\IsStringConstraint;
use Hgraca\PhpExtension\Validator\Constraint\NotEmptyStringConstraint;
use Hgraca\PhpExtension\Validator\Constraint\NotNullConstraint;
use Hgraca\PhpExtension\Validator\Constraint\PositiveNumberConstraint;

final class Assert
{
    /**
     * @phpstan-assert !null $value
     *
     * @psalm-assert !null $value
     *
     * @throws UnexpectedValueException
     */
    public static function notNull(mixed $value, string $message): void
    {
        self::evaluateConstraint(new NotNullConstraint(), $value, $message);
    }

    /**
     * @throws UnexpectedValueException
     */
    public static function notEmptyString(string $value, string $message): void
    {
        self::evaluateConstraint(new NotEmptyStringConstraint(), $value, $message);
    }

    /**
     * @throws UnexpectedValueException
     */
    public static function isPositive(int|float $value, string $message): void
    {
        self::evaluateConstraint(new PositiveNumberConstraint(), $value, $message);
    }

    /**
     * @phpstan-assert string $value
     *
     * @psalm-assert string $value
     *
     * @throws UnexpectedValueException
     */
    public static function isString(mixed $value, string $message): void
    {
        self::evaluateConstraint(new IsStringConstraint(), $value, $message);
    }

    /**
     * @template TValue
     *
     * @param TValue $value
     * @param ConstraintInterface<TValue> ...$constraints
     *
     * @throws UnexpectedValueException
     */
    public static function that($value, ConstraintInterface ...$constraints): void
    {
        foreach ($constraints as $constraint) {
            self::evaluateConstraint($constraint, $value);
        }
    }

    /**
     * @template TValue
     *
     * @param ConstraintInterface<TValue> $constraint
     * @param TValue $value
     */
    private static function evaluateConstraint(ConstraintInterface $constraint, $value, string $message = null): void
    {
        try {
            $constraint->evaluate($value);
        } catch (ExpectationFailedException $exception) {
            throw new UnexpectedValueException($message ?? $exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
