<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Helper;

use Hgraca\PhpExtension\AbstractStaticClass;
use Hgraca\PhpExtension\Exception\OutOfBoundsException;
use Hgraca\PhpExtension\Exception\UnexpectedValueException;
use Hgraca\PhpExtension\String\StringHelper;

class ArrayHelper extends AbstractStaticClass
{
    final public const DEFAULT_PATH_SEPARATOR = '.';
    private const EXCEPTION_MESSAGE_EMPTY_VALUE = 'The value provided for $separator was an empty string.';

    public static function isKeyValueStringMap(array $array): bool
    {
        return self::hasOnlyStringKeys($array) && self::hasOnlyStringValues($array);
    }

    public static function hasOnlyStringKeys(array $array): bool
    {
        return count(array_filter(array_keys($array), 'is_string')) === count($array);
    }

    public static function hasOnlyStringValues(array $array): bool
    {
        return count(array_filter(array_values($array), 'is_string')) === count($array);
    }

    public static function filterByKeys(array $hayStack, array $allowedKeys): array
    {
        return array_intersect_key($hayStack, array_flip($allowedKeys));
    }

    public static function ensureAllStringsAreWrappedIn(
        string $leftEdge,
        string $rightEdge,
        array $list,
        callable $shouldSkip = null
    ): array {
        $shouldSkip ??= ArrayHelper::defaultShouldSkip(...);

        return self::arrayMapRecursive(
            $list,
            /** @param mixed $value */
            fn (mixed $value) => !is_string($value) || $shouldSkip($value)
                ? $value
                : StringHelper::ensureWrappedWithString($leftEdge, $rightEdge, $value)
        );
    }

    /**
     * @template TKey as array-key
     * @template TValue
     *
     * @param array<TKey, TValue> $array
     */
    public static function arrayMapRecursive(array $array, callable $callback): array
    {
        $out = [];
        foreach ($array as $key => $val) {
            $out[$key] = is_array($val)
                ? self::arrayMapRecursive($val, $callback)
                : call_user_func($callback, $val);
        }

        return $out;
    }

    /**
     * @template TKey as array-key
     * @template TValue
     *
     * @param array<TKey, TValue> $array1
     * @param array<TKey, TValue> $array2
     *
     * @return array<TKey, TValue>
     */
    public static function concatenateArrays(array $array1, array $array2): array
    {
        return array_merge($array1, $array2);
    }

    /**
     * @template ArrayT as array
     * @template T
     *
     * @param ArrayT $array
     * @param T $value
     *
     * @return ArrayT
     */
    public static function set(
        array $array,
        string $path,
        $value,
        bool $createPath = false,
        string $separator = DIRECTORY_SEPARATOR
    ): array {
        Assert::notEmptyString($separator, self::EXCEPTION_MESSAGE_EMPTY_VALUE);
        $currentPath = &$array;
        $unfilteredPathAsArray = explode($separator, $path);
        if ($unfilteredPathAsArray === false) {
            throw new UnexpectedValueException("Failed to explode the path '$path' with the separator '$separator'.");
        }
        $pathAsArray = array_filter($unfilteredPathAsArray);
        $lastKey = array_pop($pathAsArray);
        foreach ($pathAsArray as $key) {
            if (!isset($currentPath[$key])) {
                if ($createPath) {
                    $currentPath[$key] = [];
                } else {
                    throw new OutOfBoundsException("Path '$path' does not exist in the array.");
                }
            }
            /** @var array<string, mixed> $currentPath */
            $currentPath = &$currentPath[$key];
        }

        $currentPath[$lastKey] = $value;

        return $array;
    }

    /**
     * @template T as array
     *
     * @param T $array
     *
     * @return T
     */
    public static function unset(
        array $array,
        string $path,
        string $separator = DIRECTORY_SEPARATOR
    ): array {
        if ($separator === '') {
            throw new UnexpectedValueException(self::EXCEPTION_MESSAGE_EMPTY_VALUE);
        }
        $currentPath = &$array;
        $pathAsArray = array_filter(explode($separator, $path));
        $lastKey = array_pop($pathAsArray);
        foreach ($pathAsArray as $key) {
            if (!isset($currentPath[$key])) {
                throw new OutOfBoundsException("Path '$path' does not exist in the array.");
            }
            /** @var array $currentPath */
            $currentPath = &$currentPath[$key];
        }

        unset($currentPath[$lastKey]);

        return $array;
    }

    public static function has(
        array $array,
        string $path,
        string $separator = DIRECTORY_SEPARATOR
    ): bool {
        if ($separator === '') {
            throw new UnexpectedValueException(self::EXCEPTION_MESSAGE_EMPTY_VALUE);
        }
        $currentPath = &$array;
        $pathAsArray = array_filter(explode($separator, $path));
        $lastKey = array_pop($pathAsArray);
        foreach ($pathAsArray as $key) {
            if (!isset($currentPath[$key])) {
                return false;
            }
            /** @var array $currentPath */
            $currentPath = &$currentPath[$key];
        }

        return isset($currentPath[$lastKey]);
    }

    /**
     * @return mixed
     */
    public static function get(array $array, string $path, string $separator = DIRECTORY_SEPARATOR)
    {
        if ($separator === '') {
            throw new UnexpectedValueException(self::EXCEPTION_MESSAGE_EMPTY_VALUE);
        }
        $currentPath = &$array;
        $pathAsArray = array_filter(explode($separator, $path));
        $lastKey = array_pop($pathAsArray);
        foreach ($pathAsArray as $key) {
            if (!isset($currentPath[$key])) {
                throw new OutOfBoundsException("Path '$path' does not exist in the array.");
            }
            /** @var array $currentPath */
            $currentPath = &$currentPath[$key];
        }

        return $currentPath[$lastKey];
    }

    /**
     * @param string[]|int[] $subset
     * @param string[]|int[] $haystack
     */
    public static function containsSubset(array $subset, array $haystack): bool
    {
        return !array_diff($subset, $haystack);
    }

    /**
     * @param array<int|string, mixed> $array
     */
    public static function containsPath(
        string $path,
        array $array,
        string $separator = self::DEFAULT_PATH_SEPARATOR
    ): bool {
        if ($separator === '') {
            throw new UnexpectedValueException(self::EXCEPTION_MESSAGE_EMPTY_VALUE);
        }
        $currentPath = &$array;
        $pathAsArray = array_filter(explode($separator, $path));
        foreach ($pathAsArray as $key) {
            if (!isset($currentPath[$key])) {
                return false;
            }
            /** @var array $currentPath */
            $currentPath = &$currentPath[$key];
        }

        return true;
    }

    /**
     * @param string $pattern A string or glob pattern
     * @param string ...$haystack A list of strings or glob patterns
     */
    public static function containsGlobMatch(string $pattern, string ...$haystack): bool
    {
        foreach ($haystack as $straw) {
            if (fnmatch($pattern, $straw) || fnmatch($straw, $pattern)) {
                return true;
            }
        }

        return false;
    }

    private static function defaultShouldSkip(): bool
    {
        return false;
    }
}
