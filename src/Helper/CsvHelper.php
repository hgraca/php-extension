<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Helper;

use Hgraca\PhpExtension\AbstractStaticClass;

final class CsvHelper extends AbstractStaticClass
{
    /**
     * @param string[] $fields
     */
    public static function arrayRowToCsv(
        array $fields,
        string $separator = ',',
        string $enclosure = '"',
        string $escape = '\\'
    ): string {
        if (is_array(current($fields))) {
            throw new CsvException('Can only convert one dimensional arrays to CSV.');
        }

        $buffer = fopen('php://temp', 'r+');
        fputcsv($buffer, $fields, $separator, $enclosure, $escape);
        rewind($buffer);
        $csv = fgets($buffer);
        fclose($buffer);

        return $csv;
    }
}
