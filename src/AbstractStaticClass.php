<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension;

/**
 * This class is just an utility class that helps us to remove duplication from the tests
 * and that's why it can't be instantiated.
 */
abstract class AbstractStaticClass
{
    protected function __construct()
    {
        // All methods should be static, so no need to instantiate any of the subclasses
    }
}
