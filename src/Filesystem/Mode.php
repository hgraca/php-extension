<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use Hgraca\PhpExtension\Enum\AbstractEnum;

/**
 * @method static self append()
 * @method bool   isAppend()
 * @method static self write()
 * @method bool   isWrite()
 */
final class Mode extends AbstractEnum
{
    protected const APPEND = 'a';
    protected const WRITE = 'w';
}
