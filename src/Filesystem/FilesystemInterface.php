<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use DateTimeImmutable;
use Hgraca\PhpExtension\Iterator\StringIteratorInterface;

interface FilesystemInterface
{
    public function write(string $absoluteFilePath, string $content, Mode $mode = null): void;

    public function writeFromIterator(
        string $absoluteFilePath,
        StringIteratorInterface $content,
        Mode $mode = null
    ): void;

    public function createDirectory(string $absoluteDirPath): void;

    public function readFile(string $absoluteFilePath): string;

    public function delete(string $absoluteGlobPathPattern): void;

    public function hasFile(string $absoluteFilePath): bool;

    public function hasDirectory(string $absoluteDirPath): bool;

    public function getFileLastModificationTime(string $absoluteFilePath): DateTimeImmutable;

    /**
     * @return string[]
     */
    public function listDirectoryContent(string $absoluteDirPath): array;
}
