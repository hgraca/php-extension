<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use DateTimeImmutable;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\Helper\ArrayHelper;
use Hgraca\PhpExtension\Iterator\StringIterator;
use Hgraca\PhpExtension\Iterator\StringIteratorInterface;
use Hgraca\PhpExtension\String\Json\JsonHelper;
use Hgraca\PhpExtension\String\StringHelper;

final class InMemoryFilesystem implements FilesystemInterface
{
    /** @var array<string, array<string, string>|string> */
    private array $filesystem = [];

    public function write(string $absoluteFilePath, string $content, Mode $mode = null): void
    {
        $this->writeFromIterator($absoluteFilePath, new StringIterator($content), $mode);
    }

    public function writeFromIterator(
        string $absoluteFilePath,
        StringIteratorInterface $content,
        Mode $mode = null
    ): void {
        if ($this->hasDirectory($absoluteFilePath)) {
            throw new FilesystemException(
                "A directory already exists in path '$absoluteFilePath'. Can not create file."
            );
        }

        $dirPath = dirname($absoluteFilePath);
        if (!$this->hasDirectory($dirPath)) {
            throw new FilesystemException(
                "Path '$dirPath' does not exist. File system: "
                . JsonHelper::encode($this->filesystem, JSON_PRETTY_PRINT
                )
            );
        }

        $contentString = '';
        $mode ??= Mode::write();
        if ($mode->isAppend() && $this->hasFile($absoluteFilePath)) {
            /** @var array{content: string, modification_time: int} $fileData */
            $fileData = ArrayHelper::get($this->filesystem, $absoluteFilePath);
            $contentString = $fileData['content'];
        }

        while ($row = $content->current()) {
            $contentString .= $row;
            $content->next();
        }

        $this->filesystem = ArrayHelper::set(
            $this->filesystem,
            $absoluteFilePath,
            [
                'content' => $contentString,
                'modification_time' => DateTimeGenerator::generate()->getTimestamp(),
            ]
        );
    }

    public function createDirectory(string $absoluteDirPath): void
    {
        if (!$this->hasFile($absoluteDirPath) && !$this->hasDirectory($absoluteDirPath)) {
            $this->filesystem = ArrayHelper::set($this->filesystem, $absoluteDirPath, [], true);
        }
    }

    public function readFile(string $absoluteFilePath): string
    {
        if (!$this->hasFile($absoluteFilePath)) {
            throw new FilesystemException("File '$absoluteFilePath' does not exist.");
        }

        /** @var array{content: string, modification_time: int} $fileData */
        $fileData = ArrayHelper::get($this->filesystem, $absoluteFilePath);

        return $fileData['content'];
    }

    public function delete(string $absoluteGlobPathPattern): void
    {
        $allPaths = $this->getAllPaths($this->filesystem);

        foreach ($allPaths as $path) {
            if (StringHelper::isGlobMatch($absoluteGlobPathPattern, $path)) {
                $this->filesystem = ArrayHelper::unset($this->filesystem, $path);
            }
        }
    }

    public function hasFile(string $absoluteFilePath): bool
    {
        if (ArrayHelper::has($this->filesystem, $absoluteFilePath) === false) {
            return false;
        }

        return $this->isFile(ArrayHelper::get($this->filesystem, $absoluteFilePath));
    }

    public function hasDirectory(string $absoluteDirPath): bool
    {
        if (ArrayHelper::has($this->filesystem, $absoluteDirPath) === false) {
            return false;
        }

        return $this->isDirectory(ArrayHelper::get($this->filesystem, $absoluteDirPath));
    }

    public function getFileLastModificationTime(string $absoluteFilePath): DateTimeImmutable
    {
        if ($this->hasFile($absoluteFilePath) === false) {
            throw new FilesystemException("File '$absoluteFilePath' does not exist.");
        }

        /** @var array{content: string, modification_time: int} $fileData */
        $fileData = ArrayHelper::get($this->filesystem, $absoluteFilePath);

        return DateTimeGenerator::generate()->setTimestamp($fileData['modification_time']);
    }

    /**
     * @return string[]
     */
    public function listDirectoryContent(string $absoluteDirPath): array
    {
        $absoluteDirPath = FileSystemHelper::sanitizeRootPath($absoluteDirPath);

        if ($this->hasDirectory($absoluteDirPath) === false) {
            throw new DirectoryNotFoundException(
                'The directory you want to list its content does not exist, check the project directories'
            );
        }

        /** @var array<string, string|array> $dirContents */
        $dirContents = ArrayHelper::get($this->filesystem, $absoluteDirPath);

        $dirContentsNames = array_keys($dirContents);
        sort($dirContentsNames);

        $prefixedContents = [];
        foreach ($dirContentsNames as $item) {
            $prefixedContents[] = "$absoluteDirPath/$item";
        }

        return $prefixedContents;
    }

    /**
     * @param array<string, array<string, string>|string> $array
     *
     * @return string[]
     */
    private function getAllPaths(array $array, bool $isRoot = true): array
    {
        $childPathList = [];
        foreach ($array as $name => $child) {
            $childPathList[] = $name;
            if ($this->isDirectory($child)) {
                /** @var array<string, array<string, string>|string> $child */
                $nestedChildPathList = $this->getAllPaths($child, false);
                foreach ($nestedChildPathList as $nestedChildPath) {
                    $childPathList[] = $name . DIRECTORY_SEPARATOR . $nestedChildPath;
                }
            }
        }

        return $isRoot
            ? array_map(
                static fn (string $path) => DIRECTORY_SEPARATOR . $path,
                $childPathList
            )
            : $childPathList;
    }

    private function isFile(mixed $data): bool
    {
        return is_array($data) && isset($data['content'], $data['modification_time']);
    }

    private function isDirectory(mixed $data): bool
    {
        return !$this->isFile($data);
    }
}
