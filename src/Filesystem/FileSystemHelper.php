<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use Hgraca\PhpExtension\AbstractStaticClass;
use InvalidArgumentException;

final class FileSystemHelper extends AbstractStaticClass
{
    public static function createDirIfNotExists(string $path, int $mode = 0744, bool $recursive = true): void
    {
        if (!is_dir($path) && !is_file($path) && !is_link($path)) {
            mkdir($path, $mode, $recursive);
        }
    }

    public static function deleteDir(string $dirPath): void
    {
        $dirPath = rtrim($dirPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        if (!file_exists($dirPath)) {
            return;
        }

        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("The given path is not a directory: '$dirPath'");
        }

        $fileList = glob($dirPath . '*', GLOB_MARK);
        foreach ($fileList as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }

        rmdir($dirPath);
    }

    public static function sanitizeRelativePath(string $path): string
    {
        return DIRECTORY_SEPARATOR . trim($path, DIRECTORY_SEPARATOR);
    }

    public static function sanitizeRootPath(string $path): string
    {
        return rtrim($path, DIRECTORY_SEPARATOR);
    }

    public static function isCompliantFilePath(string $path): bool
    {
        return preg_match('#^(\/?\w*)*\.\w+$#', $path) === 1;
    }

    public static function isCompliantDirectoryPath(string $path): bool
    {
        return preg_match('#^(\/?\w*)*$#', $path) === 1;
    }
}
