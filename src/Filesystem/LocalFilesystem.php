<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use DateTimeImmutable;
use FilesystemIterator;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\Iterator\StringIterator;
use Hgraca\PhpExtension\Iterator\StringIteratorInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

final class LocalFilesystem implements FilesystemInterface
{
    public function write(string $absoluteFilePath, string $content, Mode $mode = null): void
    {
        $this->writeFromIterator($absoluteFilePath, new StringIterator($content), $mode);
    }

    public function writeFromIterator(
        string $absoluteFilePath,
        StringIteratorInterface $content,
        Mode $mode = null
    ): void {
        if ($this->hasDirectory($absoluteFilePath)) {
            throw new FilesystemException(
                "A directory already exists in path '$absoluteFilePath'. Can not create file."
            );
        }

        $dirPath = dirname($absoluteFilePath);
        if (!$this->hasDirectory($dirPath)) {
            throw new FilesystemException("Path '$dirPath' does not exist.");
        }

        $fileStream = fopen($absoluteFilePath, (string) ($mode ?? Mode::write()));
        if ($fileStream === false) {
            throw new FilesystemException("Failed to open file '$absoluteFilePath' for writing.");
        }

        while ($row = $content->current()) {
            fwrite($fileStream, $row);
            $content->next();
        }

        fclose($fileStream);
    }

    public function createDirectory(string $absoluteDirPath): void
    {
        if (!file_exists($absoluteDirPath) || !is_dir($absoluteDirPath)) {
            $success = mkdir($absoluteDirPath, 0777, true);
            if (!$success) {
                throw new FilesystemException("Could not create directory '$absoluteDirPath'.");
            }
        }
    }

    public function readFile(string $absoluteFilePath): string
    {
        if (!$this->hasFile($absoluteFilePath)) {
            throw new FilesystemException("File '$absoluteFilePath' does not exist.");
        }

        return file_get_contents($absoluteFilePath);
    }

    public function delete(string $absoluteGlobPathPattern): void
    {
        array_map(
            function (string $path): void {
                if ($this->hasDirectory($path)) {
                    $this->deleteDirectory($path);
                } else {
                    $this->deleteFile($path);
                }
            },
            glob($absoluteGlobPathPattern) ?: []
        );
    }

    public function hasFile(string $absoluteFilePath): bool
    {
        return file_exists($absoluteFilePath) && !is_dir($absoluteFilePath);
    }

    public function hasDirectory(string $absoluteDirPath): bool
    {
        return file_exists($absoluteDirPath) && is_dir($absoluteDirPath);
    }

    public function getFileLastModificationTime(string $absoluteFilePath): DateTimeImmutable
    {
        if ($this->hasFile($absoluteFilePath) === false) {
            throw new FilesystemException("File '$absoluteFilePath' does not exist.");
        }

        $fileInformation = filemtime($absoluteFilePath);
        if ($fileInformation === false) {
            throw new FilesystemException("No information found about the requested file '{$absoluteFilePath}'");
        }

        return DateTimeGenerator::generate()->setTimestamp($fileInformation);
    }

    /**
     * @return string[]
     */
    public function listDirectoryContent(string $absoluteDirPath): array
    {
        $absoluteDirPath = FileSystemHelper::sanitizeRootPath($absoluteDirPath);

        if ($this->hasDirectory($absoluteDirPath) === false) {
            throw new DirectoryNotFoundException(
                'The directory you want to list its content does not exist, check the project directories'
            );
        }

        /** @var string[] $storageContent */
        $storageContent = array_diff(
            scandir($absoluteDirPath),
            ['.', '..'] // Always remove . and .. directories
        );

        $prefixedContents = [];
        foreach ($storageContent as $item) {
            $prefixedContents[] = "$absoluteDirPath/$item";
        }

        return $prefixedContents;
    }

    private function deleteFile(string $absoluteGlobPathPattern): void
    {
        if ($this->hasFile($absoluteGlobPathPattern)) {
            unlink($absoluteGlobPathPattern);
        }
    }

    private function deleteDirectory(string $absoluteGlobPathPattern): void
    {
        if (!$this->hasDirectory($absoluteGlobPathPattern)) {
            return;
        }

        $directoryIterator = new RecursiveDirectoryIterator(
            $absoluteGlobPathPattern,
            RecursiveDirectoryIterator::SKIP_DOTS | FilesystemIterator::SKIP_DOTS
        );
        $files = new RecursiveIteratorIterator($directoryIterator, RecursiveIteratorIterator::CHILD_FIRST);
        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            if ($file->isDir()) {
                $this->deleteDirectory($file->getRealPath());
            } else {
                $this->deleteFile($file->getRealPath());
            }
        }
        rmdir($absoluteGlobPathPattern);
    }
}
