<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use finfo;
use InvalidArgumentException;

final class LocalFileSystemMimeTypeHelper
{
    public static function isImage(string $absolutePath): bool
    {
        return in_array(self::getMimeType($absolutePath), MimeType::allImageTypes());
    }

    public static function isOfType(string $absolutePath, MimeType $mimeType): bool
    {
        return self::getMimeType($absolutePath) === (string) $mimeType;
    }

    public static function isOfAnyType(string $absolutePath, MimeType ...$mimeTypes): bool
    {
        $fileMimeType = self::getMimeType($absolutePath);

        return in_array($fileMimeType, $mimeTypes);
    }

    public static function getExtensionForMimeType(string $absolutePath): string
    {
        $fileMimeType = self::getMimeType($absolutePath);

        try {
            return MimeType::get($fileMimeType)->getDefaultExtension();
        } catch (InvalidArgumentException $e) {
            throw new NoExtensionFoundException("Unable to find extension for mimetype [{$fileMimeType}]", 0, $e);
        }
    }

    private static function getMimeType(string $absolutePath): string
    {
        $fileInfo = new finfo(FILEINFO_MIME_TYPE);

        if (file_exists($absolutePath) === false) {
            throw new FileNotFoundException("Unable to find file [{$absolutePath}]");
        }

        return $fileInfo->file($absolutePath);
    }
}
