<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Filesystem;

use Hgraca\PhpExtension\Enum\AbstractEnum;

/**
 * For MimeType reference list see this link:
 * http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
 *
 * @method static self imagePng()
 * @method bool isImagePng()
 * @method static self imageJpeg()
 * @method bool isImageJpeg()
 * @method static self imageGif()
 * @method bool isImageGif()
 * @method static self imageSvg()
 * @method bool isImageSvg()
 * @method static self imageTiff()
 * @method bool isImageTiff()
 * @method static self imageBmp()
 * @method bool isImageBmp()
 * @method static self imageMsBmp()
 * @method bool isImageMsBmp()
 * @method static self imageIco()
 * @method bool isImageIco()
 * @method static self imageMsIco()
 * @method bool isImageMsIco()
 * @method static bool imageWebp()
 * @method bool isImageWebp()
 * @method static bool textPlain()
 * @method bool isTextPlain()
 */
final class MimeType extends AbstractEnum
{
    private const IMAGE_PNG = 'image/png';
    private const IMAGE_JPEG = 'image/jpeg';
    private const IMAGE_GIF = 'image/gif';
    private const IMAGE_SVG = 'image/svg';
    private const IMAGE_TIFF = 'image/tiff';
    private const IMAGE_BMP = 'image/bmp';
    private const IMAGE_MS_BMP = 'image/x-ms-bmp';
    private const IMAGE_ICO = 'image/x-icon';
    private const IMAGE_MS_ICO = 'image/vnd.microsoft.icon';
    private const IMAGE_WEBP = 'image/webp';
    private const TEXT_PLAIN = 'text/plain';
    private const CSV = 'text/csv';

    private const IMAGE_TYPES = [
        self::IMAGE_PNG,
        self::IMAGE_JPEG,
        self::IMAGE_GIF,
        self::IMAGE_SVG,
        self::IMAGE_TIFF,
        self::IMAGE_BMP,
        self::IMAGE_MS_BMP,
        self::IMAGE_ICO,
        self::IMAGE_MS_ICO,
        self::IMAGE_WEBP,
    ];

    /**
     * @return list<string>
     */
    public static function allImageTypes(): array
    {
        return self::IMAGE_TYPES;
    }

    public function getDefaultExtension(): string
    {
        return match (true) {
            $this->isImagePng() => 'png',
            $this->isImageJpeg() => 'jpg',
            $this->isImageGif() => 'gif',
            $this->isImageSvg() => 'svg',
            $this->isImageTiff() => 'tiff',
            $this->isImageBmp(), $this->isImageMsBmp() => 'bmp',
            $this->isImageIco(), $this->isImageMsIco() => 'ico',
            $this->isImageWebp() => 'webp',
            $this->isTextPlain() => 'txt',
            default => throw new NoExtensionFoundException("Unable to find extension for MimeType [{$this}]"),
        };
    }
}
