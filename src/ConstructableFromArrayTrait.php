<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension;

use ReflectionException;
use ReflectionMethod;
use RuntimeException;

use function array_key_exists;
use function array_keys;
use function array_merge;
use function is_array;

trait ConstructableFromArrayTrait
{
    /**
     * @param array<string, mixed> $data
     *
     * @throws ReflectionException
     * @throws RuntimeException
     */
    public static function fromArray(array $data): static
    {
        $reflectionMethod = new ReflectionMethod(static::class, '__construct');
        $reflectionParameterList = $reflectionMethod->getParameters();

        $argumentList = [];
        foreach ($reflectionParameterList as $reflectionParameter) {
            $parameterName = $reflectionParameter->getName();
            if (
                !array_key_exists($parameterName, $data)
                && !$reflectionParameter->isOptional()
            ) {
                throw new RuntimeException(
                    "Can't instantiate '" . static::class . ' from an array'
                    . " because argument '$parameterName' is missing and it's not optional."
                    . ' Available argument names: ' . implode(', ', array_keys($data))
                );
            }

            $argument = $data[$parameterName] ?? $reflectionParameter->getDefaultValue();
            if (is_array($argument) && $reflectionParameter->isVariadic()) {
                $argumentList[] = $argument;
            } else {
                $argumentList[] = [$argument];
            }
        }

        return new static(...array_merge(...$argumentList));
    }
}
