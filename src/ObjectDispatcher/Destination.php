<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\ObjectDispatcher;

/**
 * This is just a DTO, it only has getters, theres no logic to test, so we ignore it for code coverage purposes.
 *
 * @codeCoverageIgnore
 */
final class Destination
{
    /** @var callable */
    private $receiver;

    public function __construct(callable $receiver, private readonly ?int $priority = null)
    {
        $this->receiver = $receiver;
    }

    public function getReceiver(): callable
    {
        return $this->receiver;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }
}
