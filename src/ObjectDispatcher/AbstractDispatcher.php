<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\ObjectDispatcher;

abstract class AbstractDispatcher
{
    /** @var Destination[][] */
    private array $objectDestinationMapper = [];

    public function addDestination(
        string $messageName,
        callable $destinationCallable,
        int $priority = 0
    ): void {
        $destinationList = $this->objectDestinationMapper[$messageName] ?? [];
        $order = $this->findIndexForInsertion($priority, ...$destinationList);
        $this->objectDestinationMapper[$messageName] = $this->insertDestinationInIndex(
            $destinationList,
            $destinationCallable,
            $priority,
            $order
        );
    }

    public function hasDestination(string $messageName): bool
    {
        return array_key_exists($messageName, $this->objectDestinationMapper)
               && !empty($this->objectDestinationMapper[$messageName]);
    }

    /**
     * @return callable[]
     */
    protected function getDestinationListForObject(string $messageName): array
    {
        $destinationList = $this->getDestinationDefinitionListForObject($messageName);

        foreach ($destinationList as $destination) {
            $receiverList[] = $destination->getReceiver();
        }

        return $receiverList ?? [];
    }

    /**
     * @return Destination[]
     */
    private function getDestinationDefinitionListForObject(string $messageName): array
    {
        return $this->objectDestinationMapper[$messageName] ?? [];
    }

    private function findIndexForInsertion(int $priority, Destination ...$destinationList): int
    {
        $i = 0;
        do {
            $destination = $destinationList[$i++] ?? null;
        } while ($destination !== null && $destination->getPriority() >= $priority);

        return --$i;
    }

    /**
     * @param Destination[] $destinationList
     *
     * @return array<int, Destination>
     */
    private function insertDestinationInIndex(
        array $destinationList,
        callable $destinationCallable,
        int $priority,
        int $i
    ): array {
        array_splice($destinationList, $i, 0, [new Destination($destinationCallable, $priority)]);

        return $destinationList;
    }
}
