<?php

declare(strict_types=1);

namespace Hgraca\PhpExtension\Exception;

final class NotImplementedException extends LogicException
{
}
