<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Exception;

use LogicException as PhpLogicException;

/**
 * Exception that represents an error in the program logic. This kind of
 * exceptions should directly lead to a fix in your code.
 *
 * In other words, these are exceptions that can happen, and should be
 * caught somewhere in our code and dealt with elegantly.
 * For example, when a user tries to register with a user name that already
 * exists, the persistence mechanism could throw a specific logic exception
 * which would be caught somewhere and show a nice error message to the user.
 *
 * This exception is in the PhpExtension, which means that it can be used in several projects of the same vendor.
 * Therefore, by catching this exception we might be catching an exception of another library created by
 * the same vendor.
 *
 * @see http://php.net/manual/en/class.logicexception.php
 */
class LogicException extends PhpLogicException implements ExceptionInterface
{
    use ExceptionTrait;
}
