<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Exception;

use RuntimeException as PhpRuntimeException;

/**
 * Exception thrown if an error which can only be found at runtime occurs.
 * During normal running of the application this exceptions should not occur.
 * So if they occur, there is a bug somewhere.
 *
 * In other words, these exceptions should never actually be thrown,
 * if they do then there is an error somewhere.
 * They are used only to signal the developers that there is a bug somewhere,
 * so they should not be caught, they should result in an application error.
 *
 * This exception is in the PhpExtension, which means that it can be used in several projects of the same vendor.
 * Therefore, by catching this exception we might be catching an exception of another library created by
 * the same vendor.
 *
 * @see http://php.net/manual/en/class.runtimeexception.php
 */
class RuntimeException extends PhpRuntimeException implements ExceptionInterface
{
    use ExceptionTrait;
}
