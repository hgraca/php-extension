<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Exception;

use Hgraca\PhpExtension\CodeUnit\ClassHelper;

class ValidationException extends InputException
{
    final public const DEFAULT_ERROR_CODE = 'UNKNOWN';

    /** @var array<string, array<string, ?string>> */
    private array $errorList = [];

    /** @var array<string, string> */
    private array $metadata = [];

    public function addError(
        string $fieldName,
        string $errorMessage,
        ?string $errorCode = self::DEFAULT_ERROR_CODE
    ): void {
        $this->errorList[$fieldName]['error_message'] = $errorMessage;
        $this->errorList[$fieldName]['error_code'] = $errorCode;
    }

    public function hasErrors(): bool
    {
        return count($this->errorList) > 0;
    }

    public function getErrorMessage(string $key): ?string
    {
        return $this->errorList[$key]['error_message'] ?? null;
    }

    public function getErrorCode(string $key): ?string
    {
        return $this->errorList[$key]['error_code'] ?? null;
    }

    public function addMetadata(string $key, string $value): void
    {
        $this->metadata[$key] = $value;
    }

    /**
     * @return array{message: string, metadata: array<mixed>, errors: array<string, array<mixed>>}
     */
    public function toArray(): array
    {
        return [
            'message' => $this->getMessage(),
            'metadata' => [...$this->metadata, 'exception' => ClassHelper::extractCanonicalClassName(static::class), 'code' => $this->getCode()],
            'errors' => $this->errorList,
        ];
    }

    public function throwIfError(): void
    {
        if ($this->hasErrors()) {
            throw $this;
        }
    }
}
