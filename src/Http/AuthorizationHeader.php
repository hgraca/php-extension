<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Http;

use Hgraca\PhpExtension\String\StringHelper;

enum AuthorizationHeader: string
{
    private const NAME = 'Authorization';

    case BEARER = 'Bearer';
    case BASIC = 'Basic';

    public static function name(): string
    {
        return self::NAME;
    }

    public static function createBearerHeader(string $credentials): string
    {
        return sprintf('%s %s', self::BEARER->value, $credentials);
    }

    public static function extractBearerToken(string $authHeader): string
    {
        return StringHelper::leftStringReplace(self::BEARER->value . ' ', $authHeader);
    }

    public static function createBasicHeader(string $userName, string $password): string
    {
        return sprintf('%s %s', self::BASIC->value, StringHelper::base64Encode(sprintf('%s:%s', $userName, $password)));
    }
}
