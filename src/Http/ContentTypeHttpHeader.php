<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Http;

enum ContentTypeHttpHeader: string
{
    case APPLICATION_JSON = 'application/json';
    case MULTIPART = 'multipart/form-data';
    case APPLICATION_X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    case APPLICATION_PDF = 'application/pdf';

    public static function name(): string
    {
        return 'Content-Type';
    }
}
