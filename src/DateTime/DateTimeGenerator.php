<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Hgraca\PhpExtension\AbstractStaticClass;

/**
 * The DateTimeGenerator is useful because it makes DateTime objects predictable and therefore testable.
 * Using DateTimeImmutable, provides for immutability, which helps reduce bugs.
 */
final class DateTimeGenerator extends AbstractStaticClass
{
    public const DATE_TIME_FORMAT = DateTimeHelper::MYSQL_DATE_TIME_FORMAT;
    public const UNIX_EPOCH_DATE_TIME = DateTimeHelper::UNIX_EPOCH_DATE_TIME;

    /** @var ?callable */
    private static $customGenerator;

    /**
     * @throws DateTimeException
     */
    public static function generate(string $time = null, DateTimeZone $timezone = null): DateTimeImmutable
    {
        $timezone = $timezone ?: DateTimeHelper::getDefaultDateTimeZone();
        $customGenerator = self::$customGenerator;

        try {
            return $customGenerator
                ? $customGenerator($time, $timezone)
                : self::defaultGenerator($time, $timezone);
        } catch (Exception $e) {
            throw new DateTimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public static function overrideDefaultGenerator(callable $customGenerator): void
    {
        self::$customGenerator = $customGenerator;
    }

    public static function reset(): void
    {
        self::$customGenerator = null;
    }

    /**
     * @throws Exception
     */
    private static function defaultGenerator(?string $time = 'now', DateTimeZone $timezone = null): DateTimeImmutable
    {
        return new DateTimeImmutable($time ?? 'now', $timezone);
    }

    public static function generateAsString(
        string $format = self::DATE_TIME_FORMAT,
        string $time = 'now',
        DateTimeZone $timezone = null
    ): string {
        return self::generate($time, $timezone)->format($format);
    }

    public static function generateRandom(
        DateTimeImmutable $from = null,
        DateTimeImmutable $to = null
    ): DateTimeImmutable {
        if ($from === null || $to === null) {
            return self::generate();
        }

        $seconds = $to->getTimestamp() - $from->getTimestamp();
        $rnd = random_int(0, $seconds);

        return $from->add(new DateInterval('PT' . $rnd . 'S'));
    }
}
