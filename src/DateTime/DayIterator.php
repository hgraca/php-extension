<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use DateInterval;
use DateTimeImmutable;
use Iterator;

/**
 * @implements Iterator<int, DateTimeImmutable>
 */
final class DayIterator implements Iterator
{
    private readonly DateTimeRange $dateTimeInterval;

    private DateTimeImmutable $currentDay;

    public function __construct(DateTimeRange $dateTimeInterval)
    {
        $this->dateTimeInterval = $dateTimeInterval->discardTimes();
        $this->rewind();
    }

    public function current(): DateTimeImmutable
    {
        return $this->currentDay;
    }

    public function next(): void
    {
        $this->currentDay = $this->getNext();
    }

    public function key(): int
    {
        return (new DateTimeRange($this->dateTimeInterval->getFrom(), $this->currentDay))->getDaysDifference();
    }

    public function valid(): bool
    {
        $key = $this->key();

        return 0 <= $key && $key <= $this->dateTimeInterval->getDaysDifference()
            && $this->dateTimeInterval->contains($this->currentDay);
    }

    public function rewind(): void
    {
        $this->currentDay = $this->dateTimeInterval->getFrom();
    }

    private function getNext(): DateTimeImmutable
    {
        return $this->dateTimeInterval->isGoingForward()
            ? $this->currentDay->add(new DateInterval('P1D'))
            : $this->currentDay->sub(new DateInterval('P1D'));
    }
}
