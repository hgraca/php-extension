<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Hgraca\PhpExtension\AbstractStaticClass;
use Hgraca\PhpExtension\DateTime\Exception\DateTimeArithmeticException;
use Hgraca\PhpExtension\DateTime\Exception\DateTimeConversionException;

final class DateTimeHelper extends AbstractStaticClass
{
    public const TIMEZONE_EU_AMSTERDAM = 'Europe/Amsterdam';
    public const TIMEZONE_UTC = 'UTC';
    public const MYSQL_DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    public const DATE_FORMAT = 'Y-m-d';
    public const DATE_FORMAT_EU_NO_SEPARATORS = 'dmY';
    public const TIME_FORMAT = 'H:i:s';
    public const UNIX_EPOCH_DATE_TIME = '1970-01-01 00:00:00';
    public const ISO_8601 = DATE_ATOM;

    public static function fromDateTimeToString(
        DateTimeInterface $dateTime,
        ?string $format = DateTimeInterface::ATOM
    ): string {
        $format = $format ?: DateTimeInterface::ATOM;

        if ($dateTime instanceof DateTime) {
            $dateTime = clone $dateTime;
        }

        return $dateTime->setTimezone(self::getDefaultDateTimeZone())->format($format);
    }

    public static function fromStringToDateTimeImmutable(
        string $dateTimeString,
        ?string $format = DateTimeInterface::ATOM
    ): DateTimeImmutable {
        $format = $format ?: DateTimeInterface::ATOM;
        $dateTime = DateTimeImmutable::createFromFormat(
            $format,
            $dateTimeString,
            self::getDefaultDateTimeZone()
        );

        if ($dateTime === false) {
            throw new DateTimeConversionException(
                "Failed to convert '{$dateTimeString}' to DateTime using the format '{$format}'."
            );
        }

        return $dateTime;
    }

    public static function getDefaultDateTimeZone(): DateTimeZone
    {
        static $dateTimeZone;

        if (!$dateTimeZone instanceof DateTimeZone) {
            $dateTimeZone = new DateTimeZone(self::TIMEZONE_EU_AMSTERDAM);
        }

        return $dateTimeZone;
    }

    /**
     * @param list<DateTimeInterface> $availableDates
     *
     * @return list<DateTimeInterface[]>
     */
    public static function groupConsecutiveDatesTogether(array $availableDates): array
    {
        $dateRanges = [];
        $rangeCount = 0;
        $previousDate = null;
        $availableDate = null;

        if (count($availableDates) === 0) {
            return $dateRanges;
        }

        $sortedAvailableDates = self::sortAscending(...$availableDates);

        /** @var DateTimeInterface[] $sortedAvailableDates */
        foreach ($sortedAvailableDates as $availableDate) {
            if ($previousDate === null) {
                $dateRanges[$rangeCount]['begin'] = $previousDate = $availableDate;
                continue;
            }

            if (
                $previousDate->format('Y-m-d') === date('Y-m-d', strtotime('-1 day', $availableDate->getTimestamp()))
            ) {
                $previousDate = $availableDate;
                continue;
            }

            $dateRanges[$rangeCount]['end'] = $previousDate;
            ++$rangeCount;
            $dateRanges[$rangeCount]['begin'] = $previousDate = $availableDate;
        }

        $dateRanges[$rangeCount]['end'] = $availableDate;

        /** @var list<array{begin: DateTimeImmutable, end: DateTimeImmutable}> */
        return $dateRanges;
    }

    /**
     * @param list<string> $voucherAvailableOnDates
     *
     * @return array<int, DateTimeImmutable|false>
     */
    public static function uniqueDates(array $voucherAvailableOnDates): array
    {
        return array_unique(
            array_map(
                fn ($date) => DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $date),
                $voucherAvailableOnDates
            ),
            SORT_REGULAR
        );
    }

    public static function sortAscending(DateTimeInterface ...$datetimeList): array
    {
        usort($datetimeList, fn (DateTimeInterface $a, DateTimeInterface $b) => $a <=> $b);

        return $datetimeList;
    }

    public static function resetSeconds(DateTimeImmutable $datetime): DateTimeImmutable
    {
        return $datetime->setTime((int) $datetime->format('H'), (int) $datetime->format('i'));
    }

    public static function resetMilliseconds(DateTimeImmutable $datetime): DateTimeImmutable
    {
        return $datetime->setTime(
            (int) $datetime->format('H'),
            (int) $datetime->format('i'),
            (int) $datetime->format('s')
        );
    }

    public static function countDays(DateTimeImmutable $firstDateTime, DateTimeImmutable $secondDateTime): int
    {
        if ($firstDateTime === $secondDateTime) {
            return 0;
        } elseif ($firstDateTime < $secondDateTime) {
            $earliestDateTime = $firstDateTime;
            $latestDateTime = $secondDateTime;
        } else {
            $earliestDateTime = $secondDateTime;
            $latestDateTime = $firstDateTime;
        }

        $earliestTimestamp = $earliestDateTime->setTime(0, 0)->getTimestamp();
        $latestTimestamp = $latestDateTime->setTime(0, 0)->add(new DateInterval('P1D'))->getTimestamp();

        return (int) (($latestTimestamp - $earliestTimestamp) / (60 * 60 * 24));
    }

    /**
     * @return WeekdayEnum[]
     */
    public static function mapDateRangeToWeekDays(DateTimeImmutable $from, DateTimeImmutable $until): array
    {
        $interval = DateInterval::createFromDateString('1 day');
        $untilPlusOneDay = $until->modify('+1 day');
        if ($untilPlusOneDay === false) {
            throw new DateTimeArithmeticException('Failed to add a day to ' . self::fromDateTimeToString($until) . '.');
        }
        $period = new DatePeriod($from, $interval, $untilPlusOneDay);
        $dateTimesWithWeekday = [];
        /** @var DateTimeImmutable $item */
        foreach ($period as $item) {
            $dateTimesWithWeekday[
                $item->format(DateTimeHelper::DATE_FORMAT)
            ] = WeekdayEnum::hydrate(strtoupper($item->format('l')));
        }

        return $dateTimesWithWeekday;
    }

    public static function resetTime(DateTimeImmutable $datetime): DateTimeImmutable
    {
        return $datetime->setTime(0, 0);
    }

    public static function hasMoreDays(DateTimeImmutable $startDate, DateTimeImmutable $endDate): bool
    {
        $startDate = DateTimeHelper::resetTime($startDate);
        $endDate = DateTimeHelper::resetTime($endDate);

        return $startDate != $endDate;
    }

    public static function getNextDay(DateTimeImmutable $startDate, DateTimeImmutable $endDate): DateTimeImmutable
    {
        $startDate = DateTimeHelper::resetTime($startDate);
        $endDate = DateTimeHelper::resetTime($endDate);

        if (DateTimeHelper::isGoingForwardInTime($startDate, $endDate)) {
            $nextDay = $startDate->add(new DateInterval('P1D'));
        } else {
            $nextDay = $startDate->sub(new DateInterval('P1D'));
        }

        return $nextDay;
    }

    public static function isGoingForwardInTime(DateTimeImmutable $startDate, DateTimeImmutable $endDate): bool
    {
        return $startDate < $endDate;
    }
}
