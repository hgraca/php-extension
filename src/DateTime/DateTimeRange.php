<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use DateTimeImmutable;

final readonly class DateTimeRange
{
    public function __construct(private DateTimeImmutable $from, private DateTimeImmutable $until)
    {
    }

    public function isGoingForward(): bool
    {
        return $this->from < $this->until;
    }

    public function getFrom(): DateTimeImmutable
    {
        return $this->from;
    }

    public function getUntil(): DateTimeImmutable
    {
        return $this->until;
    }

    public function getDaysDifference(): int
    {
        return (int) $this->until->diff($this->from)->format('%a');
    }

    public function contains(DateTimeImmutable $dateTime): bool
    {
        return $this->isGoingForward()
            ? $this->from <= $dateTime && $dateTime <= $this->until
            : $this->until <= $dateTime && $dateTime <= $this->from;
    }

    public function discardTimes(): self
    {
        return new self($this->from->setTime(0, 0), $this->until->setTime(0, 0));
    }
}
