<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use DateTimeImmutable;

final readonly class TimeInterval
{
    private function __construct(private int $miliseconds)
    {
    }

    public static function fromSeconds(int $seconds): self
    {
        return new self($seconds * 1000);
    }

    public static function fromMinutes(int $minutes): self
    {
        return new self($minutes * 60 * 1000);
    }

    public static function fromHours(int $hours): self
    {
        return new self($hours * 60 * 60 * 1000);
    }

    public static function fromDays(int $days): self
    {
        return new self($days * 24 * 60 * 60 * 1000);
    }

    public static function until(DateTimeImmutable $dateTime): self
    {
        return new self(($dateTime->getTimestamp() - time()) * 1000);
    }

    public function getMiliseconds(): int
    {
        return $this->miliseconds;
    }

    public function getSeconds(): float|int
    {
        return $this->miliseconds / 1000;
    }

    public function getMinutes(): float|int
    {
        return $this->miliseconds / 60 / 1000;
    }

    public function getHours(): float|int
    {
        return $this->miliseconds / 60 / 60 / 1000;
    }

    public function getDays(): float|int
    {
        return $this->miliseconds / 24 / 60 / 60 / 1000;
    }
}
