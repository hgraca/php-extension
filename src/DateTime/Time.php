<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use InvalidArgumentException;

final readonly class Time
{
    private const INVALID_TIME_MESSAGE = 'The provided time is invalid';

    public function __construct(private int $hours, private int $minutes, private int $seconds = 0)
    {
        $this->validate();
    }

    public static function parse(string $time): self
    {
        $timePieces = explode(':', $time);

        return new self(
            isset($timePieces[0]) ? (int) $timePieces[0] : 0,
            isset($timePieces[1]) ? (int) $timePieces[1] : 0,
            isset($timePieces[2]) ? (int) $timePieces[2] : 0
        );
    }

    public function getHours(): int
    {
        return $this->hours;
    }

    public function getMinutes(): int
    {
        return $this->minutes;
    }

    public function getSeconds(): int
    {
        return $this->seconds;
    }

    public function toString(): string
    {
        return sprintf('%02d:%02d:%02d', $this->hours, $this->minutes, $this->seconds);
    }

    private function validate(): void
    {
        $this->validateHours();
        $this->validateMinutes();
        $this->validateSeconds();
    }

    private function validateHours(): void
    {
        if ($this->hours >= 24 || $this->hours < 0) {
            $this->throwInvalidArgumentException();
        }
    }

    private function validateMinutes(): void
    {
        if ($this->minutes >= 60 || $this->minutes < 0) {
            $this->throwInvalidArgumentException();
        }
    }

    private function validateSeconds(): void
    {
        if ($this->seconds >= 60 || $this->seconds < 0) {
            $this->throwInvalidArgumentException();
        }
    }

    private function throwInvalidArgumentException(): never
    {
        throw new InvalidArgumentException(self::INVALID_TIME_MESSAGE);
    }

    public function isAfter(Time $until): bool
    {
        return $this->toString() > $until->toString();
    }
}
