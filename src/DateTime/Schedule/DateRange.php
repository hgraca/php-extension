<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime\Schedule;

use DateTimeImmutable;
use InvalidArgumentException;

final class DateRange
{
    private readonly DateTimeImmutable $from;

    private readonly DateTimeImmutable $until;

    public function __construct(DateTimeImmutable $from, DateTimeImmutable $until)
    {
        if ($until < $from) {
            throw new InvalidArgumentException('Invalid date range');
        }

        $this->from = $from;
        $this->until = $until;
    }

    public function getFrom(): DateTimeImmutable
    {
        return $this->from;
    }

    public function getUntil(): DateTimeImmutable
    {
        return $this->until;
    }
}
