<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime\Schedule;

use Hgraca\PhpExtension\DateTime\Time;
use InvalidArgumentException;

final class TimeRange
{
    private readonly \Hgraca\PhpExtension\DateTime\Time $from;

    private readonly \Hgraca\PhpExtension\DateTime\Time $until;

    public function __construct(Time $from, Time $until)
    {
        if ($from->isAfter($until)) {
            throw new InvalidArgumentException('Invalid time range');
        }

        $this->from = $from;
        $this->until = $until;
    }

    public function getFrom(): Time
    {
        return $this->from;
    }

    public function getUntil(): Time
    {
        return $this->until;
    }
}
