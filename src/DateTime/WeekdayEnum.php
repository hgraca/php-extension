<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\DateTime;

use Hgraca\PhpExtension\Enum\AbstractEnum;

/**
 * @method static WeekdayEnum monday()
 * @method bool isMonday()
 * @method static WeekdayEnum tuesday()
 * @method bool isTuesday()
 * @method static WeekdayEnum wednesday()
 * @method bool isWednesday()
 * @method static WeekdayEnum thursday()
 * @method bool isThursday()
 * @method static WeekdayEnum friday()
 * @method bool isFriday()
 * @method static WeekdayEnum saturday()
 * @method bool isSaturday()
 * @method static WeekdayEnum sunday()
 * @method bool isSunday()
 */
final class WeekdayEnum extends AbstractEnum
{
    private const MONDAY = 'MONDAY';
    private const TUESDAY = 'TUESDAY';
    private const WEDNESDAY = 'WEDNESDAY';
    private const THURSDAY = 'THURSDAY';
    private const FRIDAY = 'FRIDAY';
    private const SATURDAY = 'SATURDAY';
    private const SUNDAY = 'SUNDAY';
}
