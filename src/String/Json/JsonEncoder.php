<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\String\Json;

use Hgraca\PhpExtension\AbstractStaticClass;
use Hgraca\PhpExtension\Exception\RuntimeException;
use JsonException;

final class JsonEncoder extends AbstractStaticClass
{
    private int $options = 0;

    /** @var int<1, 2147483647> */
    private int $depth = 512;

    public static function construct(): self
    {
        return new self();
    }

    /**
     * All < and > are converted to \u003C and \u003E.
     * Available since PHP 5.3.0.
     */
    public function hexTag(): self
    {
        $this->options |= JSON_HEX_TAG;

        return $this;
    }

    /**
     * All &s are converted to \u0026.
     * Available since PHP 5.3.0.
     */
    public function hexAmp(): self
    {
        $this->options |= JSON_HEX_AMP;

        return $this;
    }

    /**
     * All ' are converted to \u0027.
     * Available since PHP 5.3.0.
     */
    public function hexApos(): self
    {
        $this->options |= JSON_HEX_APOS;

        return $this;
    }

    /**
     * All " are converted to \u0022.
     * Available since PHP 5.3.0.
     */
    public function hexQuot(): self
    {
        $this->options |= JSON_HEX_QUOT;

        return $this;
    }

    /**
     * Outputs an object rather than an array when a non-associative array is used.
     * Especially useful when the recipient of the output is expecting an object and the array is empty.
     * Available since PHP 5.3.0.
     */
    public function forceObject(): self
    {
        $this->options |= JSON_FORCE_OBJECT;

        return $this;
    }

    /**
     * Encodes numeric strings as numbers.
     * Available since PHP 5.3.3.
     */
    public function numericCheck(): self
    {
        $this->options |= JSON_NUMERIC_CHECK;

        return $this;
    }

    /**
     * Use whitespace in returned data to format it.
     * Available since PHP 5.4.0.
     */
    public function prettyPrint(): self
    {
        $this->options |= JSON_PRETTY_PRINT;

        return $this;
    }

    /**
     * Don't escape /.
     * Available since PHP 5.4.0.
     */
    public function unescapedSlashes(): self
    {
        $this->options |= JSON_UNESCAPED_SLASHES;

        return $this;
    }

    /**
     * Encode multibyte Unicode characters literally (default is to escape as \uXXXX).
     * Available since PHP 5.4.0.
     */
    public function unescapedUnicode(): self
    {
        $this->options |= JSON_UNESCAPED_UNICODE;

        return $this;
    }

    /**
     * Substitute some unencodable values instead of failing.
     * Available since PHP 5.5.0.
     */
    public function partialOutputOnError(): self
    {
        $this->options |= JSON_PARTIAL_OUTPUT_ON_ERROR;

        return $this;
    }

    /**
     * Ensures that float values are always encoded as a float value.
     * Available since PHP 5.6.6.
     */
    public function preserveZeroFraction(): self
    {
        $this->options |= JSON_PRESERVE_ZERO_FRACTION;

        return $this;
    }

    /**
     * The line terminators are kept unescaped when JSON_UNESCAPED_UNICODE is supplied. It uses the same behaviour
     * as it was before PHP 7.1 without this constant.
     * Available since PHP 7.1.0.
     */
    public function unescapedLineTerminators(): self
    {
        $this->options |= JSON_UNESCAPED_LINE_TERMINATORS;

        return $this;
    }

    /**
     * @param int<1, 2147483647> $depth
     *
     * @return $this
     */
    public function depth(int $depth = 512): self
    {
        /**
         * @phpstan-ignore-next-line
         *
         * @psalm-suppress DocblockTypeContradiction
         */
        if ($depth < 1) {
            throw new RuntimeException('Json encoding depth must be above 0.');
        }
        $this->depth = $depth;

        return $this;
    }

    /**
     * @throws JsonException
     */
    public function encode(mixed $value): string
    {
        return json_encode($value, JSON_THROW_ON_ERROR | $this->options, $this->depth);
    }
}
