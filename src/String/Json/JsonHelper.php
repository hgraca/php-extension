<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\String\Json;

use Hgraca\PhpExtension\String\StringHelper;

final class JsonHelper
{
    /**
     * @param int<1, 2147483647> $depth
     */
    public static function encode(mixed $data, int $flags = 0, int $depth = 512): string
    {
        /*
         * We add this flag, to ensure that when encoding data we do
         * not use utf8 encoding but keep to latin1.
         * When we move to GCP we can remove adding this flag by default.
         */
        $flags |= JSON_UNESCAPED_UNICODE;

        $encoded = json_encode($data, $flags, $depth);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                /** @var string $encoded */
                return $encoded;
            case JSON_ERROR_RECURSION:
                throw new JsonEncodingException('One or more recursive references in the value to be encoded');
            case JSON_ERROR_INF_OR_NAN:
                throw new JsonEncodingException('One or more NAN or INF values in the value to be encoded');
            case JSON_ERROR_UNSUPPORTED_TYPE:
                throw new JsonEncodingException('A value of a type that cannot be encoded was given');
            case JSON_ERROR_INVALID_PROPERTY_NAME:
                throw new JsonEncodingException('A property name that cannot be encoded was given');
            case JSON_ERROR_UTF16:
                throw new JsonEncodingException('Malformed UTF-16 characters, possibly incorrectly encoded');
            default:
                throw new JsonEncodingException('Unknown error');
        }
    }

    /**
     * @throws JsonDecodingException
     *
     * @return mixed
     */
    public static function decode(string $json, bool $assoc = true, int $options = 0)
    {
        /** @psalm-suppress MixedAssignment */
        $decoded = json_decode($json, $assoc, 512, $options);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $decoded;
            case JSON_ERROR_DEPTH:
                throw new JsonDecodingException("Maximum stack depth exceeded:\n'{$json}'");
            case JSON_ERROR_STATE_MISMATCH:
                throw new JsonDecodingException("Underflow or the modes mismatch:\n'{$json}'");
            case JSON_ERROR_CTRL_CHAR:
                throw new JsonDecodingException("Unexpected control character found:\n'{$json}'");
            case JSON_ERROR_SYNTAX:
                throw new JsonDecodingException("Syntax error, malformed JSON:\n'{$json}'");
            case JSON_ERROR_UTF8:
                throw new JsonDecodingException("Malformed UTF-8 characters, possibly incorrectly encoded:\n'{$json}'");
            default:
                throw new JsonDecodingException("Unknown error:\n'{$json}'");
        }
    }

    public static function isJson(string $string): bool
    {
        if (!StringHelper::hasBeginning('[', $string) && !StringHelper::hasBeginning('{', $string)) {
            return false;
        }

        /** @psalm-suppress UnusedFunctionCall */
        json_decode($string);

        return json_last_error() === JSON_ERROR_NONE;
    }

    public static function extract(string $json, string $path): string
    {
        /** @var mixed $newJson */
        $newJson = JsonHelper::decode($json);
        $pathParts = array_reduce(
            explode('.', $path),
            /**
             * @param string[] $carry
             *
             * @return string[]
             */
            function (array $carry, string $path): array {
                if (empty($path)) {
                    return $carry;
                }

                if (str_contains($path, '[')) {
                    preg_match('/(?<key>.*)\[(?<index>[^\]]*)\]/', $path, $matches);
                    $carry[] = $matches['key'];
                    $carry[] = $matches['index'];

                    return $carry;
                }

                $carry[] = $path;

                return $carry;
            },
            []
        );

        foreach ($pathParts as $pathPart) {
            /** @var mixed $newJson */
            $newJson = JsonHelper::extractKeyFromArray($newJson, $pathPart);
        }

        return JsonHelper::encode($newJson);
    }

    /**
     * @return mixed
     */
    private static function extractKeyFromArray(mixed $data, string|int $key)
    {
        if (!is_array($data) || !array_key_exists($key, $data)) {
            throw new JsonDoesNotContainPathException();
        }

        return $data[$key];
    }
}
