<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\String;

use Hgraca\PhpExtension\AbstractStaticClass;

use function array_filter;
use function array_map;
use function preg_match_all;
use function preg_quote;

use const PREG_SET_ORDER;

final class StringHelper extends AbstractStaticClass
{
    private const ALLOWED_RANDOM_CHARS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * Returns true if this string contains with the given needle.
     * If the needle is empty, it always returns true.
     */
    public static function contains(string $needle, string $haystack): bool
    {
        return $needle === '' || mb_strpos($haystack, $needle) !== false;
    }

    public static function toSlug(string $string): string
    {
        return (string) preg_replace('/\s+/', '-', mb_strtolower(trim(strip_tags($string)), 'UTF-8'));
    }

    public static function toStudlyCase(string $sentence): string
    {
        return self::removeAllSpaces(
            self::makeAllWordsUpperCaseFirst(
                self::makeLowCase(
                    self::separateCapitalizedWordsWithSpace(
                        self::separateWordsWithSpace($sentence)
                    )
                )
            )
        );
    }

    public static function toCamelCase(string $sentence): string
    {
        return lcfirst(self::toStudlyCase($sentence));
    }

    public static function toSnakeCase(string $sentence): string
    {
        $sentence = static::toStudlyCase($sentence);

        $replace = '$1_$2';

        return ctype_lower($sentence)
            ? $sentence
            : mb_strtolower((string) preg_replace('/(.)([A-Z])/', $replace, $sentence));
    }

    /**
     * '/(?<!\ )[A-Z]/' Separates all
     * '/(?<! )(?<!^)[A-Z]/' Separates all except if the first char in $sentence is caps
     * '/(?<! )(?<!^)(?<![A-Z])[A-Z]/' Separates all except the first and sequential (ie. ID)
     *
     * @param string[] $wordSeparatorList
     */
    public static function separateWordsWithSpace(
        string $sentence,
        array $wordSeparatorList = ['-', '_', '.', ' ']
    ): string {
        return str_replace($wordSeparatorList, ' ', $sentence);
    }

    /**
     * @param string[] $wordSeparatorList
     */
    public static function makeAllWordsUpperCaseFirst(
        string $sentence,
        array $wordSeparatorList = ['-', '_', '.', ' ']
    ): string {
        return ucwords($sentence, implode('', $wordSeparatorList));
    }

    public static function makeLowCase(string $sentence): string
    {
        return mb_strtolower($sentence);
    }

    public static function removeAllSpaces(string $sentence): string
    {
        return str_replace([' '], '', $sentence);
    }

    public static function separateCapitalizedWordsWithSpace(string $sentence): string
    {
        return (string) preg_replace('/([a-z])([A-Z])/', '$1 $2', $sentence);
    }

    public static function hasBeginning(string $needle, string $haystack): bool
    {
        return $needle === '' || mb_strpos($haystack, $needle) === 0;
    }

    public static function hasEnding(string $needle, string $haystack): bool
    {
        return $needle === '' || mb_substr($haystack, -mb_strlen($needle)) === $needle;
    }

    public static function isJson(string $string): bool
    {
        json_decode($string);

        return json_last_error() === JSON_ERROR_NONE;
    }

    public static function remove(string $search, string $haystack): string
    {
        return static::replace($search, '', $haystack);
    }

    public static function removeFromBeginning(string $search, string $haystack): string
    {
        return $search === ''
            ? $haystack
            : static::replaceFromBeginning($search, '', $haystack);
    }

    public static function removeFromEnd(string $search, string $haystack): string
    {
        return static::replaceFromEnd($search, '', $haystack);
    }

    public static function replace(string $search, string $replacement, string $haystack): string
    {
        return str_replace($search, $replacement, $haystack);
    }

    public static function replaceFromBeginning(string $search, string $replacement, string $haystack): string
    {
        if ($search === '' || !self::hasBeginning($search, $haystack)) {
            return $haystack;
        }

        $search = '/' . preg_quote($search, '/') . '/';

        return (string) preg_replace($search, $replacement, $haystack, 1);
    }

    public static function replaceFromEnd(string $search, string $replacement, string $haystack): string
    {
        return strrev(self::replaceFromBeginning(strrev($search), strrev($replacement), strrev($haystack)));
    }

    /**
     * @return string[]
     */
    public static function extractFromBetween(string $before, string $after, string $subject): array
    {
        if ($before === '' || $after === '' || $subject === '') {
            return [];
        }

        $delimiter = '/';
        $before = preg_quote($before, $delimiter);
        $after = preg_quote($after, $delimiter);

        if (preg_match_all("$delimiter$before(.*?)$after$delimiter", $subject, $matches, PREG_SET_ORDER) !== false) {
            return array_filter(
                array_map(
                    fn ($match) => $match[1],
                    $matches
                )
            );
        }

        return [];
    }

    public static function containsOnly(string $needle, string $allowedCharacters): bool
    {
        $allowedCharactersList = array_unique(str_split($allowedCharacters));
        $charsInString = array_unique(str_split($needle));

        return count(array_diff($charsInString, $allowedCharactersList)) === 0;
    }

    public static function toKebabCase(string $sentence): string
    {
        return strtr(self::toSnakeCase($sentence), ['_' => '-']);
    }

    public static function toConstantCase(string $sentence): string
    {
        return strtoupper(self::toSnakeCase($sentence));
    }

    public static function getRandomString(int $length = 5, string $characterSet = self::ALLOWED_RANDOM_CHARS): string
    {
        $randomString = '';

        for ($i = 0; $i < $length; ++$i) {
            $index = random_int(0, strlen($characterSet) - 1);
            $randomString .= $characterSet[$index];
        }

        return $randomString;
    }

    public static function shuffle(string $input): string
    {
        $splitStr = str_split($input);
        $shuffledString = '';

        do {
            $index = random_int(0, count($splitStr) - 1);
            $shuffledString .= $splitStr[$index];
            array_splice($splitStr, $index, 1);
        } while (strlen($shuffledString) < strlen($input));

        return $shuffledString;
    }

    public static function base64UrlEncode(string $input): string
    {
        return strtr(base64_encode($input), '+/=', '._-');
    }

    public static function base64UrlDecode(string $input): string
    {
        return base64_decode(strtr($input, '._-', '+/='));
    }

    public static function base64Encode(string $string): string
    {
        return base64_encode($string);
    }

    public static function base64Decode(string $string): string
    {
        return base64_decode($string);
    }

    public static function ensureStartsWith(string $needle, string $haystack): string
    {
        return $needle . self::leftStringReplace($needle, $haystack);
    }

    public static function ensureEndsWith(string $needle, string $haystack): string
    {
        return self::rightStringReplace($needle, $haystack) . $needle;
    }

    public static function ensureWrappedWithString(string $leftEdge, string $rightEdge, string $haystack): string
    {
        return self::ensureStartsWith(
            $leftEdge,
            self::ensureEndsWith($rightEdge, $haystack)
        );
    }

    public static function stringReplace(string $needle, string $haystack, string $replacement = ''): string
    {
        $needle = preg_quote($needle, '/');

        /** @var string */
        return preg_replace("/$needle/s", $replacement, $haystack);
    }

    public static function leftStringReplace(string $trim, string $haystack, string $replacement = ''): string
    {
        $trim = preg_quote($trim, '/');

        /** @var string */
        return preg_replace("/^$trim/s", $replacement, $haystack);
    }

    public static function rightStringReplace(string $trim, string $haystack, string $replacement = ''): string
    {
        $trim = preg_quote($trim, '/');

        /** @var string */
        return preg_replace("/$trim$/s", $replacement, $haystack);
    }

    public static function toHumanReadable(string $input): string
    {
        return ucfirst(
            self::makeLowCase(
                ltrim(
                    preg_replace(
                        '/(?<!\ )[A-Z]/',
                        ' $0',
                        self::separateWordsWithSpace($input)
                    )
                )
            )
        );
    }

    public static function generateUserFriendlyId(): string
    {
        /** @var array{r1: int, r2: int, r3: int, r: int} */
        $randoms = unpack('nr1/nr2/nr3/nr', random_bytes(10));
        $randoms['r1'] |= ($randoms['r'] <<= 4) & 0xF0000;
        $randoms['r2'] |= ($randoms['r'] <<= 4) & 0xF0000;
        $randoms['r3'] |= ($randoms['r'] <<= 4) & 0xF0000;

        return strtr(
            sprintf(
                '%03s-%03s-%04s',
                substr((string) $randoms['r1'], 0, 3),
                substr((string) $randoms['r2'], 0, 3),
                substr(base_convert((string) $randoms['r3'], 10, 32), 0, 4)
            ),
            'abcdefghijklmnopqrstuv',
            'ABCDEFGHJKMNPQRSTVWXYZ'
        );
    }

    public static function isGlobMatch(string $pattern, string $filename): bool
    {
        return fnmatch($pattern, $filename);
    }
}
