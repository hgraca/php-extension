<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Enum;

use Hgraca\PhpExtension\CodeUnit\TypeHelper;
use Hgraca\PhpExtension\Exception\LogicException;
use Throwable;

final class FailedToCreateEnumSingletonRegistryKeyException extends LogicException
{
    /**
     * @param mixed $enumValue
     */
    public function __construct(string $enumFqcn, $enumValue, Throwable $previous)
    {
        $enumValueType = TypeHelper::getType($enumValue);
        $enumValueString = TypeHelper::forceToString($enumValue);

        parent::__construct(
            "Could enum registry key with FQNC '$enumFqcn', value type '$enumValueType' and value '$enumValueString'.",
            0,
            $previous
        );
    }
}
