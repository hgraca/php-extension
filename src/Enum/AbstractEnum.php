<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Enum;

use BadMethodCallException;
use Hgraca\PhpExtension\CodeUnit\ClassHelper;
use Hgraca\PhpExtension\Exception\RuntimeException;
use Hgraca\PhpExtension\String\StringHelper;
use InvalidArgumentException;
use JsonSerializable;
use ReflectionClass;
use ReflectionException;
use Stringable;

use function array_search;
use function array_values;
use function in_array;

abstract class AbstractEnum implements JsonSerializable, Stringable
{
    private static ?\Hgraca\PhpExtension\Enum\EnumSingletonRegistry $registry = null;

    /** @var mixed */
    protected $value;

    final protected function __construct(mixed $value)
    {
        /** @var bool $value */
        if (!$this->isValid($value)) {
            $message = 'Value [%s] is not matching any valid value of class "%s". Valid values are [%s].';

            throw new InvalidArgumentException(sprintf(
                $message,
                (string) $value,
                $this->getClassName(),
                self::getValidOptionsAsString()
            ));
        }

        $this->value = $value;
    }

    public function jsonSerialize(): mixed
    {
        return $this->value;
    }

    /**
     * @return static
     */
    public static function hydrate(mixed $value): self
    {
        return static::get($value);
    }

    /**
     * @return static
     */
    public static function get(mixed $value): self
    {
        if (self::$registry === null) {
            self::$registry = new EnumSingletonRegistry();
        }

        if (!self::$registry->contains(static::class, $value)) {
            self::$registry->add(new static($value));
        }

        /** @var static */
        return self::$registry->get(static::class, $value);
    }

    /**
     * @param array<mixed> $arguments
     *
     * @return static
     */
    public static function __callStatic(string $methodName, array $arguments): self
    {
        foreach (self::getConstants() as $option => $value) {
            $expectedMethodName = StringHelper::toCamelCase($option);
            if ($expectedMethodName === $methodName) {
                return static::get($value);
            }
        }

        throw new BadMethodCallException(sprintf('%s::%s() does not exist', static::class, $methodName));
    }

    /**
     * @param array<mixed> $arguments
     *
     * @return bool
     */
    public function __call(string $methodName, array $arguments)
    {
        foreach (self::getConstants() as $option => $value) {
            $isaMethodName = 'is' . StringHelper::toStudlyCase($option);
            if ($isaMethodName === $methodName) {
                return $this->equals(static::get($value));
            }
        }

        throw new BadMethodCallException(sprintf('%s::%s() does not exist', static::class, $methodName));
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getKey(): string
    {
        /** @var string */
        return array_search($this->value, self::getConstants(), true);
    }

    public function equals(self $other): bool
    {
        return $other::class === static::class && $other->value === $this->value;
    }

    public static function getValidOptions(): array
    {
        return array_values(self::getConstants());
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    /**
     * @param bool $value
     */
    protected function isValid($value): bool
    {
        return in_array($value, self::getValidOptions(), true);
    }

    protected function getClassName(): string
    {
        return ClassHelper::extractCanonicalClassName(static::class);
    }

    /**
     * @return array<mixed>
     */
    private static function getConstants(): array
    {
        try {
            return (new ReflectionClass(static::class))->getConstants();
        } catch (ReflectionException $e) {
            throw new RuntimeException(
                'Error getting the constants of the Enum: ' . static::class,
                (int) $e->getCode(),
                $e
            );
        }
    }

    private static function getValidOptionsAsString(): string
    {
        return implode(
            ', ',
            array_map(fn ($option) => var_export($option, true), self::getValidOptions())
        );
    }
}
