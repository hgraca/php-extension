<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Enum;

use Hgraca\PhpExtension\CodeUnit\TypeHelper;
use Throwable;

/**
 * @internal AbstractEnum
 */
final class EnumSingletonRegistry
{
    /** @var array<string, AbstractEnum> */
    private array $registry = [];

    public function add(AbstractEnum $enum): void
    {
        $enumFqcn = $enum::class;
        /** @psalm-suppress MixedAssignment */
        $enumValue = $enum->getValue();

        if ($this->contains($enumFqcn, $enumValue)) {
            throw new EnumAlreadyExistsInRegistryException($enumFqcn, $enumValue);
        }

        $this->registry[$this->createRegistryKey($enumFqcn, $enumValue)] = $enum;
    }

    public function get(string $enumFqcn, mixed $enumValue): AbstractEnum
    {
        if (!$this->contains($enumFqcn, $enumValue)) {
            throw new EnumNotFoundInRegistryException($enumFqcn, $enumValue);
        }

        return $this->registry[$this->createRegistryKey($enumFqcn, $enumValue)];
    }

    public function contains(string $enumFqcn, mixed $enumValue): bool
    {
        return array_key_exists($this->createRegistryKey($enumFqcn, $enumValue), $this->registry);
    }

    private function createRegistryKey(string $enumFqcn, mixed $enumValue): string
    {
        try {
            $enumValueString = (string) $enumValue;
        } catch (Throwable $t) {
            throw new FailedToCreateEnumSingletonRegistryKeyException($enumFqcn, $enumValue, $t);
        }

        return $enumFqcn . '::' . TypeHelper::getType($enumValue) . '::' . $enumValueString;
    }
}
