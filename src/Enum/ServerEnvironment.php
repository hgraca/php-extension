<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Enum;

enum ServerEnvironment: string
{
    use EnumHydratorTrait;
    use EnumIntrospectionTrait;

    case LOCAL = 'local'; // dev machine bare bones, outside any runtime container (docker, vagrant, LXC, ..)
    case DEVELOPMENT = 'dev'; // dev machine inside a runtime container (docker, vagrant, LXC, ...)
    case ACCEPTANCE = 'acc'; // Testing in the CI.
    case STAGING = 'staging'; // Server for manual testing, for the PO to do QA. An instance per MR.
    case DEMO = 'demo'; // Demo server with a prod instance of the application for sales, training, etc.
    case PRODUCTION = 'prod'; // Production server.
    case TEST = 'test'; // test environment

    public static function get(string $name): self
    {
        return self::hydrateFromName($name);
    }
}
