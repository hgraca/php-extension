<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Collection;

use ArrayIterator;
use Iterator;

use function count;

/**
 * @template TKey
 * @template TValue
 *
 * @implements CollectionInterface<TKey, TValue>
 */
class Collection implements CollectionInterface
{
    /**
     * @param array<array-key, TValue> $itemList
     */
    public function __construct(protected array $itemList = [])
    {
    }

    /**
     * @return Iterator<TKey, TValue>
     */
    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->itemList);
    }

    public function count(): int
    {
        return count($this->itemList);
    }

    /**
     * @return array<array-key, TValue>
     */
    public function toArray(): array
    {
        return $this->itemList;
    }

    /**
     * @param TValue $item
     */
    public function contains($item): bool
    {
        return in_array($item, $this->itemList, true);
    }

    public function containsWhere(callable $filter): bool
    {
        try {
            $this->firstWhere($filter);

            return true;
        } catch (NoResultFoundException) {
            return false;
        }
    }

    /**
     * @return TValue
     */
    public function first()
    {
        foreach ($this->itemList as $value) {
            return $value;
        }

        throw new EmptyCollectionException();
    }

    /**
     * @return TValue
     */
    public function last()
    {
        return $this->reverse()
                    ->first();
    }

    /**
     * @return CollectionInterface<TKey, TValue>
     */
    public function reverse(): CollectionInterface
    {
        /** @phpstan-ignore-next-line */
        return new static(array_reverse($this->itemList));
    }

    /**
     * @return TValue
     */
    public function firstWhere(callable $filter)
    {
        foreach ($this->itemList as $value) {
            if ($filter($value) === true) {
                return $value;
            }
        }

        throw new NoResultFoundException();
    }

    /**
     * @return Collection<TKey, TValue>
     */
    public function values()
    {
        /** @phpstan-ignore-next-line */
        return new static(array_values($this->itemList));
    }

    /**
     * @param Closure(TValue): bool $filter
     *
     * @return Collection<TKey, TValue>
     *
     * @phpstan-ignore-next-line
     */
    public function filter(callable $filter = null)
    {
        if ($filter === null) {
            /** @phpstan-ignore-next-line */
            return new static(array_filter($this->itemList));
        }

        /** @phpstan-ignore-next-line */
        return new static(array_filter(
            $this->itemList,
            $filter
        ));
    }

    public function isEmpty(): bool
    {
        return $this->count() === 0;
    }

    /**
     * @param TValue[] $items
     */
    public function push(...$items): void
    {
        foreach ($items as $item) {
            /** @phpstan-ignore-next-line */
            $this->itemList[] = $item;
        }
    }
}
