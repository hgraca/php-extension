<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Collection;

use Countable;
use Iterator;
use IteratorAggregate;

/**
 * @template TKey
 * @template TValue
 *
 * @template-extends IteratorAggregate<TKey, TValue>
 */
interface CollectionInterface extends Countable, IteratorAggregate
{
    /**
     * @return Iterator<TKey, TValue>
     */
    public function getIterator(): Iterator;

    public function count(): int;

    /**
     * @return array<array-key, TValue>
     */
    public function toArray(): array;

    /**
     * @param TValue $item
     */
    public function contains($item): bool;

    /**
     * @return TValue
     */
    public function first();

    /**
     * @return TValue
     */
    public function last();

    /**
     * @return CollectionInterface<TKey, TValue>
     */
    public function reverse(): CollectionInterface;

    /**
     * @return TValue
     */
    public function firstWhere(callable $filter);

    /**
     * @return Collection<TKey, TValue>
     */
    public function values();

    /**
     * @param Closure(TValue): bool $filter
     *
     * @return Collection<TKey, TValue>
     *
     * @phpstan-ignore-next-line
     */
    public function filter(callable $filter = null);

    public function isEmpty(): bool;

    /**
     * @param TValue[] $items
     */
    public function push(...$items): void;
}
