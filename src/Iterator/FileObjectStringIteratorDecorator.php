<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Iterator;

use SplFileObject;

final readonly class FileObjectStringIteratorDecorator implements StringIteratorInterface
{
    public function __construct(private SplFileObject $fileObject)
    {
    }

    public function next(): void
    {
        $this->fileObject->next();
    }

    public function key(): int
    {
        return $this->fileObject->key();
    }

    public function valid(): bool
    {
        return $this->fileObject->valid();
    }

    public function rewind(): void
    {
        $this->fileObject->rewind();
    }

    public function current(): ?string
    {
        /** @var string|null */
        return $this->fileObject->current() ?: null;
    }
}
