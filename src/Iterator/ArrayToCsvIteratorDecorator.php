<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Iterator;

use Hgraca\PhpExtension\Helper\CsvHelper;
use Iterator;

final class ArrayToCsvIteratorDecorator implements StringIteratorInterface
{
    public function __construct(private readonly Iterator $arrayIterator, private bool $isHeader = true)
    {
        $this->arrayIterator->rewind();
    }

    public function current(): ?string
    {
        /** @var array|null $row */
        $row = $this->arrayIterator->current();

        if ($row === null) {
            return null;
        }

        /** @var string[] $row */
        $row = $this->isHeader ? array_keys($row) : $row;

        return CsvHelper::arrayRowToCsv($row);
    }

    public function next(): void
    {
        if ($this->isHeader) {
            $this->isHeader = false;
        } else {
            $this->arrayIterator->next();
        }
    }

    public function key(): string
    {
        if ($this->isHeader) {
            return 'header';
        }

        return (string) $this->arrayIterator->key();
    }

    public function valid(): bool
    {
        if ($this->isHeader) {
            return true;
        }

        return $this->arrayIterator->valid();
    }

    public function rewind(): void
    {
        $this->isHeader = true;
        $this->arrayIterator->rewind();
    }
}
