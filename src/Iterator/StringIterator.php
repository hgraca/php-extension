<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Iterator;

use ArrayIterator;

final class StringIterator implements StringIteratorInterface
{
    private readonly ArrayIterator $stringList;

    public function __construct(string ...$stringList)
    {
        $this->stringList = new ArrayIterator($stringList);
    }

    public function next(): void
    {
        $this->stringList->next();
    }

    public function key(): void
    {
        $this->stringList->key();
    }

    public function valid(): bool
    {
        return $this->stringList->valid();
    }

    public function rewind(): void
    {
        $this->stringList->rewind();
    }

    public function current(): ?string
    {
        /** @var string|null */
        return $this->stringList->current();
    }
}
