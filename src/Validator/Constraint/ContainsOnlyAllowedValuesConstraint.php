<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

/**
 * @extends AbstractConstraint<array<int, mixed>>
 */
final class ContainsOnlyAllowedValuesConstraint extends AbstractConstraint
{
    /**
     * @param array<array-key, mixed> $allowedValuesPool
     */
    public function __construct(private readonly array $allowedValuesPool)
    {
    }

    public function doEvaluate($value): void
    {
        if (empty(array_diff($value, $this->allowedValuesPool))) {
            return;
        }

        throw new ExpectationFailedException(
            'It contains not allowed values',
            0,
            null,
            'IT_CONTAINS_NOT_ALLOWED_VALUES'
        );
    }
}
