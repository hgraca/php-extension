<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

use DateTimeImmutable;
use Exception;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;

/**
 * @extends AbstractConstraint<string>
 */
class StringDateIsInThePastConstraint extends AbstractConstraint
{
    /**
     * @param string $value
     */
    protected function doEvaluate($value): void
    {
        $date = $this->getDateFromString($value);

        $this->assertDateIsInThePast($date);
    }

    private function getDateFromString(string $value): DateTimeImmutable
    {
        try {
            return DateTimeGenerator::generate($value);
        } catch (Exception $exception) {
            throw new ExpectationFailedException(
                'Value is not a valid date',
                0,
                $exception,
                'NOT_VALID_DATE_ERROR'
            );
        }
    }

    private function assertDateIsInThePast(DateTimeImmutable $date): void
    {
        if ($date > DateTimeGenerator::generate()) {
            throw new ExpectationFailedException(
                'Value is not a date in the past',
                0,
                null,
                'NOT_DATE_IN_THE_PAST_ERROR'
            );
        }
    }
}
