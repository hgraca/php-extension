<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

/**
 * @extends AbstractConstraint<string>
 */
final class DateIntervalConstraint extends AbstractConstraint
{
    private const DATE_TIME_FORMAT = '/^P(?!$)(\d+Y)?(\d+M)?(\d+W)?(\d+D)?(T(?=\d+[HMS])(\d+H)?(\d+M)?(\d+S)?)?$/';

    protected function doEvaluate($value): void
    {
        if (!$this->isValidDateTimeFormat($value)) {
            throw new ExpectationFailedException('The format nor representation does not follow ISO 8601 standards.');
        }
    }

    /**
     * @param string $value
     */
    public function isValidDateTimeFormat($value): bool
    {
        return preg_match(self::DATE_TIME_FORMAT, $value) === 1;
    }
}
