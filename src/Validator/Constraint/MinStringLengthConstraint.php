<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

/**
 * @extends AbstractConstraint<string>
 */
final class MinStringLengthConstraint extends AbstractConstraint
{
    public function __construct(private readonly int $minLength)
    {
    }

    /**
     * @param string $value
     */
    protected function doEvaluate($value): void
    {
        if (mb_strlen($value) < $this->minLength) {
            throw new ExpectationFailedException(
                'Min length violation',
                0,
                null,
                'MIN_LENGTH_URL_VIOLATION_ERROR'
            );
        }
    }
}
