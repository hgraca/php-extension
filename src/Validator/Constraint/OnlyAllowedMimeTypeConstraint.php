<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

use Hgraca\PhpExtension\Filesystem\FileMetadata;
use Hgraca\PhpExtension\Filesystem\MimeType;

/**
 * @extends AbstractConstraint<FileMetadata>
 */
final class OnlyAllowedMimeTypeConstraint extends AbstractConstraint
{
    /** @var MimeType[] */
    private readonly array $allowedMimeTypes;

    public function __construct(MimeType ...$allowedMimeTypes)
    {
        $this->allowedMimeTypes = $allowedMimeTypes;
    }

    /**
     * @param FileMetadata $value
     */
    public function doEvaluate($value): void
    {
        if (in_array($value->getMimeType(), $this->allowedMimeTypes)) {
            return;
        }

        throw new ExpectationFailedException(
            'Mime type is not allowed',
            0,
            null,
            'NOT_ALLOWED_MIME_TYPE'
        );
    }
}
