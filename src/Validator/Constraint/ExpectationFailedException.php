<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

use Hgraca\PhpExtension\Exception\InputException;
use Throwable;

final class ExpectationFailedException extends InputException
{
    public function __construct(
        string $message = '',
        int $code = 0,
        Throwable $previous = null,
        protected string $errorCode = ''
    ) {
        parent::__construct($message, $code, $previous);
    }

    public static function withOverriddenMessageAndCode(
        ExpectationFailedException $exception,
        string $customErrorMessage,
        string $customErrorCode
    ): self {
        return new ExpectationFailedException(
            $customErrorMessage,
            0,
            $exception,
            $customErrorCode
        );
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
}
