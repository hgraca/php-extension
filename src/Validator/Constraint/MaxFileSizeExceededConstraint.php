<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

use Hgraca\PhpExtension\Filesystem\FileMetadata;
use Hgraca\PhpExtension\Filesystem\FileSize;

/**
 * @extends AbstractConstraint<FileMetadata>
 */
final class MaxFileSizeExceededConstraint extends AbstractConstraint
{
    public function __construct(private readonly FileSize $maxFileSize)
    {
    }

    /**
     * @param FileMetadata $value
     */
    public function doEvaluate($value): void
    {
        if ($this->maxFileSize->inBytes() >= $value->getSize()->inBytes()) {
            return;
        }

        throw new ExpectationFailedException(
            'File size exceeds the max allowed size',
            0,
            null,
            'MAX_FILE_SIZE_EXCEEDED'
        );
    }
}
