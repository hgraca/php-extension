<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

use DateTimeImmutable;

/**
 * @extends AbstractConstraint<DateTimeImmutable>
 */
final class NotBeforeDateConstraint extends AbstractConstraint
{
    public function __construct(private readonly DateTimeImmutable $referenceDate)
    {
    }

    protected function doEvaluate($value): void
    {
        if ($value >= $this->referenceDate) {
            return;
        }

        throw new ExpectationFailedException(
            'Date is before reference date',
            0,
            null,
            'DATE_IS_BEFORE_REFERENCE_DATE'
        );
    }
}
