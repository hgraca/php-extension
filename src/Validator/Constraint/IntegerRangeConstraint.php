<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

/**
 * @extends AbstractConstraint<int>
 */
final class IntegerRangeConstraint extends AbstractConstraint
{
    public function __construct(private readonly int $bottomBoundary, private readonly int $topBoundary)
    {
    }

    /**
     * @param int $value
     */
    protected function doEvaluate($value): void
    {
        if ($value < $this->bottomBoundary) {
            throw new ExpectationFailedException(
                'Boundary violation',
                0,
                null,
                'BOTTOM_BOUNDARY_VIOLATION'
            );
        }

        if ($value > $this->topBoundary) {
            throw new ExpectationFailedException(
                'Boundary violation',
                0,
                null,
                'TOP_BOUNDARY_VIOLATION'
            );
        }
    }
}
