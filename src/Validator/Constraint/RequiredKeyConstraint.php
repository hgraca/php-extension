<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

/**
 * @extends AbstractConstraint<array<string, ?string>>
 */
final class RequiredKeyConstraint extends AbstractConstraint
{
    /** @var string[] */
    private readonly array $requiredKeys;

    public function __construct(string ...$requiredKeys)
    {
        $this->requiredKeys = $requiredKeys;
    }

    /**
     * @param array<?string, ?string> $value
     */
    protected function doEvaluate($value): void
    {
        foreach ($this->requiredKeys as $key) {
            if (!isset($value[$key])) {
                throw new ExpectationFailedException(
                    sprintf('Required key %s is missing', $key),
                    0,
                    null,
                    'MISSING-KEY'
                );
            }
        }
    }
}
