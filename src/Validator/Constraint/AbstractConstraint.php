<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator\Constraint;

/**
 * @template TValue
 *
 * @implements ConstraintInterface<TValue>
 */
abstract class AbstractConstraint implements ConstraintInterface
{
    private ?string $customErrorMessage = null;

    private ?string $customErrorCode = null;

    /**
     * @param TValue $value
     *
     * @throws ExpectationFailedException
     */
    abstract protected function doEvaluate($value): void;

    final public function evaluate($value): void
    {
        if ($this->customErrorCode || $this->customErrorMessage) {
            $this->overrideException($value);
        } else {
            $this->doEvaluate($value);
        }
    }

    final public function withErrorMessage(string $customErrorMessage): ConstraintInterface
    {
        $this->customErrorMessage = $customErrorMessage;

        return $this;
    }

    final public function withErrorCode(string $customErrorCode): ConstraintInterface
    {
        $this->customErrorCode = $customErrorCode;

        return $this;
    }

    /**
     * @param TValue $value
     *
     * @throws ExpectationFailedException
     */
    private function overrideException($value): void
    {
        try {
            $this->doEvaluate($value);
        } catch (ExpectationFailedException $exception) {
            throw ExpectationFailedException::withOverriddenMessageAndCode(
                $exception,
                $this->customErrorMessage ?? $exception->getMessage(),
                $this->customErrorCode ?? $exception->getErrorCode()
            );
        }
    }
}
