<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\Validator;

use Hgraca\PhpExtension\Exception\ValidationException;

final readonly class NotEmptyStringValueValidator
{
    public const EMPTY_STRING_VALUE_VIOLATION_ERROR_CODE = 'EMPTY_STRING_VALUE_VIOLATION_ERROR';

    public function __construct(private ValidationException $validationException)
    {
    }

    public function validate(string $value, string $fieldName): void
    {
        if (empty(trim($value))) {
            $this->validationException->addError($fieldName, self::EMPTY_STRING_VALUE_VIOLATION_ERROR_CODE);
        }
    }
}
