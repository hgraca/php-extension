<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\CodeUnit;

use function array_key_exists;

/**
 * @template In of object
 * @template Out of object
 */
abstract readonly class AbstractClassesOneOnOneMapper
{
    /**
     * @param array<
     *      class-string<In>,
     *      class-string<Out>
     * > $map
     */
    public function __construct(private array $map)
    {
    }

    /**
     * @param In $object
     *
     * @return class-string<Out>
     */
    public function mapFromObject(object $object): string
    {
        $this->assertMappingExists($object::class);

        return $this->map[$object::class];
    }

    /**
     * @param class-string<In> $fqcn
     *
     * @return class-string<Out>
     */
    public function mapFromFqcn(string $fqcn): string
    {
        $this->assertMappingExists($fqcn);

        return $this->map[$fqcn];
    }

    public function assertMappingExists(string $fqcn): void
    {
        if (!array_key_exists($fqcn, $this->map)) {
            throw new MappingNotFoundException($fqcn);
        }
    }
}
