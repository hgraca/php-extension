<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\CodeUnit;

use Hgraca\PhpExtension\AbstractStaticClass;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionProperty;

use function array_merge;
use function count;

final class ClassHelper extends AbstractStaticClass
{
    public static function extractNamespace(string $fqcn): string
    {
        return mb_substr($fqcn, 0, (int) mb_strrpos($fqcn, '\\'));
    }

    public static function extractCanonicalClassName(string $classFqcn): string
    {
        return mb_substr($classFqcn, mb_strrpos($classFqcn, '\\') + 1);
    }

    public static function extractCanonicalMethodName(string $methodFqcn): string
    {
        return mb_substr($methodFqcn, mb_strrpos($methodFqcn, '::') + 2);
    }

    /**
     * @param class-string $className
     *
     * @throws ReflectionException
     *
     * @return ReflectionProperty[]
     */
    public static function getClassPropertyList(string $className): array
    {
        $reflectionClass = new ReflectionClass($className);
        $reflectionPropertyList = $reflectionClass->getProperties();
        $finalReflectionPropertyList = [];
        foreach ($reflectionPropertyList as $reflectionProperty) {
            $finalReflectionPropertyList[$reflectionProperty->getName()] = $reflectionProperty;
        }
        if ($parentClass = $reflectionClass->getParentClass()) {
            $parentPropertyList = self::getClassPropertyList($parentClass->getName());
            if (count($parentPropertyList) > 0) {
                $finalReflectionPropertyList = array_merge($parentPropertyList, $finalReflectionPropertyList);
            }
        }

        return $finalReflectionPropertyList;
    }

    /**
     * @param class-string $className
     *
     * @throws ReflectionException
     *
     * @return string[]
     */
    public static function getClassPropertyListAsStringList(string $className): array
    {
        $reflectionPropertiesList = self::getClassPropertyList($className);

        $finalReflectionPropertyList = [];

        foreach ($reflectionPropertiesList as $reflectionProperty) {
            $finalReflectionPropertyList[] = $reflectionProperty->getName();
        }

        return $finalReflectionPropertyList;
    }

    /**
     * @param class-string $classFqcn
     *
     * @throws ReflectionException
     */
    public static function getStaticPropertyValue(string $classFqcn, string $propertyName): mixed
    {
        $class = new ReflectionClass($classFqcn);
        $staticProperties = $class->getStaticProperties();

        return $staticProperties[$propertyName];
    }

    /**
     * @template PropertyValue
     *
     * @param class-string $classFqcn
     * @param PropertyValue $value
     *
     * @throws ReflectionException
     */
    public static function setStaticPropertyValue(string $classFqcn, string $propertyName, $value): void
    {
        $class = new ReflectionClass($classFqcn);
        $reflectedProperty = $class->getProperty($propertyName);
        /** @psalm-suppress UnusedMethodCall */
        $reflectedProperty->setAccessible(true);
        $reflectedProperty->setValue($value);
    }

    /**
     * @param class-string $className
     *
     * @throws ReflectionException
     *
     * @return array<mixed>
     */
    public static function getClassConstantList(string $className): array
    {
        return (new ReflectionClass($className))->getConstants();
    }

    /**
     * @template T of object
     *
     * @param class-string<T> $classFqcn
     *
     * @throws ReflectionException
     *
     * @return T
     */
    public static function instantiateWithoutConstructor(string $classFqcn)
    {
        return (new ReflectionClass($classFqcn))->newInstanceWithoutConstructor();
    }

    /**
     * @param class-string $className
     *
     * @return list<array{type: string, name: string}>
     */
    public static function getClassMethodParameterList(string $className, string $methodName): array
    {
        $reflectionClass = new ReflectionClass($className);
        $methodReflectionParameterList = $reflectionClass->getMethod($methodName)->getParameters();
        $methodParameterList = [];
        foreach ($methodReflectionParameterList as $methodReflectionParameter) {
            /** @var ?ReflectionNamedType $methodReflectionParameterType */
            $methodReflectionParameterType = $methodReflectionParameter->getType();
            $methodParameterList[] = [
                'type' => $methodReflectionParameterType !== null ? $methodReflectionParameterType->getName() : 'mixed',
                'name' => $methodReflectionParameter->getName(),
            ];
        }

        return $methodParameterList;
    }

    /**
     * @param ReflectionClass<object> $class
     */
    public static function getReflectionProperty(ReflectionClass $class, string $propertyName): ReflectionProperty
    {
        try {
            return $class->getProperty($propertyName);
        } catch (ReflectionException $e) {
            $parentClass = $class->getParentClass();
            if ($parentClass === false) {
                throw $e;
            }

            return static::getReflectionProperty($parentClass, $propertyName);
        }
    }
}
