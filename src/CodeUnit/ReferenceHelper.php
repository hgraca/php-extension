<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\CodeUnit;

use Hgraca\PhpExtension\Exception\RuntimeException;
use ReflectionException;

use function explode;
use function is_array;
use function is_object;
use function uniqid;

final class ReferenceHelper
{
    public static function isSame(mixed &$itemA, mixed &$itemB): bool
    {
        if ($itemA !== $itemB) {
            return false;
        }

        if (is_object($itemA)) {
            return $itemA === $itemB;
        }

        // Save original values of the items under comparison
        $originalItemA = $itemA;
        $originalItemB = $itemB;

        // Change itemB
        $newValueOfItemB = uniqid();
        $itemB = $newValueOfItemB;

        // Verify if itemA changed when we changed item B, in which case both variables reference the same item.
        $itemAChangedWhenItemBChanged = $itemA === $newValueOfItemB;

        // Put the items original values into place
        $itemA = $originalItemA;
        $itemB = $originalItemB;

        return $itemAChangedWhenItemBChanged;
    }

    /**
     * @param string $path Dot separated nested property list
     * @param array<mixed>|object $subject the object or array to get the property from
     *
     * @throws ReflectionException
     */
    public static function getNestedProperty(string $path, array|object $subject): mixed
    {
        if (mb_strpos($path, '.') === false) {
            $currentPropertyName = $path;
        } else {
            /** @psalm-suppress PossiblyUndefinedArrayOffset */
            [$currentPropertyName, $newPath] = explode('.', $path, 2);
        }

        /**
         * @psalm-suppress MixedAssignment
         * @psalm-suppress MixedArgument
         * @psalm-suppress PossiblyUndefinedVariable
         */
        switch (true) {
            case is_array($subject):
                $property = $subject[$currentPropertyName];

                return mb_strpos($path, '.') === false
                    ? $property
                    /** @phpstan-ignore-next-line If we reach this, we know $property is array|object */
                    : self::getNestedProperty($newPath, $property);
            case is_object($subject):
                $property = ObjectHelper::getProtectedProperty($subject, $currentPropertyName);

                return mb_strpos($path, '.') === false
                    ? $property
                    /** @phpstan-ignore-next-line If we reach this, we know $property is array|object */
                    : self::getNestedProperty($newPath, $property);
            default:
                throw new RuntimeException(
                    'I can\'t mine a property from items of type ' . TypeHelper::getType($subject)
                );
        }
    }
}
