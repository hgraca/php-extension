<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\CodeUnit;

use Hgraca\PhpExtension\AbstractStaticClass;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

use function array_merge;
use function array_reverse;
use function array_values;

final class ObjectHelper extends AbstractStaticClass
{
    /**
     * @param array<string, mixed> $propertyList
     */
    public static function setPropertiesFromKeyValue(object $object, array $propertyList): void
    {
        $hierarchyPropertyList = self::getAllHierarchyReflectionProperties($object);
        foreach ($hierarchyPropertyList as $propertyName => $reflectionProperty) {
            /** @psalm-suppress UnusedMethodCall */
            $reflectionProperty->setAccessible(true);

            if (array_key_exists($propertyName, $propertyList)) {
                $property = &$propertyList[$propertyName];
            } elseif (self::isPropertyNullable($reflectionProperty)) {
                $property = null;
            } else {
                $class = $object::class;
                throw new InvalidArgumentException(
                    "The target object, of class '$class', "
                    . "requires property '$propertyName', "
                    . "which is not in the given property list: \n"
                    . print_r($propertyList, true)
                );
            }

            $reflectionProperty->setValue($object, $property); // This doesn't set by reference, but for the future...
            unset($propertyList[$propertyName]);
        }
    }

    /**
     * @return array<string, mixed>
     */
    public static function getPropertiesAsKeyValue(object $object): array
    {
        return self::getAllHierarchyPropertiesAsKeyValue($object);
    }

    /**
     * @param array<mixed> $arguments
     *
     * @throws ReflectionException
     *
     * @return mixed
     */
    public static function invokeProtectedMethod(object $object, string $methodName, array $arguments = [])
    {
        $class = new ReflectionClass($object::class);
        $method = $class->getMethod($methodName);
        /** @psalm-suppress UnusedMethodCall */
        $method->setAccessible(true);

        return $method->invokeArgs($object, $arguments);
    }

    /**
     * @template ClassValue
     *
     * @param ClassValue $value
     *
     * @throws ReflectionException
     */
    public static function setProtectedProperty(object $object, string $propertyName, $value): void
    {
        $class = new ReflectionClass($object::class);

        $property = ClassHelper::getReflectionProperty($class, $propertyName);
        /** @psalm-suppress UnusedMethodCall */
        $property->setAccessible(true);
        $property->setValue($object, $value);
    }

    /**
     * @throws ReflectionException
     *
     * @return mixed
     */
    public static function getProtectedProperty(object $object, string $propertyName)
    {
        $class = new ReflectionClass($object::class);

        $property = ClassHelper::getReflectionProperty($class, $propertyName);
        /** @psalm-suppress UnusedMethodCall */
        $property->setAccessible(true);

        return $property->getValue($object);
    }

    /**
     * @return array<string, mixed>
     */
    private static function getAllHierarchyPropertiesAsKeyValue(object $object): array
    {
        $propertyList = [];
        self::visitHierarchyProperties(
            $object,
            static function (ReflectionProperty $reflectionProperty) use (&$propertyList, $object): void {
                $propertyList[$reflectionProperty->getDeclaringClass()->getName()][$reflectionProperty->getName()]
                    = $reflectionProperty->getValue($object);
            }
        );

        return !empty($propertyList) ? array_merge(...array_values(array_reverse($propertyList))) : [];
    }

    /**
     * @return ReflectionProperty[]
     */
    private static function getAllHierarchyReflectionProperties(object $object): array
    {
        $propertyList = [];
        self::visitHierarchyProperties(
            $object,
            static function (ReflectionProperty $reflectionProperty) use (&$propertyList): void {
                $propertyList[$reflectionProperty->getDeclaringClass()->getName()][$reflectionProperty->getName()] =
                    $reflectionProperty;
            }
        );

        return !empty($propertyList) ? array_merge(...array_values(array_reverse($propertyList))) : [];
    }

    private static function visitHierarchyProperties(object $object, callable $visitProperty): void
    {
        try {
            $reflectionClass = new ReflectionClass($object);
            do {
                foreach ($reflectionClass->getProperties() as $reflectionProperty) {
                    /** @psalm-suppress UnusedMethodCall */
                    $reflectionProperty->setAccessible(true);
                    $visitProperty($reflectionProperty);
                }
            } while ($reflectionClass = $reflectionClass->getParentClass());
        } catch (ReflectionException) {
            // Silently ignore this exception so the visiting can continue. Maybe we should log a warning though.
        }
    }

    private static function isPropertyNullable(ReflectionProperty $reflectionProperty): bool
    {
        $docBlock = $reflectionProperty->getDocComment();

        if ($docBlock === false) {
            return false;
        }

        return preg_match('/(\* @var \?|\* @var null\||\* @var .+\|null)/mi', $docBlock) === 1;
    }
}
