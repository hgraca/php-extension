<?php

declare(strict_types=1);

/*
 * This file is part of PhpExtension,
 * a PHP library with code that can be included into a project and used as if it was part of the PHP core itself.
 *
 * @link https://herbertograca.com/2018/07/07/more-than-concentric-layers/
 *
 * (c) Herberto Graça
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hgraca\PhpExtension\CodeUnit;

use Hgraca\PhpExtension\AbstractStaticClass;
use Hgraca\PhpExtension\String\StringHelper;
use Throwable;

class TypeHelper extends AbstractStaticClass
{
    private const LABEL_REGEX = '/[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/';
    private const SCALAR_TYPE_LIST = ['boolean', 'bool', 'integer', 'int', 'float', 'string'];
    private const COMPOUND_TYPE_LIST = ['array', 'object', 'callable', 'iterable'];
    private const SPECIAL_TYPE_LIST = ['resource', 'null'];

    /**
     * @return string|class-string
     */
    public static function getType(mixed $subject): string
    {
        $type = gettype($subject);

        return match ($type) {
            'boolean' => 'bool',
            'integer' => 'int',
            'double' => 'float',
            'object' => $subject::class,
            'NULL' => 'null',
            'string', 'array', 'resource', 'resource (closed)' => $type,
            default => throw new UnknownTypeException($subject),
        };
    }

    public static function isValidFQCN(string $string): bool
    {
        return self::isValidLabel($string)
            && !self::isScalarType($string)
            && !self::isCompoundType($string)
            && !self::isSpecialType($string)
            && !StringHelper::hasEnding('\\', $string);
    }

    /**
     * `label` refers to variables names, namespaces, class names, function names, etc.
     */
    public static function isValidLabel(string $string): bool
    {
        return preg_match(self::LABEL_REGEX, $string) === 1;
    }

    public static function isScalarType(string $string): bool
    {
        return in_array($string, self::SCALAR_TYPE_LIST, true);
    }

    public static function isCompoundType(string $string): bool
    {
        return in_array($string, self::COMPOUND_TYPE_LIST, true);
    }

    public static function isSpecialType(string $string): bool
    {
        return in_array($string, self::SPECIAL_TYPE_LIST, true);
    }

    public static function isNativeType(string $type): bool
    {
        return self::isScalarType($type)
            || self::isCompoundType($type)
            || self::isSpecialType($type);
    }

    public static function forceToString(mixed $value): string
    {
        try {
            return (string) $value;
        } catch (Throwable) {
            return print_r($value, true);
        }
    }
}
